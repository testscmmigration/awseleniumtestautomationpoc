package com.saf.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.JxlWriteException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;
import com.saf.utilities.DatabaseConnection;
import com.saf.utilities.Log;
import com.saf.utilities.TestDataProvider;

public abstract class BaseTest {

	private static final String TASKLIST = "tasklist";
	private static final String KILL = "taskkill /F /IM ";

	public static ExtentReports report;
	public static ExtentTest loggernew;
	public static String extentreportpath = null;
	public static String executionReceiptspath = null;
	public static String extentreportfinalpath = null;
	public static String dailyTranReportsPath = null;

	public static String reportscreenshotpath = null;
	String screenshot_path = null;
	String image = null;
	public static String curProj = System.getProperty("user.dir");
	public static String databaseName;
	public static String databasePassword;
	public static String databaseServerIP;
	public static String databaseType;
	public static String databaseUserName;
	public static String testDataSource;
	public static DatabaseConnection databaseConnection;
	public static String inputDataSheetPath;
	public static String outputDataSheetFolder;
	public static String outputDataSheetPath;
	public static String environment;
	public static String webdriverServerHostName;
	public static String webdriverServerPortName;
	public static String webdriverUrl;
	public static String usefakeauthUrl;
	public static String fakeauthUrl;
	public static String applicationUrl;
	public static String chromeProfile;
	public static String phoneNumberAreaCode;
	public static int senMinRangeA;
	public static int senMinRangeB;
	public static int senMaxRangeA;
	public static int senMaxRangeB;
	public static int recMinRangeA;
	public static int recMinRangeB;
	public static int recMaxRangeA;
	public static int recMaxRangeB;
	public static int bpMinRangeA;
	public static int bpMinRangeB;
	public static int bpMaxRangeA;
	public static int bpMaxRangeB;
	public static int DOBYearMinRange;
	public static int DOBYearMaxRange;
	public static int DOBMonthMinRange;
	public static int DOBMonthMaxRange;
	public static int DOBDayMinRange;
	public static int DOBDayMaxRange;
	public static int timeOut;

	public WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}

	public LinkedHashMap<String, String> mapDataSheet;
	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();
	public TestDataProvider testDataProvider;
	public String testName;
	public String testBrowser;

	public static ResourceBundle globalProperties;
	public static ResourceBundle gridProperties;

	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.YYYY-HH.mm.ss");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	Date reportStamp = new Date();
	String reportTimeStamp = dateTimeFormat.format(reportStamp);

	Date currentDateTime = null;
	protected String dateTime = null;

	public static int screenshotCount = 0;

	protected static final Logger logger = Log.getInstance(Thread.currentThread().getStackTrace()[1].getClassName());

	// Data row count.
	// So that data can be written back to excel at the same row.
	public int rowCount;

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public BaseTest() {
		this.testDataProvider = new TestDataProvider();

		// Read the Global properties
		getGlobalProperties();

		// Read the Grid properties
		// getGridProperties();
	}

	/**
	 * This function reads Global properties
	 * 
	 * @throws DataSheetException
	 */
	protected void getGlobalProperties() {
		globalProperties = ResourceBundle.getBundle("global");

		// Read the Global properties
		environment = globalProperties.getString("environment");
		usefakeauthUrl = globalProperties.getString("usefakeauthURL");
		fakeauthUrl = globalProperties.getString("fakeauthURL");
		applicationUrl = globalProperties.getString("applicationURL");
		chromeProfile = globalProperties.getString("chromeProfile");
		timeOut = Integer.parseInt(globalProperties.getString("time_out"));
		extentreportpath = globalProperties.getString("extentreport_path");
		executionReceiptspath = globalProperties.getString("executionReceipts_path");
		dailyTranReportsPath = globalProperties.getString("dailyTranReports_path");
		senMinRangeA = Integer.parseInt(globalProperties.getString("senMinRangeA"));
		senMinRangeB = Integer.parseInt(globalProperties.getString("senMinRangeB"));
		senMaxRangeA = Integer.parseInt(globalProperties.getString("senMaxRangeA"));
		senMaxRangeB = Integer.parseInt(globalProperties.getString("senMaxRangeB"));
		recMinRangeA = Integer.parseInt(globalProperties.getString("recMinRangeA"));
		recMinRangeB = Integer.parseInt(globalProperties.getString("recMinRangeB"));
		recMaxRangeA = Integer.parseInt(globalProperties.getString("recMaxRangeA"));
		recMaxRangeB = Integer.parseInt(globalProperties.getString("recMaxRangeB"));
		bpMinRangeA = Integer.parseInt(globalProperties.getString("bpMinRangeA"));
		bpMinRangeB = Integer.parseInt(globalProperties.getString("bpMinRangeB"));
		bpMaxRangeA = Integer.parseInt(globalProperties.getString("bpMaxRangeA"));
		bpMaxRangeB = Integer.parseInt(globalProperties.getString("bpMaxRangeB"));
		DOBYearMinRange = Integer.parseInt(globalProperties.getString("DOBYearMinRange"));
		DOBYearMaxRange = Integer.parseInt(globalProperties.getString("DOBYearMaxRange"));
		DOBMonthMinRange = Integer.parseInt(globalProperties.getString("DOBMonthMinRange"));
		DOBMonthMaxRange = Integer.parseInt(globalProperties.getString("DOBMonthMaxRange"));
		DOBDayMinRange = Integer.parseInt(globalProperties.getString("DOBDayMinRange"));
		DOBDayMaxRange = Integer.parseInt(globalProperties.getString("DOBDayMaxRange"));
		phoneNumberAreaCode = globalProperties.getString("phoneNumberAreaCode");

		if (globalProperties.containsKey("test_data_source")) {
			testDataSource = globalProperties.getString("test_data_source");
		}

		if (testDataSource.equalsIgnoreCase("excel")) {
			inputDataSheetPath = curProj + "\\src\\test\\resources\\Input_DataSheet\\Data.xls";
			outputDataSheetFolder = curProj + "/" + "target" + "/" + "Output_DataSheet";
			createDirectories(outputDataSheetFolder);
			outputDataSheetPath = outputDataSheetFolder + "\\Data_" + dateTimeFormat.format(new Date()) + ".xls";

			if (inputDataSheetPath.equals("")) {
				logger.error("Please provide a valid sheet path.");
				Assert.fail();
			}
		} else if (testDataSource.equalsIgnoreCase("database")) {
			databaseName = globalProperties.getString("database_name");
			databaseType = globalProperties.getString("database_type");
			databaseUserName = globalProperties.getString("database_username");
			databasePassword = globalProperties.getString("database_password");
			databaseServerIP = globalProperties.getString("database_ip");
		} else {
			logger.error("Please provide a valid test data source value.");
			Assert.fail();
		}
	}

	/**
	 * This function reads Grid properties
	 */
	protected void getGridProperties() {
		gridProperties = ResourceBundle.getBundle("grid");
		webdriverServerHostName = gridProperties.getString("webdriver_hostname");
		webdriverServerPortName = gridProperties.getString("webdriver_port");
		webdriverUrl = gridProperties.getString("webdriver_defaultURL");
	}

	/**
	 * @param testName
	 * @param browser
	 * @param mapDataSheet
	 */
	public BaseTest(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		this.testName = testName;
		this.testDataProvider = new TestDataProvider();
		this.testBrowser = browser;
		this.mapDataSheet = mapDataSheet;

		System.out.println("BaseTest -> Data: " + mapDataSheet.toString());
		System.out.println("BaseTest -> TestName: " + testName);
		System.out.println("BaseTest -> TestBrowser: " + testBrowser + "\n");

		this.rowCount = Integer.parseInt(mapDataSheet.get("rowCount"));
		System.out.println("Row Count: " + rowCount + "\n");
	}

	@BeforeSuite
	public void beforeSuite() throws BiffException, IOException, WriteException {
		extentreportfinalpath = "target" + "/" + "ExecutionReport" + "/" + extentreportpath + "_"
				+ dateTimeFormat.format(new Date());
		report = new ExtentReports(extentreportfinalpath + "/" + "ExtentReports.html", false, NetworkMode.OFFLINE);
		report.addSystemInfo("Environment", environment);
		Workbook workbook = Workbook.getWorkbook(new File(inputDataSheetPath));
		WritableWorkbook copy = Workbook.createWorkbook(new File(outputDataSheetPath), workbook);
		copy.write();
		copy.close();
		if (workbook != null) {
			workbook.close();
		}
	}

	/**
	 * This function initiate a web driver client
	 * 
	 * @throws Exception
	 */
	@BeforeMethod
	public void beforeMethod() throws Exception {
		screenshotCount = 0;
		currentDateTime = new Date();
		dateTime = dateTimeFormat.format(currentDateTime);

		// Mention the path of ExtentReport to be generated
		loggernew = report.startTest(getValue("Browser") + "-" + getValue("Scenario"));

		System.out.println("BaseTest -> BeforeMethod -> TestData: " + mapDataSheet.toString());
		System.out.println("BaseTest -> BeforeMethod -> TestName: " + testName);
		System.out.println("BaseTest -> BeforeMethod -> TestBrowser: " + testBrowser + "\n");

		reportscreenshotpath = this.getClass().getSimpleName() + "_"
				+ globalProperties.getString("reportscreenshot_path") + dateTime + "/";

		if (!(this.testBrowser == null)) {
			String formattedTime = timeFormat.format(new Date()).toString();
			logger.info("\n" + "\nTest Case Name            :  " + testName + "\nExecution using Data Row  :  "
					+ mapDataSheet.toString() + "\nStart time                :  " + formattedTime
					+ "\n-------------------------------------------------------------------------------------------------------------------------------------");

			if (this.testBrowser.equalsIgnoreCase("InternetExplorer") || this.testBrowser.equalsIgnoreCase("Edge")
					|| this.testBrowser.equalsIgnoreCase("Firefox") || this.testBrowser.equalsIgnoreCase("Chrome")
					|| this.testBrowser.equalsIgnoreCase("HeadlessChrome")
					|| this.testBrowser.equalsIgnoreCase("HTMLUnitDriver")
					|| this.testBrowser.equalsIgnoreCase("PhantomJS")) {

				// Find and kill particular running browser instances before starting the test
				killParticularBrowserInstances(this.testBrowser);

				// Start WebDriver
				startWebDriverClient(this.testBrowser);

				logger.info("Test initialization.");
				logger.info("Calling startWebDriverClient for browser: " + this.testBrowser);
			} else {
				logger.error("Browser name" + " '" + this.testBrowser + "' "
						+ "is invalid:Please give InternetExplorer, Edge, Firefox, Chrome, , HeadlessChrome, HTMLUnitDriver or PhantomJS.");
			}
		}
	}

	@AfterMethod
	public void afterMethod(ITestResult testResult) throws Exception {
		if (!(this.testBrowser == null)) {
			if (this.testBrowser.equalsIgnoreCase("InternetExplorer") || this.testBrowser.equalsIgnoreCase("Edge")
					|| this.testBrowser.equalsIgnoreCase("Firefox") || this.testBrowser.equalsIgnoreCase("Chrome")
					|| this.testBrowser.equalsIgnoreCase("HeadlessChrome")
					|| this.testBrowser.equalsIgnoreCase("HTMLUnitDriver")
					|| this.testBrowser.equalsIgnoreCase("PhantomJS")) {
				if (testDataSource.equalsIgnoreCase("database")) {
					while (databaseConnection != null)
						databaseConnection.closeDatabaseConnectivity();
				}

				// Can call the method 'addDataToOutputExcel' if needed
				if (testDataSource.equalsIgnoreCase("excel")) {
					// call to excel Writing
				}

				System.out.println("Test Result: " + testResult.getStatus());
				try {
					if (testResult.getStatus() == ITestResult.SUCCESS) {

					} else if (testResult.getStatus() == ITestResult.FAILURE) {
						// If the test case failed due to exception take the screen shot and write the
						// error message into the excel
						takeScreenshotFail(testResult.getThrowable().getMessage());
						dataToBeWritten.clear();
						dataToBeWritten.put("Failure Message", testResult.getThrowable().getMessage());
						addDataToOutputExcel(dataToBeWritten);

						// If the error popup exists close it and end the session by clicking on the
						// logout link
						WebElement errorModalSubmit;
						try {
							errorModalSubmit = driver.findElement(By.id("btnErrorModalSubmit"));
							if (errorModalSubmit.isDisplayed()) {
								errorModalSubmit.click();
								WebElement Logout = driver.findElement(By.xpath("//li[3]/span/span"));
								Logout.click();
							}
						} catch (Exception e) {
							logger.info("The element with "
									+ By.id("btnErrorModalSubmit").toString().replace("By.", " ") + " not found. ",
									e.fillInStackTrace());
							throw new NoSuchElementException("The element with "
									+ By.id("btnErrorModalSubmit").toString().replace("By.", " ") + " not found.");
						}
					}
				} catch (Exception e) {
					logger.error("Test case: " + getValue("Browser") + " " + getValue("Scenario") + " failed. ",
							e.fillInStackTrace());
				}
			} else {
				logger.error("Invalid browser name.");
			}

			report.endTest(loggernew);

			// Clear the cache for the ChromeDriver instance.
//				if (this.testBrowser.equalsIgnoreCase("Chrome")) {
//					driver.get("chrome://settings/clearBrowserData");
//					Thread.sleep(10000);
//					driver.findElement(By.cssSelector("* /deep/ #clearBrowsingDataConfirm")).click();
//				}

			driver.close();
			driver.quit();

			// Find and kill particular running browser instances after test execution
			killParticularBrowserInstances(this.testBrowser);

			String sourceFolder = this.getClass().getSimpleName() + "_"
					+ globalProperties.getString("reportscreenshot_path") + dateTime;
			File sourceDirectory = new File(sourceFolder);
			String destinationFolder = extentreportfinalpath + "/" + sourceFolder;
			File destinationDirectory = new File(destinationFolder);
			FileUtils.moveDirectory(sourceDirectory, destinationDirectory);
		}

		String formattedTime = timeFormat.format(new Date()).toString();
		logger.info("In afterMethod - Performing test cleanup , Closing webdriver instance\n"
				+ "End time         :       " + formattedTime
				+ "\n---------------------------------------------------------------------------------------------------\n"
				+

				"-----------------------------------------------------------------------------------------------------\n");
	}

	@AfterSuite
	public void afterSuite() {
		report.flush();
	}

	/**
	 * This function identifies all the processes particular to a specific browser to kill  
	 */
	public static void killParticularBrowserInstances(String browser) throws Exception {
		switch (browser) {
		case "InternetExplorer":
			while (isProcessRunning("iexplore.exe")) {
				killProcess("iexplore.exe");
			}
			while (isProcessRunning("IEDriverServer.exe")) {
				killProcess("IEDriverServer.exe");
			}
			while (isProcessRunning("iexplore.exe *32")) {
				killProcess("iexplore.exe *32");
			}
			break;
		}
	}

	/**
	 * This function finds running win-processes
	 */
	public static boolean isProcessRunning(String serviceName) throws Exception {
		Process p = Runtime.getRuntime().exec(TASKLIST);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			// System.out.println(line);
			if (line.contains(serviceName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This function kills running win-processes
	 */
	public static void killProcess(String serviceName) throws Exception {
		Runtime.getRuntime().exec(KILL + serviceName);
	}

	/**
	 * This function creates the directory if it doesn't exists
	 */
	public void createDirectory(String dirName) {
		File theDir = new File(dirName);
		if (!theDir.exists()) {
			logger.info("Creating directory: " + theDir.getName());
			try {
				theDir.mkdir();
			} catch (Exception e) {
				logger.error("Issue in creating directory: ", e.fillInStackTrace());
			}
		}
	}

	/**
	 * This function creates the directories if they doesn't exists
	 */
	public void createDirectories(String dirsName) {
		File theDirs = new File(dirsName);
		if (!theDirs.exists()) {
			logger.info("Creating directories: " + theDirs.getName());
			try {
				theDirs.mkdirs();
			} catch (Exception e) {
				logger.error("Issue in creating directories: ", e.fillInStackTrace());
			}
		}
	}

	/**
	 * This function returns random number of required length
	 */
	public static String generateRandomNumber(int length) {
		StringBuilder builder = new StringBuilder();
		Random random = new Random(System.nanoTime());
		builder.append(String.valueOf(random.nextInt(9) + 1)); // avoid the leading digit is 0;
		for (int i = 1; i < length; ++i)
			builder.append(String.valueOf(random.nextInt(10)));
		return builder.toString();
	}

	/**
	 * This function returns random number within the given range
	 */
	public static String generateRandomNumberWithinRange(int min, int max) {
		Random random = new Random(System.nanoTime());
		return Integer.toString(random.nextInt(max - min + 1) + min);
	}

	/**
	 * This function returns random alphabet
	 */
	public static String generateRandomAlphabet() {
		String alphabets[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
				"S", "T", "U", "V", "W", "X", "Y", "Z" };
		return alphabets[new Random().nextInt(alphabets.length)];
	}

	/**
	 * This function returns random sender first name
	 */
	public static String generateRandomSenderFirstName() {
		String senderFirstNames[] = { "AARON", "ABBY", "ABIGAIL", "ADA", "ADAM", "ADDIE", "ADELA", "ADELE", "ADELINE",
				"ADRIAN", "ADRIAN", "ADRIANA", "ADRIENNE", "AGNES", "AIDA", "AILEEN", "AIMEE", "AISHA", "ALAN", "ALANA",
				"ALBA", "ALBERT", "ALBERTA", "ALBERTO", "ALEJANDRA", "ALEX", "ALEXANDER", "ALEXANDRA", "ALEXANDRIA",
				"ALEXIS", "ALFRED", "ALFREDA", "ALFREDO", "ALICE", "ALICIA", "ALINE", "ALISA", "ALISHA", "ALISON",
				"ALISSA", "ALLAN", "ALLEN", "ALLIE", "ALLISON", "ALLYSON", "ALMA", "ALTA", "ALTHEA", "ALVIN", "ALYCE",
				"ALYSON", "ALYSSA", "AMALIA", "AMANDA", "AMBER", "AMELIA", "AMIE", "AMPARO", "AMY", "ANA", "ANASTASIA",
				"ANDRE", "ANDREA", "ANDREW", "ANDY", "ANGEL", "ANGEL", "ANGELA", "ANGELIA", "ANGELICA", "ANGELINA",
				"ANGELINE", "ANGELIQUE", "ANGELITA", "ANGIE", "ANITA", "ANN", "ANNA", "ANNABELLE", "ANNE", "ANNETTE",
				"ANNIE", "ANNMARIE", "ANTHONY", "ANTOINETTE", "ANTONIA", "ANTONIO", "APRIL", "ARACELI", "ARLENE",
				"ARLINE", "ARMANDO", "ARNOLD", "ARTHUR", "ASHLEE", "ASHLEY", "AUDRA", "AUDREY", "AUGUSTA", "AURELIA",
				"AURORA", "AUTUMN", "AVA", "AVIS", "BARBARA", "BARBRA", "BARRY", "BEATRICE", "BEATRIZ", "BECKY",
				"BELINDA", "BEN", "BENITA", "BENJAMIN", "BERNADETTE", "BERNADINE", "BERNARD", "BERNICE", "BERTA",
				"BERTHA", "BERTIE", "BERYL", "BESSIE", "BETH", "BETHANY", "BETSY", "BETTE", "BETTIE", "BETTY", "BETTYE",
				"BEULAH", "BEVERLEY", "BEVERLY", "BIANCA", "BILL", "BILLIE", "BILLY", "BLANCA", "BLANCHE", "BOB",
				"BOBBI", "BOBBIE", "BOBBY", "BONITA", "BONNIE", "BRAD", "BRADLEY", "BRANDI", "BRANDIE", "BRANDON",
				"BRANDY", "BRENDA", "BRENT", "BRETT", "BRIAN", "BRIANA", "BRIANNA", "BRIDGET", "BRIDGETT", "BRIDGETTE",
				"BRIGITTE", "BRITNEY", "BRITTANY", "BRITTNEY", "BROOKE", "BRUCE", "BRYAN", "BYRON", "CAITLIN", "CALLIE",
				"CALVIN", "CAMILLE", "CANDACE", "CANDICE", "CANDY", "CARA", "CAREY", "CARISSA", "CARL", "CARLA",
				"CARLENE", "CARLOS", "CARLY", "CARMELA", "CARMELLA", "CARMEN", "CAROL", "CAROLE", "CAROLINA",
				"CAROLINE", "CAROLYN", "CARRIE", "CASANDRA", "CASEY", "CASEY", "CASSANDRA", "CASSIE", "CATALINA",
				"CATHERINE", "CATHLEEN", "CATHRYN", "CATHY", "CECELIA", "CECIL", "CECILE", "CECILIA", "CELESTE",
				"CELIA", "CELINA", "CHAD", "CHANDRA", "CHARITY", "CHARLENE", "CHARLES", "CHARLIE", "CHARLOTTE",
				"CHARMAINE", "CHASITY", "CHELSEA", "CHERI", "CHERIE", "CHERRY", "CHERYL", "CHESTER", "CHRIS", "CHRIS",
				"CHRISTA", "CHRISTI", "CHRISTIAN", "CHRISTIAN", "CHRISTIE", "CHRISTINA", "CHRISTINE", "CHRISTOPHER",
				"CHRISTY", "CHRYSTAL", "CINDY", "CLAIRE", "CLARA", "CLARE", "CLARENCE", "CLARICE", "CLARISSA", "CLAUDE",
				"CLAUDETTE", "CLAUDIA", "CLAUDINE", "CLAYTON", "CLEO", "CLIFFORD", "CLIFTON", "CLINTON", "CLYDE",
				"CODY", "COLEEN", "COLETTE", "COLLEEN", "CONCEPCION", "CONCETTA", "CONNIE", "CONSTANCE", "CONSUELO",
				"CORA", "COREY", "CORINA", "CORINE", "CORINNE", "CORNELIA", "CORRINE", "CORY", "COURTNEY", "CRAIG",
				"CRISTINA", "CRYSTAL", "CURTIS", "CYNTHIA", "DAISY", "DALE", "DALE", "DAN", "DANA", "DANIEL",
				"DANIELLE", "DANNY", "DAPHNE", "DARCY", "DARLA", "DARLENE", "DARRELL", "DARREN", "DARRYL", "DARYL",
				"DAVE", "DAVID", "DAWN", "DEAN", "DEANA", "DEANN", "DEANNA", "DEANNE", "DEBBIE", "DEBORA", "DEBORAH",
				"DEBRA", "DEE", "DEENA", "DEIDRE", "DEIRDRE", "DELIA", "DELLA", "DELORES", "DELORIS", "DENA", "DENISE",
				"DENNIS", "DEREK", "DERRICK", "DESIREE", "DIANA", "DIANE", "DIANN", "DIANNA", "DIANNE", "DINA",
				"DIONNE", "DIXIE", "DOLLIE", "DOLLY", "DOLORES", "DOMINIQUE", "DON", "DONA", "DONALD", "DONNA", "DORA",
				"DOREEN", "DORIS", "DOROTHEA", "DOROTHY", "DORTHY", "DOUGLAS", "DUANE", "DUSTIN", "DWAYNE", "DWIGHT",
				"EARL", "EARLENE", "EARLINE", "EARNESTINE", "EBONY", "EDDIE", "EDDIE", "EDGAR", "EDITH", "EDNA",
				"EDUARDO", "EDWARD", "EDWIN", "EDWINA", "EFFIE", "EILEEN", "ELAINE", "ELBA", "ELEANOR", "ELENA",
				"ELINOR", "ELISA", "ELISABETH", "ELISE", "ELIZA", "ELIZABETH", "ELLA", "ELLEN", "ELMA", "ELMER",
				"ELNORA", "ELOISE", "ELSA", "ELSIE", "ELVA", "ELVIA", "ELVIRA", "EMILIA", "EMILY", "EMMA", "ENID",
				"ENRIQUE", "ERIC", "ERICA", "ERICKA", "ERIK", "ERIKA", "ERIN", "ERMA", "ERNA", "ERNEST", "ERNESTINE",
				"ESMERALDA", "ESPERANZA", "ESSIE", "ESTELA", "ESTELLA", "ESTELLE", "ESTER", "ESTHER", "ETHEL", "ETTA",
				"EUGENE", "EUGENIA", "EULA", "EUNICE", "EVA", "EVANGELINA", "EVANGELINE", "EVE", "EVELYN", "EVERETT",
				"FAITH", "FANNIE", "FANNY", "FAY", "FAYE", "FELECIA", "FELICIA", "FELIX", "FERN", "FERNANDO", "FLORA",
				"FLORENCE", "FLORINE", "FLOSSIE", "FLOYD", "FRAN", "FRANCES", "FRANCESCA", "FRANCINE", "FRANCIS",
				"FRANCIS", "FRANCISCA", "FRANCISCO", "FRANK", "FRANKIE", "FRANKLIN", "FRED", "FREDA", "FREDDIE",
				"FREDERICK", "FREIDA", "FRIEDA", "GABRIEL", "GABRIELA", "GABRIELLE", "GAIL", "GALE", "GARY", "GAY",
				"GAYLE", "GENA", "GENE", "GENEVA", "GENEVIEVE", "GEORGE", "GEORGETTE", "GEORGIA", "GEORGINA", "GERALD",
				"GERALDINE", "GERTRUDE", "GILBERT", "GILDA", "GINA", "GINGER", "GLADYS", "GLEN", "GLENDA", "GLENN",
				"GLENNA", "GLORIA", "GOLDIE", "GORDON", "GRACE", "GRACIE", "GRACIELA", "GREG", "GREGORY", "GRETA",
				"GRETCHEN", "GUADALUPE", "GUY", "GWEN", "GWENDOLYN", "HALEY", "HALLIE", "HANNAH", "HAROLD", "HARRIET",
				"HARRIETT", "HARRY", "HARVEY", "HATTIE", "HAZEL", "HEATHER", "HECTOR", "HEIDI", "HELEN", "HELENA",
				"HELENE", "HELGA", "HENRIETTA", "HENRY", "HERBERT", "HERMAN", "HERMINIA", "HESTER", "HILARY", "HILDA",
				"HILLARY", "HOLLIE", "HOLLY", "HOPE", "HOWARD", "HUGH", "IAN", "IDA", "ILA", "ILENE", "IMELDA",
				"IMOGENE", "INA", "INES", "INEZ", "INGRID", "IRENE", "IRIS", "IRMA", "ISAAC", "ISABEL", "ISABELLA",
				"ISABELLE", "IVA", "IVAN", "IVY", "JACK", "JACKIE", "JACKLYN", "JACLYN", "JACOB", "JACQUELINE",
				"JACQUELYN", "JAIME", "JAIME", "JAMES", "JAMES", "JAMI", "JAMIE", "JAMIE", "JAN", "JANA", "JANE",
				"JANELL", "JANELLE", "JANET", "JANETTE", "JANICE", "JANIE", "JANINE", "JANIS", "JANNA", "JANNIE",
				"JARED", "JASMINE", "JASON", "JAVIER", "JAY", "JAYNE", "JEAN", "JEANETTE", "JEANIE", "JEANINE",
				"JEANNE", "JEANNETTE", "JEANNIE", "JEANNINE", "JEFF", "JEFFERY", "JEFFREY", "JENIFER", "JENNA",
				"JENNIE", "JENNIFER", "JENNY", "JEREMY", "JERI", "JEROME", "JERRI", "JERRY", "JERRY", "JESSE",
				"JESSICA", "JESSIE", "JESSIE", "JESUS", "JEWEL", "JEWELL", "JILL", "JILLIAN", "JIM", "JIMMIE", "JIMMIE",
				"JIMMY", "JO", "JOAN", "JOANN", "JOANNA", "JOANNE", "JOCELYN", "JODI", "JODIE", "JODY", "JOE", "JOEL",
				"JOHANNA", "JOHN", "JOHN", "JOHNNIE", "JOHNNIE", "JOHNNY", "JOLENE", "JON", "JONATHAN", "JONI",
				"JORDAN", "JORDAN", "JORGE", "JOSE", "JOSEFA", "JOSEFINA", "JOSEPH", "JOSEPHINE", "JOSHUA", "JOSIE",
				"JOY", "JOYCE", "JUAN", "JUANA", "JUANITA", "JUDITH", "JUDY", "JULIA", "JULIAN", "JULIANA", "JULIANNE",
				"JULIE", "JULIET", "JULIETTE", "JULIO", "JUNE", "JUSTIN", "JUSTINE", "KAITLIN", "KARA", "KAREN", "KARI",
				"KARIN", "KARINA", "KARL", "KARLA", "KARYN", "KASEY", "KATE", "KATELYN", "KATHARINE", "KATHERINE",
				"KATHERYN", "KATHIE", "KATHLEEN", "KATHRINE", "KATHRYN", "KATHY", "KATIE", "KATINA", "KATRINA", "KATY",
				"KAY", "KAYE", "KAYLA", "KEISHA", "KEITH", "KELLEY", "KELLI", "KELLIE", "KELLY", "KELLY", "KELSEY",
				"KEN", "KENDRA", "KENNETH", "KENT", "KENYA", "KERI", "KERRI", "KERRY", "KEVIN", "KIM", "KIMBERLEY",
				"KIMBERLY", "KIRK", "KIRSTEN", "KITTY", "KRIS", "KRISTA", "KRISTEN", "KRISTI", "KRISTIE", "KRISTIN",
				"KRISTINA", "KRISTINE", "KRISTY", "KRYSTAL", "KURT", "KYLE", "LACEY", "LACY", "LADONNA", "LAKEISHA",
				"LAKISHA", "LANA", "LANCE", "LARA", "LARRY", "LATASHA", "LATISHA", "LATONYA", "LATOYA", "LAURA",
				"LAUREL", "LAUREN", "LAURI", "LAURIE", "LAVERNE", "LAVONNE", "LAWANDA", "LAWRENCE", "LEA", "LEAH",
				"LEANN", "LEANNA", "LEANNE", "LEE", "LEE", "LEIGH", "LEILA", "LELA", "LELIA", "LENA", "LENORA",
				"LENORE", "LEO", "LEOLA", "LEON", "LEONA", "LEONARD", "LEONOR", "LEROY", "LESA", "LESLEY", "LESLIE",
				"LESLIE", "LESSIE", "LESTER", "LETA", "LETHA", "LETICIA", "LETITIA", "LEWIS", "LIDIA", "LILA", "LILIA",
				"LILIAN", "LILIANA", "LILLIAN", "LILLIE", "LILLY", "LILY", "LINA", "LINDA", "LINDSAY", "LINDSEY",
				"LISA", "LIZ", "LIZA", "LIZZIE", "LLOYD", "LOIS", "LOLA", "LOLITA", "LONNIE", "LORA", "LORAINE",
				"LORENA", "LORENE", "LORETTA", "LORI", "LORIE", "LORNA", "LORRAINE", "LORRIE", "LOTTIE", "LOU",
				"LOUELLA", "LOUIS", "LOUISA", "LOUISE", "LOURDES", "LUANN", "LUCIA", "LUCILE", "LUCILLE", "LUCINDA",
				"LUCY", "LUELLA", "LUIS", "LUISA", "LULA", "LUPE", "LUZ", "LYDIA", "LYNDA", "LYNETTE", "LYNN", "LYNNE",
				"LYNNETTE", "MABEL", "MABLE", "MADELEINE", "MADELINE", "MADELYN", "MADGE", "MAE", "MAGDALENA", "MAGGIE",
				"MAI", "MALINDA", "MALLORY", "MAMIE", "MANDY", "MANUEL", "MANUELA", "MARA", "MARC", "MARCELLA", "MARCI",
				"MARCIA", "MARCIE", "MARCUS", "MARCY", "MARGARET", "MARGARITA", "MARGERY", "MARGIE", "MARGO", "MARGRET",
				"MARGUERITE", "MARI", "MARIA", "MARIAN", "MARIANA", "MARIANNE", "MARIBEL", "MARICELA", "MARIE",
				"MARIETTA", "MARILYN", "MARINA", "MARIO", "MARION", "MARION", "MARISA", "MARISOL", "MARISSA", "MARITZA",
				"MARJORIE", "MARK", "MARLA", "MARLENE", "MARQUITA", "MARSHA", "MARSHALL", "MARTA", "MARTHA", "MARTIN",
				"MARTINA", "MARVA", "MARVIN", "MARY", "MARYANN", "MARYANNE", "MARYELLEN", "MARYLOU", "MATHEW",
				"MATILDA", "MATTHEW", "MATTIE", "MAUDE", "MAURA", "MAUREEN", "MAURICE", "MAVIS", "MAX", "MAXINE", "MAY",
				"MAYRA", "MEAGAN", "MEGAN", "MEGHAN", "MELANIE", "MELBA", "MELINDA", "MELISA", "MELISSA", "MELODY",
				"MELVA", "MELVIN", "MERCEDES", "MEREDITH", "MERLE", "MIA", "MICHAEL", "MICHAEL", "MICHEAL", "MICHELE",
				"MICHELLE", "MIGUEL", "MIKE", "MILAGROS", "MILDRED", "MILLICENT", "MILLIE", "MILTON", "MINDY",
				"MINERVA", "MINNIE", "MIRANDA", "MIRIAM", "MISTY", "MITCHELL", "MITZI", "MOLLIE", "MOLLY", "MONA",
				"MONICA", "MONIQUE", "MORGAN", "MORRIS", "MURIEL", "MYRA", "MYRNA", "MYRTLE", "NADIA", "NADINE",
				"NANCY", "NANETTE", "NANNIE", "NAOMI", "NATALIA", "NATALIE", "NATASHA", "NATHAN", "NATHANIEL", "NEIL",
				"NELDA", "NELL", "NELLIE", "NELSON", "NETTIE", "NEVA", "NICHOLAS", "NICHOLE", "NICOLE", "NIKKI", "NINA",
				"NITA", "NOELLE", "NOEMI", "NOLA", "NONA", "NORA", "NOREEN", "NORMA", "NORMAN", "ODESSA", "OFELIA",
				"OLA", "OLGA", "OLIVE", "OLIVIA", "OLLIE", "OPAL", "OPHELIA", "ORA", "OSCAR", "PAIGE", "PAM", "PAMELA",
				"PANSY", "PAT", "PATRICA", "PATRICE", "PATRICIA", "PATRICK", "PATSY", "PATTI", "PATTY", "PAUL", "PAULA",
				"PAULETTE", "PAULINE", "PEARL", "PEARLIE", "PEDRO", "PEGGY", "PENELOPE", "PENNY", "PERRY", "PETER",
				"PETRA", "PHILIP", "PHILLIP", "PHOEBE", "PHYLLIS", "POLLY", "PRISCILLA", "QUEEN", "RACHAEL", "RACHEL",
				"RACHELLE", "RAE", "RAFAEL", "RALPH", "RAMON", "RAMONA", "RANDALL", "RANDI", "RANDY", "RAQUEL", "RAUL",
				"RAY", "RAYMOND", "REBA", "REBECCA", "REBEKAH", "REGINA", "REGINALD", "RENA", "RENE", "RENE", "RENEE",
				"REVA", "REYNA", "RHEA", "RHODA", "RHONDA", "RICARDO", "RICHARD", "RICK", "RICKY", "RITA", "ROBBIE",
				"ROBERT", "ROBERT", "ROBERTA", "ROBERTO", "ROBIN", "ROBYN", "ROCHELLE", "RODNEY", "ROGER", "ROLAND",
				"RON", "RONALD", "RONDA", "RONNIE", "ROSA", "ROSALIE", "ROSALIND", "ROSALINDA", "ROSALYN", "ROSANNA",
				"ROSANNE", "ROSARIO", "ROSE", "ROSEANN", "ROSELLA", "ROSEMARIE", "ROSEMARY", "ROSETTA", "ROSIE",
				"ROSLYN", "ROSS", "ROWENA", "ROXANNE", "ROXIE", "ROY", "RUBEN", "RUBY", "RUSSELL", "RUTH", "RUTHIE",
				"RYAN", "SABRINA", "SADIE", "SALLIE", "SALLY", "SALVADOR", "SAM", "SAMANTHA", "SAMUEL", "SANDRA",
				"SANDY", "SARA", "SARAH", "SASHA", "SAUNDRA", "SAVANNAH", "SCOTT", "SEAN", "SELENA", "SELMA", "SERENA",
				"SERGIO", "SETH", "SHANA", "SHANE", "SHANNA", "SHANNON", "SHARI", "SHARLENE", "SHARON", "SHARRON",
				"SHAUNA", "SHAWN", "SHAWN", "SHAWNA", "SHEENA", "SHEILA", "SHELBY", "SHELIA", "SHELLEY", "SHELLY",
				"SHEREE", "SHERI", "SHERRI", "SHERRIE", "SHERRY", "SHERYL", "SHIRLEY", "SIDNEY", "SILVIA", "SIMONE",
				"SOCORRO", "SOFIA", "SONDRA", "SONIA", "SONJA", "SONYA", "SOPHIA", "SOPHIE", "STACEY", "STACI",
				"STACIE", "STACY", "STANLEY", "STEFANIE", "STELLA", "STEPHANIE", "STEPHEN", "STEVE", "STEVEN", "SUE",
				"SUMMER", "SUSAN", "SUSANA", "SUSANNA", "SUSANNE", "SUSIE", "SUZANNE", "SUZETTE", "SYBIL", "SYLVIA",
				"TABATHA", "TABITHA", "TAMARA", "TAMEKA", "TAMERA", "TAMI", "TAMIKA", "TAMMI", "TAMMIE", "TAMMY",
				"TAMRA", "TANIA", "TANISHA", "TANYA", "TARA", "TASHA", "TAYLOR", "TED", "TERESA", "TERI", "TERRA",
				"TERRANCE", "TERRENCE", "TERRI", "TERRIE", "TERRY", "TERRY", "TESSA", "THELMA", "THEODORE", "THERESA",
				"THERESE", "THOMAS", "TIA", "TIFFANY", "TIM", "TIMOTHY", "TINA", "TISHA", "TODD", "TOM", "TOMMIE",
				"TOMMY", "TONI", "TONIA", "TONY", "TONYA", "TRACEY", "TRACI", "TRACIE", "TRACY", "TRACY", "TRAVIS",
				"TRICIA", "TRINA", "TRISHA", "TROY", "TRUDY", "TWILA", "TYLER", "TYRONE", "URSULA", "VALARIE",
				"VALERIA", "VALERIE", "VANESSA", "VELMA", "VERA", "VERNA", "VERNON", "VERONICA", "VICKI", "VICKIE",
				"VICKY", "VICTOR", "VICTORIA", "VILMA", "VINCENT", "VIOLA", "VIOLET", "VIRGIE", "VIRGIL", "VIRGINIA",
				"VIVIAN", "VONDA", "WADE", "WALLACE", "WALTER", "WANDA", "WARREN", "WAYNE", "WENDI", "WENDY", "WESLEY",
				"WHITNEY", "WILDA", "WILLA", "WILLARD", "WILLIAM", "WILLIE", "WILLIE", "WILMA", "WINIFRED", "WINNIE",
				"YESENIA", "YOLANDA", "YOUNG", "YVETTE", "YVONNE", "ZACHARY", "ZELMA" };
		return senderFirstNames[new Random().nextInt(senderFirstNames.length)];
	}

	/**
	 * This function returns random sender middle name
	 */
	public static String generateRandomSenderMiddleName() {
		String senderMiddleNames[] = { "I" };
		return senderMiddleNames[new Random().nextInt(senderMiddleNames.length)];
	}

	/**
	 * This function returns random sender last name
	 */
	public static String generateRandomSenderLastName() {
		String senederLastNames[] = { "ABBOTT", "ACEVEDO", "ACOSTA", "ADAMS", "ADKINS", "AGUILAR", "AGUIRRE", "ALBERT",
				"ALEXANDER", "ALFORD", "ALLEN", "ALLISON", "ALSTON", "ALVARADO", "ALVAREZ", "ANDERSON", "ANDREWS",
				"ANTHONY", "ARMSTRONG", "ARNOLD", "ASHLEY", "ATKINS", "ATKINSON", "AUSTIN", "AVERY", "AVILA", "AYALA",
				"AYERS", "BAILEY", "BAIRD", "BAKER", "BALDWIN", "BALL", "BALLARD", "BANKS", "BARBER", "BARKER",
				"BARLOW", "BARNES", "BARNETT", "BARR", "BARRERA", "BARRETT", "BARRON", "BARRY", "BARTLETT", "BARTON",
				"BASS", "BATES", "BATTLE", "BAUER", "BAXTER", "BEACH", "BEAN", "BEARD", "BEASLEY", "BECK", "BECKER",
				"BELL", "BENDER", "BENJAMIN", "BENNETT", "BENSON", "BENTLEY", "BENTON", "BERG", "BERGER", "BERNARD",
				"BERRY", "BEST", "BIRD", "BISHOP", "BLACK", "BLACKBURN", "BLACKWELL", "BLAIR", "BLAKE", "BLANCHARD",
				"BLANKENSHIP", "BLEVINS", "BOLTON", "BOND", "BONNER", "BOOKER", "BOONE", "BOOTH", "BOWEN", "BOWERS",
				"BOWMAN", "BOYD", "BOYER", "BOYLE", "BRADFORD", "BRADLEY", "BRADSHAW", "BRADY", "BRANCH", "BRAY",
				"BRENNAN", "BREWER", "BRIDGES", "BRIGGS", "BRIGHT", "BRITT", "BROCK", "BROOKS", "BROWN", "BROWNING",
				"BRUCE", "BRYAN", "BRYANT", "BUCHANAN", "BUCK", "BUCKLEY", "BUCKNER", "BULLOCK", "BURCH", "BURGESS",
				"BURKE", "BURKS", "BURNETT", "BURNS", "BURRIS", "BURT", "BURTON", "BUSH", "BUTLER", "BYERS", "BYRD",
				"CABRERA", "CAIN", "CALDERON", "CALDWELL", "CALHOUN", "CALLAHAN", "CAMACHO", "CAMERON", "CAMPBELL",
				"CAMPOS", "CANNON", "CANTRELL", "CANTU", "CARDENAS", "CAREY", "CARLSON", "CARNEY", "CARPENTER", "CARR",
				"CARRILLO", "CARROLL", "CARSON", "CARTER", "CARVER", "CASE", "CASEY", "CASH", "CASTANEDA", "CASTILLO",
				"CASTRO", "CERVANTES", "CHAMBERS", "CHAN", "CHANDLER", "CHANEY", "CHANG", "CHAPMAN", "CHARLES", "CHASE",
				"CHAVEZ", "CHEN", "CHERRY", "CHRISTENSEN", "CHRISTIAN", "CHURCH", "CLARK", "CLARKE", "CLAY", "CLAYTON",
				"CLEMENTS", "CLEMONS", "CLEVELAND", "CLINE", "COBB", "COCHRAN", "COFFEY", "COHEN", "COLE", "COLEMAN",
				"COLLIER", "COLLINS", "COLON", "COMBS", "COMPTON", "CONLEY", "CONNER", "CONRAD", "CONTRERAS", "CONWAY",
				"COOK", "COOKE", "COOLEY", "COOPER", "COPELAND", "CORTEZ", "COTE", "COTTON", "COX", "CRAFT", "CRAIG",
				"CRANE", "CRAWFORD", "CROSBY", "CROSS", "CRUZ", "CUMMINGS", "CUNNINGHAM", "CURRY", "CURTIS", "DALE",
				"DALTON", "DANIEL", "DANIELS", "DAUGHERTY", "DAVENPORT", "DAVID", "DAVIDSON", "DAVIS", "DAWSON", "DAY",
				"DEAN", "DECKER", "DEJESUS", "DELACRUZ", "DELANEY", "DELEON", "DELGADO", "DENNIS", "DIAZ", "DICKERSON",
				"DICKSON", "DILLARD", "DILLON", "DIXON", "DODSON", "DOMINGUEZ", "DONALDSON", "DONOVAN", "DORSEY",
				"DOTSON", "DOUGLAS", "DOWNS", "DOYLE", "DRAKE", "DUDLEY", "DUFFY", "DUKE", "DUNCAN", "DUNLAP", "DUNN",
				"DURAN", "DURHAM", "DYER", "EATON", "EDWARDS", "ELLIOTT", "ELLIS", "ELLISON", "EMERSON", "ENGLAND",
				"ENGLISH", "ERICKSON", "ESPINOZA", "ESTES", "ESTRADA", "EVANS", "EVERETT", "EWING", "FARLEY", "FARMER",
				"FARRELL", "FAULKNER", "FERGUSON", "FERNANDEZ", "FERRELL", "FIELDS", "FIGUEROA", "FINCH", "FINLEY",
				"FISCHER", "FISHER", "FITZGERALD", "FITZPATRICK", "FLEMING", "FLETCHER", "FLORES", "FLOWERS", "FLOYD",
				"FLYNN", "FOLEY", "FORBES", "FORD", "FOREMAN", "FOSTER", "FOWLER", "FOX", "FRANCIS", "FRANCO", "FRANK",
				"FRANKLIN", "FRANKS", "FRAZIER", "FREDERICK", "FREEMAN", "FRENCH", "FROST", "FRY", "FRYE", "FUENTES",
				"FULLER", "FULTON", "GAINES", "GALLAGHER", "GALLEGOS", "GALLOWAY", "GAMBLE", "GARCIA", "GARDNER",
				"GARNER", "GARRETT", "GARRISON", "GARZA", "GATES", "GAY", "GENTRY", "GEORGE", "GIBBS", "GIBSON",
				"GILBERT", "GILES", "GILL", "GILLESPIE", "GILLIAM", "GILMORE", "GLASS", "GLENN", "GLOVER", "GOFF",
				"GOLDEN", "GOMEZ", "GONZALES", "GONZALEZ", "GOOD", "GOODMAN", "GOODWIN", "GORDON", "GOULD", "GRAHAM",
				"GRANT", "GRAVES", "GRAY", "GREEN", "GREENE", "GREER", "GREGORY", "GRIFFIN", "GRIFFITH", "GRIMES",
				"GROSS", "GUERRA", "GUERRERO", "GUTHRIE", "GUTIERREZ", "GUY", "GUZMAN", "HAHN", "HALE", "HALEY", "HALL",
				"HAMILTON", "HAMMOND", "HAMPTON", "HANCOCK", "HANEY", "HANSEN", "HANSON", "HARDIN", "HARDING", "HARDY",
				"HARMON", "HARPER", "HARRELL", "HARRINGTON", "HARRIS", "HARRISON", "HART", "HARTMAN", "HARVEY",
				"HATFIELD", "HAWKINS", "HAYDEN", "HAYES", "HAYNES", "HAYS", "HEAD", "HEATH", "HEBERT", "HENDERSON",
				"HENDRICKS", "HENDRIX", "HENRY", "HENSLEY", "HENSON", "HERMAN", "HERNANDEZ", "HERRERA", "HERRING",
				"HESS", "HESTER", "HEWITT", "HICKMAN", "HICKS", "HIGGINS", "HILL", "HINES", "HINTON", "HOBBS", "HODGE",
				"HODGES", "HOFFMAN", "HOGAN", "HOLCOMB", "HOLDEN", "HOLDER", "HOLLAND", "HOLLOWAY", "HOLMAN", "HOLMES",
				"HOLT", "HOOD", "HOOPER", "HOOVER", "HOPKINS", "HOPPER", "HORN", "HORNE", "HORTON", "HOUSE", "HOUSTON",
				"HOWARD", "HOWE", "HOWELL", "HUBBARD", "HUBER", "HUDSON", "HUFF", "HUFFMAN", "HUGHES", "HULL",
				"HUMPHREY", "HUNT", "HUNTER", "HURLEY", "HURST", "HUTCHINSON", "HYDE", "INGRAM", "IRWIN", "JACKSON",
				"JACOBS", "JACOBSON", "JAMES", "JARVIS", "JEFFERSON", "JENKINS", "JENNINGS", "JENSEN", "JIMENEZ",
				"JOHNS", "JOHNSON", "JOHNSTON", "JONES", "JORDAN", "JOSEPH", "JOYCE", "JOYNER", "JUAREZ", "JUSTICE",
				"KANE", "KAUFMAN", "KEITH", "KELLER", "KELLEY", "KELLY", "KEMP", "KENNEDY", "KENT", "KERR", "KEY",
				"KIDD", "KIM", "KING", "KINNEY", "KIRBY", "KIRK", "KIRKLAND", "KLEIN", "KLINE", "KNAPP", "KNIGHT",
				"KNOWLES", "KNOX", "KOCH", "KRAMER", "LAMB", "LAMBERT", "LANCASTER", "LANDRY", "LANE", "LANG",
				"LANGLEY", "LARA", "LARSEN", "LARSON", "LAWRENCE", "LAWSON", "LE", "LEACH", "LEBLANC", "LEE", "LEON",
				"LEONARD", "LESTER", "LEVINE", "LEVY", "LEWIS", "LINDSAY", "LINDSEY", "LITTLE", "LIVINGSTON", "LLOYD",
				"LOGAN", "LONG", "LOPEZ", "LOTT", "LOVE", "LOWE", "LOWERY", "LUCAS", "LUNA", "LYNCH", "LYNN", "LYONS",
				"MACDONALD", "MACIAS", "MACK", "MADDEN", "MADDOX", "MALDONADO", "MALONE", "MANN", "MANNING", "MARKS",
				"MARQUEZ", "MARSH", "MARSHALL", "MARTIN", "MARTINEZ", "MASON", "MASSEY", "MATHEWS", "MATHIS",
				"MATTHEWS", "MAXWELL", "MAY", "MAYER", "MAYNARD", "MAYO", "MAYS", "MCBRIDE", "MCCALL", "MCCARTHY",
				"MCCARTY", "MCCLAIN", "MCCLURE", "MCCONNELL", "MCCORMICK", "MCCOY", "MCCRAY", "MCCULLOUGH", "MCDANIEL",
				"MCDONALD", "MCDOWELL", "MCFADDEN", "MCFARLAND", "MCGEE", "MCGOWAN", "MCGUIRE", "MCINTOSH", "MCINTYRE",
				"MCKAY", "MCKEE", "MCKENZIE", "MCKINNEY", "MCKNIGHT", "MCLAUGHLIN", "MCLEAN", "MCLEOD", "MCMAHON",
				"MCMILLAN", "MCNEIL", "MCPHERSON", "MEADOWS", "MEDINA", "MEJIA", "MELENDEZ", "MELTON", "MENDEZ",
				"MENDOZA", "MERCADO", "MERCER", "MERRILL", "MERRITT", "MEYER", "MEYERS", "MICHAEL", "MIDDLETON",
				"MILES", "MILLER", "MILLS", "MIRANDA", "MITCHELL", "MOLINA", "MONROE", "MONTGOMERY", "MONTOYA", "MOODY",
				"MOON", "MOONEY", "MOORE", "MORALES", "MORAN", "MORENO", "MORGAN", "MORIN", "MORRIS", "MORRISON",
				"MORROW", "MORSE", "MORTON", "MOSES", "MOSLEY", "MOSS", "MUELLER", "MULLEN", "MULLINS", "MUNOZ",
				"MURPHY", "MURRAY", "MYERS", "NASH", "NAVARRO", "NEAL", "NELSON", "NEWMAN", "NEWTON", "NGUYEN",
				"NICHOLS", "NICHOLSON", "NIELSEN", "NIEVES", "NIXON", "NOBLE", "NOEL", "NOLAN", "NORMAN", "NORRIS",
				"NORTON", "NUNEZ", "OBRIEN", "OCHOA", "OCONNOR", "ODOM", "ODONNELL", "OLIVER", "OLSEN", "OLSON",
				"ONEAL", "ONEIL", "ONEILL", "ORR", "ORTEGA", "ORTIZ", "OSBORN", "OSBORNE", "OWEN", "OWENS", "PACE",
				"PACHECO", "PADILLA", "PAGE", "PALMER", "PARK", "PARKER", "PARKS", "PARRISH", "PARSONS", "PATE",
				"PATEL", "PATRICK", "PATTERSON", "PATTON", "PAUL", "PAYNE", "PEARSON", "PECK", "PENA", "PENNINGTON",
				"PEREZ", "PERKINS", "PERRY", "PETERS", "PETERSEN", "PETERSON", "PETTY", "PHELPS", "PHILLIPS", "PICKETT",
				"PIERCE", "PITTMAN", "PITTS", "POLLARD", "POOLE", "POPE", "PORTER", "POTTER", "POTTS", "POWELL",
				"POWERS", "PRATT", "PRESTON", "PRICE", "PRINCE", "PRUITT", "PUCKETT", "PUGH", "QUINN", "RAMIREZ",
				"RAMOS", "RAMSEY", "RANDALL", "RANDOLPH", "RASMUSSEN", "RATLIFF", "RAY", "RAYMOND", "REED", "REESE",
				"REEVES", "REID", "REILLY", "REYES", "REYNOLDS", "RHODES", "RICE", "RICH", "RICHARD", "RICHARDS",
				"RICHARDSON", "RICHMOND", "RIDDLE", "RIGGS", "RILEY", "RIOS", "RIVAS", "RIVERA", "RIVERS", "ROACH",
				"ROBBINS", "ROBERSON", "ROBERTS", "ROBERTSON", "ROBINSON", "ROBLES", "ROCHA", "RODGERS", "RODRIGUEZ",
				"RODRIQUEZ", "ROGERS", "ROJAS", "ROLLINS", "ROMAN", "ROMERO", "ROSA", "ROSALES", "ROSARIO", "ROSE",
				"ROSS", "ROTH", "ROWE", "ROWLAND", "ROY", "RUIZ", "RUSH", "RUSSELL", "RUSSO", "RUTLEDGE", "RYAN",
				"SALAS", "SALAZAR", "SALINAS", "SAMPSON", "SANCHEZ", "SANDERS", "SANDOVAL", "SANFORD", "SANTANA",
				"SANTIAGO", "SANTOS", "SARGENT", "SAUNDERS", "SAVAGE", "SAWYER", "SCHMIDT", "SCHNEIDER", "SCHROEDER",
				"SCHULTZ", "SCHWARTZ", "SCOTT", "SEARS", "SELLERS", "SERRANO", "SEXTON", "SHAFFER", "SHANNON", "SHARP",
				"SHARPE", "SHAW", "SHELTON", "SHEPARD", "SHEPHERD", "SHEPPARD", "SHERMAN", "SHIELDS", "SHORT", "SILVA",
				"SIMMONS", "SIMON", "SIMPSON", "SIMS", "SINGLETON", "SKINNER", "SLATER", "SLOAN", "SMALL", "SMITH",
				"SNIDER", "SNOW", "SNYDER", "SOLIS", "SOLOMON", "SOSA", "SOTO", "SPARKS", "SPEARS", "SPENCE", "SPENCER",
				"STAFFORD", "STANLEY", "STANTON", "STARK", "STEELE", "STEIN", "STEPHENS", "STEPHENSON", "STEVENS",
				"STEVENSON", "STEWART", "STOKES", "STONE", "STOUT", "STRICKLAND", "STRONG", "STUART", "SUAREZ",
				"SULLIVAN", "SUMMERS", "SUTTON", "SWANSON", "SWEENEY", "SWEET", "SYKES", "TALLEY", "TANNER", "TATE",
				"TAYLOR", "TERRELL", "TERRY", "THOMAS", "THOMPSON", "THORNTON", "TILLMAN", "TODD", "TORRES", "TOWNSEND",
				"TRAN", "TRAVIS", "TREVINO", "TRUJILLO", "TUCKER", "TURNER", "TYLER", "TYSON", "UNDERWOOD", "VALDEZ",
				"VALENCIA", "VALENTINE", "VALENZUELA", "VANCE", "VANG", "VARGAS", "VASQUEZ", "VAUGHAN", "VAUGHN",
				"VAZQUEZ", "VEGA", "VELASQUEZ", "VELAZQUEZ", "VELEZ", "VILLARREAL", "VINCENT", "VINSON", "WADE",
				"WAGNER", "WALKER", "WALL", "WALLACE", "WALLER", "WALLS", "WALSH", "WALTER", "WALTERS", "WALTON",
				"WARD", "WARE", "WARNER", "WARREN", "WASHINGTON", "WATERS", "WATKINS", "WATSON", "WATTS", "WEAVER",
				"WEBB", "WEBER", "WEBSTER", "WEEKS", "WEISS", "WELCH", "WELLS", "WEST", "WHEELER", "WHITAKER", "WHITE",
				"WHITEHEAD", "WHITFIELD", "WHITLEY", "WHITNEY", "WIGGINS", "WILCOX", "WILDER", "WILEY", "WILKERSON",
				"WILKINS", "WILKINSON", "WILLIAM", "WILLIAMS", "WILLIAMSON", "WILLIS", "WILSON", "WINTERS", "WISE",
				"WITT", "WOLF", "WOLFE", "WONG", "WOOD", "WOODARD", "WOODS", "WOODWARD", "WOOTEN", "WORKMAN", "WRIGHT",
				"WYATT", "WYNN", "YANG", "YATES", "YORK", "YOUNG", "ZAMORA", "ZIMMERMAN" };
		return senederLastNames[new Random().nextInt(senederLastNames.length)];
	}

	/**
	 * This function returns random sender second last name
	 */
	public static String generateRandomSenderSecondLastName() {
		String senderSecondLastNames[] = { "PEDRO", "ARTURO", "LOYOLA", "INC", "MERC", "NENA", "NEMPA", "MUCHA", "KIKA",
				"KRIKA", "BOMBA", "TWEETA", "RAMA", "CHEETA", "KOOTA", "PRADA", "INA", "MINA", "MIKA", "KAI", "KAMA",
				"REEKA", "RAM", "PAM", "POSH", "DIKA" };
		return senderSecondLastNames[new Random().nextInt(senderSecondLastNames.length)];
	}

	/**
	 * This function returns random receiver first name
	 */
	public static String generateRandomReceiverFirstName() {
		String receiverFirstNames[] = { "ABBOTT", "ACEVEDO", "ACOSTA", "ADAMS", "ADKINS", "AGUILAR", "AGUIRRE",
				"ALBERT", "ALEXANDER", "ALFORD", "ALLEN", "ALLISON", "ALSTON", "ALVARADO", "ALVAREZ", "ANDERSON",
				"ANDREWS", "ANTHONY", "ARMSTRONG", "ARNOLD", "ASHLEY", "ATKINS", "ATKINSON", "AUSTIN", "AVERY", "AVILA",
				"AYALA", "AYERS", "BAILEY", "BAIRD", "BAKER", "BALDWIN", "BALL", "BALLARD", "BANKS", "BARBER", "BARKER",
				"BARLOW", "BARNES", "BARNETT", "BARR", "BARRERA", "BARRETT", "BARRON", "BARRY", "BARTLETT", "BARTON",
				"BASS", "BATES", "BATTLE", "BAUER", "BAXTER", "BEACH", "BEAN", "BEARD", "BEASLEY", "BECK", "BECKER",
				"BELL", "BENDER", "BENJAMIN", "BENNETT", "BENSON", "BENTLEY", "BENTON", "BERG", "BERGER", "BERNARD",
				"BERRY", "BEST", "BIRD", "BISHOP", "BLACK", "BLACKBURN", "BLACKWELL", "BLAIR", "BLAKE", "BLANCHARD",
				"BLANKENSHIP", "BLEVINS", "BOLTON", "BOND", "BONNER", "BOOKER", "BOONE", "BOOTH", "BOWEN", "BOWERS",
				"BOWMAN", "BOYD", "BOYER", "BOYLE", "BRADFORD", "BRADLEY", "BRADSHAW", "BRADY", "BRANCH", "BRAY",
				"BRENNAN", "BREWER", "BRIDGES", "BRIGGS", "BRIGHT", "BRITT", "BROCK", "BROOKS", "BROWN", "BROWNING",
				"BRUCE", "BRYAN", "BRYANT", "BUCHANAN", "BUCK", "BUCKLEY", "BUCKNER", "BULLOCK", "BURCH", "BURGESS",
				"BURKE", "BURKS", "BURNETT", "BURNS", "BURRIS", "BURT", "BURTON", "BUSH", "BUTLER", "BYERS", "BYRD",
				"CABRERA", "CAIN", "CALDERON", "CALDWELL", "CALHOUN", "CALLAHAN", "CAMACHO", "CAMERON", "CAMPBELL",
				"CAMPOS", "CANNON", "CANTRELL", "CANTU", "CARDENAS", "CAREY", "CARLSON", "CARNEY", "CARPENTER", "CARR",
				"CARRILLO", "CARROLL", "CARSON", "CARTER", "CARVER", "CASE", "CASEY", "CASH", "CASTANEDA", "CASTILLO",
				"CASTRO", "CERVANTES", "CHAMBERS", "CHAN", "CHANDLER", "CHANEY", "CHANG", "CHAPMAN", "CHARLES", "CHASE",
				"CHAVEZ", "CHEN", "CHERRY", "CHRISTENSEN", "CHRISTIAN", "CHURCH", "CLARK", "CLARKE", "CLAY", "CLAYTON",
				"CLEMENTS", "CLEMONS", "CLEVELAND", "CLINE", "COBB", "COCHRAN", "COFFEY", "COHEN", "COLE", "COLEMAN",
				"COLLIER", "COLLINS", "COLON", "COMBS", "COMPTON", "CONLEY", "CONNER", "CONRAD", "CONTRERAS", "CONWAY",
				"COOK", "COOKE", "COOLEY", "COOPER", "COPELAND", "CORTEZ", "COTE", "COTTON", "COX", "CRAFT", "CRAIG",
				"CRANE", "CRAWFORD", "CROSBY", "CROSS", "CRUZ", "CUMMINGS", "CUNNINGHAM", "CURRY", "CURTIS", "DALE",
				"DALTON", "DANIEL", "DANIELS", "DAUGHERTY", "DAVENPORT", "DAVID", "DAVIDSON", "DAVIS", "DAWSON", "DAY",
				"DEAN", "DECKER", "DEJESUS", "DELACRUZ", "DELANEY", "DELEON", "DELGADO", "DENNIS", "DIAZ", "DICKERSON",
				"DICKSON", "DILLARD", "DILLON", "DIXON", "DODSON", "DOMINGUEZ", "DONALDSON", "DONOVAN", "DORSEY",
				"DOTSON", "DOUGLAS", "DOWNS", "DOYLE", "DRAKE", "DUDLEY", "DUFFY", "DUKE", "DUNCAN", "DUNLAP", "DUNN",
				"DURAN", "DURHAM", "DYER", "EATON", "EDWARDS", "ELLIOTT", "ELLIS", "ELLISON", "EMERSON", "ENGLAND",
				"ENGLISH", "ERICKSON", "ESPINOZA", "ESTES", "ESTRADA", "EVANS", "EVERETT", "EWING", "FARLEY", "FARMER",
				"FARRELL", "FAULKNER", "FERGUSON", "FERNANDEZ", "FERRELL", "FIELDS", "FIGUEROA", "FINCH", "FINLEY",
				"FISCHER", "FISHER", "FITZGERALD", "FITZPATRICK", "FLEMING", "FLETCHER", "FLORES", "FLOWERS", "FLOYD",
				"FLYNN", "FOLEY", "FORBES", "FORD", "FOREMAN", "FOSTER", "FOWLER", "FOX", "FRANCIS", "FRANCO", "FRANK",
				"FRANKLIN", "FRANKS", "FRAZIER", "FREDERICK", "FREEMAN", "FRENCH", "FROST", "FRY", "FRYE", "FUENTES",
				"FULLER", "FULTON", "GAINES", "GALLAGHER", "GALLEGOS", "GALLOWAY", "GAMBLE", "GARCIA", "GARDNER",
				"GARNER", "GARRETT", "GARRISON", "GARZA", "GATES", "GAY", "GENTRY", "GEORGE", "GIBBS", "GIBSON",
				"GILBERT", "GILES", "GILL", "GILLESPIE", "GILLIAM", "GILMORE", "GLASS", "GLENN", "GLOVER", "GOFF",
				"GOLDEN", "GOMEZ", "GONZALES", "GONZALEZ", "GOOD", "GOODMAN", "GOODWIN", "GORDON", "GOULD", "GRAHAM",
				"GRANT", "GRAVES", "GRAY", "GREEN", "GREENE", "GREER", "GREGORY", "GRIFFIN", "GRIFFITH", "GRIMES",
				"GROSS", "GUERRA", "GUERRERO", "GUTHRIE", "GUTIERREZ", "GUY", "GUZMAN", "HAHN", "HALE", "HALEY", "HALL",
				"HAMILTON", "HAMMOND", "HAMPTON", "HANCOCK", "HANEY", "HANSEN", "HANSON", "HARDIN", "HARDING", "HARDY",
				"HARMON", "HARPER", "HARRELL", "HARRINGTON", "HARRIS", "HARRISON", "HART", "HARTMAN", "HARVEY",
				"HATFIELD", "HAWKINS", "HAYDEN", "HAYES", "HAYNES", "HAYS", "HEAD", "HEATH", "HEBERT", "HENDERSON",
				"HENDRICKS", "HENDRIX", "HENRY", "HENSLEY", "HENSON", "HERMAN", "HERNANDEZ", "HERRERA", "HERRING",
				"HESS", "HESTER", "HEWITT", "HICKMAN", "HICKS", "HIGGINS", "HILL", "HINES", "HINTON", "HOBBS", "HODGE",
				"HODGES", "HOFFMAN", "HOGAN", "HOLCOMB", "HOLDEN", "HOLDER", "HOLLAND", "HOLLOWAY", "HOLMAN", "HOLMES",
				"HOLT", "HOOD", "HOOPER", "HOOVER", "HOPKINS", "HOPPER", "HORN", "HORNE", "HORTON", "HOUSE", "HOUSTON",
				"HOWARD", "HOWE", "HOWELL", "HUBBARD", "HUBER", "HUDSON", "HUFF", "HUFFMAN", "HUGHES", "HULL",
				"HUMPHREY", "HUNT", "HUNTER", "HURLEY", "HURST", "HUTCHINSON", "HYDE", "INGRAM", "IRWIN", "JACKSON",
				"JACOBS", "JACOBSON", "JAMES", "JARVIS", "JEFFERSON", "JENKINS", "JENNINGS", "JENSEN", "JIMENEZ",
				"JOHNS", "JOHNSON", "JOHNSTON", "JONES", "JORDAN", "JOSEPH", "JOYCE", "JOYNER", "JUAREZ", "JUSTICE",
				"KANE", "KAUFMAN", "KEITH", "KELLER", "KELLEY", "KELLY", "KEMP", "KENNEDY", "KENT", "KERR", "KEY",
				"KIDD", "KIM", "KING", "KINNEY", "KIRBY", "KIRK", "KIRKLAND", "KLEIN", "KLINE", "KNAPP", "KNIGHT",
				"KNOWLES", "KNOX", "KOCH", "KRAMER", "LAMB", "LAMBERT", "LANCASTER", "LANDRY", "LANE", "LANG",
				"LANGLEY", "LARA", "LARSEN", "LARSON", "LAWRENCE", "LAWSON", "LE", "LEACH", "LEBLANC", "LEE", "LEON",
				"LEONARD", "LESTER", "LEVINE", "LEVY", "LEWIS", "LINDSAY", "LINDSEY", "LITTLE", "LIVINGSTON", "LLOYD",
				"LOGAN", "LONG", "LOPEZ", "LOTT", "LOVE", "LOWE", "LOWERY", "LUCAS", "LUNA", "LYNCH", "LYNN", "LYONS",
				"MACDONALD", "MACIAS", "MACK", "MADDEN", "MADDOX", "MALDONADO", "MALONE", "MANN", "MANNING", "MARKS",
				"MARQUEZ", "MARSH", "MARSHALL", "MARTIN", "MARTINEZ", "MASON", "MASSEY", "MATHEWS", "MATHIS",
				"MATTHEWS", "MAXWELL", "MAY", "MAYER", "MAYNARD", "MAYO", "MAYS", "MCBRIDE", "MCCALL", "MCCARTHY",
				"MCCARTY", "MCCLAIN", "MCCLURE", "MCCONNELL", "MCCORMICK", "MCCOY", "MCCRAY", "MCCULLOUGH", "MCDANIEL",
				"MCDONALD", "MCDOWELL", "MCFADDEN", "MCFARLAND", "MCGEE", "MCGOWAN", "MCGUIRE", "MCINTOSH", "MCINTYRE",
				"MCKAY", "MCKEE", "MCKENZIE", "MCKINNEY", "MCKNIGHT", "MCLAUGHLIN", "MCLEAN", "MCLEOD", "MCMAHON",
				"MCMILLAN", "MCNEIL", "MCPHERSON", "MEADOWS", "MEDINA", "MEJIA", "MELENDEZ", "MELTON", "MENDEZ",
				"MENDOZA", "MERCADO", "MERCER", "MERRILL", "MERRITT", "MEYER", "MEYERS", "MICHAEL", "MIDDLETON",
				"MILES", "MILLER", "MILLS", "MIRANDA", "MITCHELL", "MOLINA", "MONROE", "MONTGOMERY", "MONTOYA", "MOODY",
				"MOON", "MOONEY", "MOORE", "MORALES", "MORAN", "MORENO", "MORGAN", "MORIN", "MORRIS", "MORRISON",
				"MORROW", "MORSE", "MORTON", "MOSES", "MOSLEY", "MOSS", "MUELLER", "MULLEN", "MULLINS", "MUNOZ",
				"MURPHY", "MURRAY", "MYERS", "NASH", "NAVARRO", "NEAL", "NELSON", "NEWMAN", "NEWTON", "NGUYEN",
				"NICHOLS", "NICHOLSON", "NIELSEN", "NIEVES", "NIXON", "NOBLE", "NOEL", "NOLAN", "NORMAN", "NORRIS",
				"NORTON", "NUNEZ", "OBRIEN", "OCHOA", "OCONNOR", "ODOM", "ODONNELL", "OLIVER", "OLSEN", "OLSON",
				"ONEAL", "ONEIL", "ONEILL", "ORR", "ORTEGA", "ORTIZ", "OSBORN", "OSBORNE", "OWEN", "OWENS", "PACE",
				"PACHECO", "PADILLA", "PAGE", "PALMER", "PARK", "PARKER", "PARKS", "PARRISH", "PARSONS", "PATE",
				"PATEL", "PATRICK", "PATTERSON", "PATTON", "PAUL", "PAYNE", "PEARSON", "PECK", "PENA", "PENNINGTON",
				"PEREZ", "PERKINS", "PERRY", "PETERS", "PETERSEN", "PETERSON", "PETTY", "PHELPS", "PHILLIPS", "PICKETT",
				"PIERCE", "PITTMAN", "PITTS", "POLLARD", "POOLE", "POPE", "PORTER", "POTTER", "POTTS", "POWELL",
				"POWERS", "PRATT", "PRESTON", "PRICE", "PRINCE", "PRUITT", "PUCKETT", "PUGH", "QUINN", "RAMIREZ",
				"RAMOS", "RAMSEY", "RANDALL", "RANDOLPH", "RASMUSSEN", "RATLIFF", "RAY", "RAYMOND", "REED", "REESE",
				"REEVES", "REID", "REILLY", "REYES", "REYNOLDS", "RHODES", "RICE", "RICH", "RICHARD", "RICHARDS",
				"RICHARDSON", "RICHMOND", "RIDDLE", "RIGGS", "RILEY", "RIOS", "RIVAS", "RIVERA", "RIVERS", "ROACH",
				"ROBBINS", "ROBERSON", "ROBERTS", "ROBERTSON", "ROBINSON", "ROBLES", "ROCHA", "RODGERS", "RODRIGUEZ",
				"RODRIQUEZ", "ROGERS", "ROJAS", "ROLLINS", "ROMAN", "ROMERO", "ROSA", "ROSALES", "ROSARIO", "ROSE",
				"ROSS", "ROTH", "ROWE", "ROWLAND", "ROY", "RUIZ", "RUSH", "RUSSELL", "RUSSO", "RUTLEDGE", "RYAN",
				"SALAS", "SALAZAR", "SALINAS", "SAMPSON", "SANCHEZ", "SANDERS", "SANDOVAL", "SANFORD", "SANTANA",
				"SANTIAGO", "SANTOS", "SARGENT", "SAUNDERS", "SAVAGE", "SAWYER", "SCHMIDT", "SCHNEIDER", "SCHROEDER",
				"SCHULTZ", "SCHWARTZ", "SCOTT", "SEARS", "SELLERS", "SERRANO", "SEXTON", "SHAFFER", "SHANNON", "SHARP",
				"SHARPE", "SHAW", "SHELTON", "SHEPARD", "SHEPHERD", "SHEPPARD", "SHERMAN", "SHIELDS", "SHORT", "SILVA",
				"SIMMONS", "SIMON", "SIMPSON", "SIMS", "SINGLETON", "SKINNER", "SLATER", "SLOAN", "SMALL", "SMITH",
				"SNIDER", "SNOW", "SNYDER", "SOLIS", "SOLOMON", "SOSA", "SOTO", "SPARKS", "SPEARS", "SPENCE", "SPENCER",
				"STAFFORD", "STANLEY", "STANTON", "STARK", "STEELE", "STEIN", "STEPHENS", "STEPHENSON", "STEVENS",
				"STEVENSON", "STEWART", "STOKES", "STONE", "STOUT", "STRICKLAND", "STRONG", "STUART", "SUAREZ",
				"SULLIVAN", "SUMMERS", "SUTTON", "SWANSON", "SWEENEY", "SWEET", "SYKES", "TALLEY", "TANNER", "TATE",
				"TAYLOR", "TERRELL", "TERRY", "THOMAS", "THOMPSON", "THORNTON", "TILLMAN", "TODD", "TORRES", "TOWNSEND",
				"TRAN", "TRAVIS", "TREVINO", "TRUJILLO", "TUCKER", "TURNER", "TYLER", "TYSON", "UNDERWOOD", "VALDEZ",
				"VALENCIA", "VALENTINE", "VALENZUELA", "VANCE", "VANG", "VARGAS", "VASQUEZ", "VAUGHAN", "VAUGHN",
				"VAZQUEZ", "VEGA", "VELASQUEZ", "VELAZQUEZ", "VELEZ", "VILLARREAL", "VINCENT", "VINSON", "WADE",
				"WAGNER", "WALKER", "WALL", "WALLACE", "WALLER", "WALLS", "WALSH", "WALTER", "WALTERS", "WALTON",
				"WARD", "WARE", "WARNER", "WARREN", "WASHINGTON", "WATERS", "WATKINS", "WATSON", "WATTS", "WEAVER",
				"WEBB", "WEBER", "WEBSTER", "WEEKS", "WEISS", "WELCH", "WELLS", "WEST", "WHEELER", "WHITAKER", "WHITE",
				"WHITEHEAD", "WHITFIELD", "WHITLEY", "WHITNEY", "WIGGINS", "WILCOX", "WILDER", "WILEY", "WILKERSON",
				"WILKINS", "WILKINSON", "WILLIAM", "WILLIAMS", "WILLIAMSON", "WILLIS", "WILSON", "WINTERS", "WISE",
				"WITT", "WOLF", "WOLFE", "WONG", "WOOD", "WOODARD", "WOODS", "WOODWARD", "WOOTEN", "WORKMAN", "WRIGHT",
				"WYATT", "WYNN", "YANG", "YATES", "YORK", "YOUNG", "ZAMORA", "ZIMMERMAN" };
		return receiverFirstNames[new Random().nextInt(receiverFirstNames.length)];
	}

	/**
	 * This function returns random receiver last name
	 */
	public static String generateRandomReceiverLastName() {
		String receiverLastNames[] = { "AARON", "ABBY", "ABIGAIL", "ADA", "ADAM", "ADDIE", "ADELA", "ADELE", "ADELINE",
				"ADRIAN", "ADRIAN", "ADRIANA", "ADRIENNE", "AGNES", "AIDA", "AILEEN", "AIMEE", "AISHA", "ALAN", "ALANA",
				"ALBA", "ALBERT", "ALBERTA", "ALBERTO", "ALEJANDRA", "ALEX", "ALEXANDER", "ALEXANDRA", "ALEXANDRIA",
				"ALEXIS", "ALFRED", "ALFREDA", "ALFREDO", "ALICE", "ALICIA", "ALINE", "ALISA", "ALISHA", "ALISON",
				"ALISSA", "ALLAN", "ALLEN", "ALLIE", "ALLISON", "ALLYSON", "ALMA", "ALTA", "ALTHEA", "ALVIN", "ALYCE",
				"ALYSON", "ALYSSA", "AMALIA", "AMANDA", "AMBER", "AMELIA", "AMIE", "AMPARO", "AMY", "ANA", "ANASTASIA",
				"ANDRE", "ANDREA", "ANDREW", "ANDY", "ANGEL", "ANGEL", "ANGELA", "ANGELIA", "ANGELICA", "ANGELINA",
				"ANGELINE", "ANGELIQUE", "ANGELITA", "ANGIE", "ANITA", "ANN", "ANNA", "ANNABELLE", "ANNE", "ANNETTE",
				"ANNIE", "ANNMARIE", "ANTHONY", "ANTOINETTE", "ANTONIA", "ANTONIO", "APRIL", "ARACELI", "ARLENE",
				"ARLINE", "ARMANDO", "ARNOLD", "ARTHUR", "ASHLEE", "ASHLEY", "AUDRA", "AUDREY", "AUGUSTA", "AURELIA",
				"AURORA", "AUTUMN", "AVA", "AVIS", "BARBARA", "BARBRA", "BARRY", "BEATRICE", "BEATRIZ", "BECKY",
				"BELINDA", "BEN", "BENITA", "BENJAMIN", "BERNADETTE", "BERNADINE", "BERNARD", "BERNICE", "BERTA",
				"BERTHA", "BERTIE", "BERYL", "BESSIE", "BETH", "BETHANY", "BETSY", "BETTE", "BETTIE", "BETTY", "BETTYE",
				"BEULAH", "BEVERLEY", "BEVERLY", "BIANCA", "BILL", "BILLIE", "BILLY", "BLANCA", "BLANCHE", "BOB",
				"BOBBI", "BOBBIE", "BOBBY", "BONITA", "BONNIE", "BRAD", "BRADLEY", "BRANDI", "BRANDIE", "BRANDON",
				"BRANDY", "BRENDA", "BRENT", "BRETT", "BRIAN", "BRIANA", "BRIANNA", "BRIDGET", "BRIDGETT", "BRIDGETTE",
				"BRIGITTE", "BRITNEY", "BRITTANY", "BRITTNEY", "BROOKE", "BRUCE", "BRYAN", "BYRON", "CAITLIN", "CALLIE",
				"CALVIN", "CAMILLE", "CANDACE", "CANDICE", "CANDY", "CARA", "CAREY", "CARISSA", "CARL", "CARLA",
				"CARLENE", "CARLOS", "CARLY", "CARMELA", "CARMELLA", "CARMEN", "CAROL", "CAROLE", "CAROLINA",
				"CAROLINE", "CAROLYN", "CARRIE", "CASANDRA", "CASEY", "CASEY", "CASSANDRA", "CASSIE", "CATALINA",
				"CATHERINE", "CATHLEEN", "CATHRYN", "CATHY", "CECELIA", "CECIL", "CECILE", "CECILIA", "CELESTE",
				"CELIA", "CELINA", "CHAD", "CHANDRA", "CHARITY", "CHARLENE", "CHARLES", "CHARLIE", "CHARLOTTE",
				"CHARMAINE", "CHASITY", "CHELSEA", "CHERI", "CHERIE", "CHERRY", "CHERYL", "CHESTER", "CHRIS", "CHRIS",
				"CHRISTA", "CHRISTI", "CHRISTIAN", "CHRISTIAN", "CHRISTIE", "CHRISTINA", "CHRISTINE", "CHRISTOPHER",
				"CHRISTY", "CHRYSTAL", "CINDY", "CLAIRE", "CLARA", "CLARE", "CLARENCE", "CLARICE", "CLARISSA", "CLAUDE",
				"CLAUDETTE", "CLAUDIA", "CLAUDINE", "CLAYTON", "CLEO", "CLIFFORD", "CLIFTON", "CLINTON", "CLYDE",
				"CODY", "COLEEN", "COLETTE", "COLLEEN", "CONCEPCION", "CONCETTA", "CONNIE", "CONSTANCE", "CONSUELO",
				"CORA", "COREY", "CORINA", "CORINE", "CORINNE", "CORNELIA", "CORRINE", "CORY", "COURTNEY", "CRAIG",
				"CRISTINA", "CRYSTAL", "CURTIS", "CYNTHIA", "DAISY", "DALE", "DALE", "DAN", "DANA", "DANIEL",
				"DANIELLE", "DANNY", "DAPHNE", "DARCY", "DARLA", "DARLENE", "DARRELL", "DARREN", "DARRYL", "DARYL",
				"DAVE", "DAVID", "DAWN", "DEAN", "DEANA", "DEANN", "DEANNA", "DEANNE", "DEBBIE", "DEBORA", "DEBORAH",
				"DEBRA", "DEE", "DEENA", "DEIDRE", "DEIRDRE", "DELIA", "DELLA", "DELORES", "DELORIS", "DENA", "DENISE",
				"DENNIS", "DEREK", "DERRICK", "DESIREE", "DIANA", "DIANE", "DIANN", "DIANNA", "DIANNE", "DINA",
				"DIONNE", "DIXIE", "DOLLIE", "DOLLY", "DOLORES", "DOMINIQUE", "DON", "DONA", "DONALD", "DONNA", "DORA",
				"DOREEN", "DORIS", "DOROTHEA", "DOROTHY", "DORTHY", "DOUGLAS", "DUANE", "DUSTIN", "DWAYNE", "DWIGHT",
				"EARL", "EARLENE", "EARLINE", "EARNESTINE", "EBONY", "EDDIE", "EDDIE", "EDGAR", "EDITH", "EDNA",
				"EDUARDO", "EDWARD", "EDWIN", "EDWINA", "EFFIE", "EILEEN", "ELAINE", "ELBA", "ELEANOR", "ELENA",
				"ELINOR", "ELISA", "ELISABETH", "ELISE", "ELIZA", "ELIZABETH", "ELLA", "ELLEN", "ELMA", "ELMER",
				"ELNORA", "ELOISE", "ELSA", "ELSIE", "ELVA", "ELVIA", "ELVIRA", "EMILIA", "EMILY", "EMMA", "ENID",
				"ENRIQUE", "ERIC", "ERICA", "ERICKA", "ERIK", "ERIKA", "ERIN", "ERMA", "ERNA", "ERNEST", "ERNESTINE",
				"ESMERALDA", "ESPERANZA", "ESSIE", "ESTELA", "ESTELLA", "ESTELLE", "ESTER", "ESTHER", "ETHEL", "ETTA",
				"EUGENE", "EUGENIA", "EULA", "EUNICE", "EVA", "EVANGELINA", "EVANGELINE", "EVE", "EVELYN", "EVERETT",
				"FAITH", "FANNIE", "FANNY", "FAY", "FAYE", "FELECIA", "FELICIA", "FELIX", "FERN", "FERNANDO", "FLORA",
				"FLORENCE", "FLORINE", "FLOSSIE", "FLOYD", "FRAN", "FRANCES", "FRANCESCA", "FRANCINE", "FRANCIS",
				"FRANCIS", "FRANCISCA", "FRANCISCO", "FRANK", "FRANKIE", "FRANKLIN", "FRED", "FREDA", "FREDDIE",
				"FREDERICK", "FREIDA", "FRIEDA", "GABRIEL", "GABRIELA", "GABRIELLE", "GAIL", "GALE", "GARY", "GAY",
				"GAYLE", "GENA", "GENE", "GENEVA", "GENEVIEVE", "GEORGE", "GEORGETTE", "GEORGIA", "GEORGINA", "GERALD",
				"GERALDINE", "GERTRUDE", "GILBERT", "GILDA", "GINA", "GINGER", "GLADYS", "GLEN", "GLENDA", "GLENN",
				"GLENNA", "GLORIA", "GOLDIE", "GORDON", "GRACE", "GRACIE", "GRACIELA", "GREG", "GREGORY", "GRETA",
				"GRETCHEN", "GUADALUPE", "GUY", "GWEN", "GWENDOLYN", "HALEY", "HALLIE", "HANNAH", "HAROLD", "HARRIET",
				"HARRIETT", "HARRY", "HARVEY", "HATTIE", "HAZEL", "HEATHER", "HECTOR", "HEIDI", "HELEN", "HELENA",
				"HELENE", "HELGA", "HENRIETTA", "HENRY", "HERBERT", "HERMAN", "HERMINIA", "HESTER", "HILARY", "HILDA",
				"HILLARY", "HOLLIE", "HOLLY", "HOPE", "HOWARD", "HUGH", "IAN", "IDA", "ILA", "ILENE", "IMELDA",
				"IMOGENE", "INA", "INES", "INEZ", "INGRID", "IRENE", "IRIS", "IRMA", "ISAAC", "ISABEL", "ISABELLA",
				"ISABELLE", "IVA", "IVAN", "IVY", "JACK", "JACKIE", "JACKLYN", "JACLYN", "JACOB", "JACQUELINE",
				"JACQUELYN", "JAIME", "JAIME", "JAMES", "JAMES", "JAMI", "JAMIE", "JAMIE", "JAN", "JANA", "JANE",
				"JANELL", "JANELLE", "JANET", "JANETTE", "JANICE", "JANIE", "JANINE", "JANIS", "JANNA", "JANNIE",
				"JARED", "JASMINE", "JASON", "JAVIER", "JAY", "JAYNE", "JEAN", "JEANETTE", "JEANIE", "JEANINE",
				"JEANNE", "JEANNETTE", "JEANNIE", "JEANNINE", "JEFF", "JEFFERY", "JEFFREY", "JENIFER", "JENNA",
				"JENNIE", "JENNIFER", "JENNY", "JEREMY", "JERI", "JEROME", "JERRI", "JERRY", "JERRY", "JESSE",
				"JESSICA", "JESSIE", "JESSIE", "JESUS", "JEWEL", "JEWELL", "JILL", "JILLIAN", "JIM", "JIMMIE", "JIMMIE",
				"JIMMY", "JO", "JOAN", "JOANN", "JOANNA", "JOANNE", "JOCELYN", "JODI", "JODIE", "JODY", "JOE", "JOEL",
				"JOHANNA", "JOHN", "JOHN", "JOHNNIE", "JOHNNIE", "JOHNNY", "JOLENE", "JON", "JONATHAN", "JONI",
				"JORDAN", "JORDAN", "JORGE", "JOSE", "JOSEFA", "JOSEFINA", "JOSEPH", "JOSEPHINE", "JOSHUA", "JOSIE",
				"JOY", "JOYCE", "JUAN", "JUANA", "JUANITA", "JUDITH", "JUDY", "JULIA", "JULIAN", "JULIANA", "JULIANNE",
				"JULIE", "JULIET", "JULIETTE", "JULIO", "JUNE", "JUSTIN", "JUSTINE", "KAITLIN", "KARA", "KAREN", "KARI",
				"KARIN", "KARINA", "KARL", "KARLA", "KARYN", "KASEY", "KATE", "KATELYN", "KATHARINE", "KATHERINE",
				"KATHERYN", "KATHIE", "KATHLEEN", "KATHRINE", "KATHRYN", "KATHY", "KATIE", "KATINA", "KATRINA", "KATY",
				"KAY", "KAYE", "KAYLA", "KEISHA", "KEITH", "KELLEY", "KELLI", "KELLIE", "KELLY", "KELLY", "KELSEY",
				"KEN", "KENDRA", "KENNETH", "KENT", "KENYA", "KERI", "KERRI", "KERRY", "KEVIN", "KIM", "KIMBERLEY",
				"KIMBERLY", "KIRK", "KIRSTEN", "KITTY", "KRIS", "KRISTA", "KRISTEN", "KRISTI", "KRISTIE", "KRISTIN",
				"KRISTINA", "KRISTINE", "KRISTY", "KRYSTAL", "KURT", "KYLE", "LACEY", "LACY", "LADONNA", "LAKEISHA",
				"LAKISHA", "LANA", "LANCE", "LARA", "LARRY", "LATASHA", "LATISHA", "LATONYA", "LATOYA", "LAURA",
				"LAUREL", "LAUREN", "LAURI", "LAURIE", "LAVERNE", "LAVONNE", "LAWANDA", "LAWRENCE", "LEA", "LEAH",
				"LEANN", "LEANNA", "LEANNE", "LEE", "LEE", "LEIGH", "LEILA", "LELA", "LELIA", "LENA", "LENORA",
				"LENORE", "LEO", "LEOLA", "LEON", "LEONA", "LEONARD", "LEONOR", "LEROY", "LESA", "LESLEY", "LESLIE",
				"LESLIE", "LESSIE", "LESTER", "LETA", "LETHA", "LETICIA", "LETITIA", "LEWIS", "LIDIA", "LILA", "LILIA",
				"LILIAN", "LILIANA", "LILLIAN", "LILLIE", "LILLY", "LILY", "LINA", "LINDA", "LINDSAY", "LINDSEY",
				"LISA", "LIZ", "LIZA", "LIZZIE", "LLOYD", "LOIS", "LOLA", "LOLITA", "LONNIE", "LORA", "LORAINE",
				"LORENA", "LORENE", "LORETTA", "LORI", "LORIE", "LORNA", "LORRAINE", "LORRIE", "LOTTIE", "LOU",
				"LOUELLA", "LOUIS", "LOUISA", "LOUISE", "LOURDES", "LUANN", "LUCIA", "LUCILE", "LUCILLE", "LUCINDA",
				"LUCY", "LUELLA", "LUIS", "LUISA", "LULA", "LUPE", "LUZ", "LYDIA", "LYNDA", "LYNETTE", "LYNN", "LYNNE",
				"LYNNETTE", "MABEL", "MABLE", "MADELEINE", "MADELINE", "MADELYN", "MADGE", "MAE", "MAGDALENA", "MAGGIE",
				"MAI", "MALINDA", "MALLORY", "MAMIE", "MANDY", "MANUEL", "MANUELA", "MARA", "MARC", "MARCELLA", "MARCI",
				"MARCIA", "MARCIE", "MARCUS", "MARCY", "MARGARET", "MARGARITA", "MARGERY", "MARGIE", "MARGO", "MARGRET",
				"MARGUERITE", "MARI", "MARIA", "MARIAN", "MARIANA", "MARIANNE", "MARIBEL", "MARICELA", "MARIE",
				"MARIETTA", "MARILYN", "MARINA", "MARIO", "MARION", "MARION", "MARISA", "MARISOL", "MARISSA", "MARITZA",
				"MARJORIE", "MARK", "MARLA", "MARLENE", "MARQUITA", "MARSHA", "MARSHALL", "MARTA", "MARTHA", "MARTIN",
				"MARTINA", "MARVA", "MARVIN", "MARY", "MARYANN", "MARYANNE", "MARYELLEN", "MARYLOU", "MATHEW",
				"MATILDA", "MATTHEW", "MATTIE", "MAUDE", "MAURA", "MAUREEN", "MAURICE", "MAVIS", "MAX", "MAXINE", "MAY",
				"MAYRA", "MEAGAN", "MEGAN", "MEGHAN", "MELANIE", "MELBA", "MELINDA", "MELISA", "MELISSA", "MELODY",
				"MELVA", "MELVIN", "MERCEDES", "MEREDITH", "MERLE", "MIA", "MICHAEL", "MICHAEL", "MICHEAL", "MICHELE",
				"MICHELLE", "MIGUEL", "MIKE", "MILAGROS", "MILDRED", "MILLICENT", "MILLIE", "MILTON", "MINDY",
				"MINERVA", "MINNIE", "MIRANDA", "MIRIAM", "MISTY", "MITCHELL", "MITZI", "MOLLIE", "MOLLY", "MONA",
				"MONICA", "MONIQUE", "MORGAN", "MORRIS", "MURIEL", "MYRA", "MYRNA", "MYRTLE", "NADIA", "NADINE",
				"NANCY", "NANETTE", "NANNIE", "NAOMI", "NATALIA", "NATALIE", "NATASHA", "NATHAN", "NATHANIEL", "NEIL",
				"NELDA", "NELL", "NELLIE", "NELSON", "NETTIE", "NEVA", "NICHOLAS", "NICHOLE", "NICOLE", "NIKKI", "NINA",
				"NITA", "NOELLE", "NOEMI", "NOLA", "NONA", "NORA", "NOREEN", "NORMA", "NORMAN", "ODESSA", "OFELIA",
				"OLA", "OLGA", "OLIVE", "OLIVIA", "OLLIE", "OPAL", "OPHELIA", "ORA", "OSCAR", "PAIGE", "PAM", "PAMELA",
				"PANSY", "PAT", "PATRICA", "PATRICE", "PATRICIA", "PATRICK", "PATSY", "PATTI", "PATTY", "PAUL", "PAULA",
				"PAULETTE", "PAULINE", "PEARL", "PEARLIE", "PEDRO", "PEGGY", "PENELOPE", "PENNY", "PERRY", "PETER",
				"PETRA", "PHILIP", "PHILLIP", "PHOEBE", "PHYLLIS", "POLLY", "PRISCILLA", "QUEEN", "RACHAEL", "RACHEL",
				"RACHELLE", "RAE", "RAFAEL", "RALPH", "RAMON", "RAMONA", "RANDALL", "RANDI", "RANDY", "RAQUEL", "RAUL",
				"RAY", "RAYMOND", "REBA", "REBECCA", "REBEKAH", "REGINA", "REGINALD", "RENA", "RENE", "RENE", "RENEE",
				"REVA", "REYNA", "RHEA", "RHODA", "RHONDA", "RICARDO", "RICHARD", "RICK", "RICKY", "RITA", "ROBBIE",
				"ROBERT", "ROBERT", "ROBERTA", "ROBERTO", "ROBIN", "ROBYN", "ROCHELLE", "RODNEY", "ROGER", "ROLAND",
				"RON", "RONALD", "RONDA", "RONNIE", "ROSA", "ROSALIE", "ROSALIND", "ROSALINDA", "ROSALYN", "ROSANNA",
				"ROSANNE", "ROSARIO", "ROSE", "ROSEANN", "ROSELLA", "ROSEMARIE", "ROSEMARY", "ROSETTA", "ROSIE",
				"ROSLYN", "ROSS", "ROWENA", "ROXANNE", "ROXIE", "ROY", "RUBEN", "RUBY", "RUSSELL", "RUTH", "RUTHIE",
				"RYAN", "SABRINA", "SADIE", "SALLIE", "SALLY", "SALVADOR", "SAM", "SAMANTHA", "SAMUEL", "SANDRA",
				"SANDY", "SARA", "SARAH", "SASHA", "SAUNDRA", "SAVANNAH", "SCOTT", "SEAN", "SELENA", "SELMA", "SERENA",
				"SERGIO", "SETH", "SHANA", "SHANE", "SHANNA", "SHANNON", "SHARI", "SHARLENE", "SHARON", "SHARRON",
				"SHAUNA", "SHAWN", "SHAWN", "SHAWNA", "SHEENA", "SHEILA", "SHELBY", "SHELIA", "SHELLEY", "SHELLY",
				"SHEREE", "SHERI", "SHERRI", "SHERRIE", "SHERRY", "SHERYL", "SHIRLEY", "SIDNEY", "SILVIA", "SIMONE",
				"SOCORRO", "SOFIA", "SONDRA", "SONIA", "SONJA", "SONYA", "SOPHIA", "SOPHIE", "STACEY", "STACI",
				"STACIE", "STACY", "STANLEY", "STEFANIE", "STELLA", "STEPHANIE", "STEPHEN", "STEVE", "STEVEN", "SUE",
				"SUMMER", "SUSAN", "SUSANA", "SUSANNA", "SUSANNE", "SUSIE", "SUZANNE", "SUZETTE", "SYBIL", "SYLVIA",
				"TABATHA", "TABITHA", "TAMARA", "TAMEKA", "TAMERA", "TAMI", "TAMIKA", "TAMMI", "TAMMIE", "TAMMY",
				"TAMRA", "TANIA", "TANISHA", "TANYA", "TARA", "TASHA", "TAYLOR", "TED", "TERESA", "TERI", "TERRA",
				"TERRANCE", "TERRENCE", "TERRI", "TERRIE", "TERRY", "TERRY", "TESSA", "THELMA", "THEODORE", "THERESA",
				"THERESE", "THOMAS", "TIA", "TIFFANY", "TIM", "TIMOTHY", "TINA", "TISHA", "TODD", "TOM", "TOMMIE",
				"TOMMY", "TONI", "TONIA", "TONY", "TONYA", "TRACEY", "TRACI", "TRACIE", "TRACY", "TRACY", "TRAVIS",
				"TRICIA", "TRINA", "TRISHA", "TROY", "TRUDY", "TWILA", "TYLER", "TYRONE", "URSULA", "VALARIE",
				"VALERIA", "VALERIE", "VANESSA", "VELMA", "VERA", "VERNA", "VERNON", "VERONICA", "VICKI", "VICKIE",
				"VICKY", "VICTOR", "VICTORIA", "VILMA", "VINCENT", "VIOLA", "VIOLET", "VIRGIE", "VIRGIL", "VIRGINIA",
				"VIVIAN", "VONDA", "WADE", "WALLACE", "WALTER", "WANDA", "WARREN", "WAYNE", "WENDI", "WENDY", "WESLEY",
				"WHITNEY", "WILDA", "WILLA", "WILLARD", "WILLIAM", "WILLIE", "WILLIE", "WILMA", "WINIFRED", "WINNIE",
				"YESENIA", "YOLANDA", "YOUNG", "YVETTE", "YVONNE", "ZACHARY", "ZELMA" };
		return receiverLastNames[new Random().nextInt(receiverLastNames.length)];
	}

	/**
	 * This function returns random receiver middle name
	 */
	public static String generateRandomReceiverMiddleName() {
		String receiverMiddleNames[] = { "I" };
		return receiverMiddleNames[new Random().nextInt(receiverMiddleNames.length)];
	}

	/**
	 * This function returns random receiver second last name
	 */
	public static String generateRandomReceiverSecondLastName() {
		String receiverSecondLastNames[] = { "PEDRO", "ARTURO", "LOYOLA", "INC", "MERC", "NENA", "NEMPA", "MUCHA",
				"KIKA", "KRIKA", "BOMBA", "TWEETA", "RAMA", "CHEETA", "KOOTA", "PRADA", "INA", "MINA", "MIKA", "KAI",
				"KAMA", "REEKA", "RAM", "PAM", "POSH", "DIKA" };
		return receiverSecondLastNames[new Random().nextInt(receiverSecondLastNames.length)];
	}

	/**
	 * Makes the web driver to wait till the mentioned seconds
	 */
	public void wait(int timeToWait) {
		try {
			Thread.sleep(timeToWait * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This function is the shortcut for screenshot when the Test is Failed
	 */
	public void takeScreenshotFail(String failureMessage) {
		try {
			Thread.sleep(2000);
			screenshot_path = captureScreenshot(driver);
			image = loggernew.addScreenCapture(screenshot_path);
			loggernew.log(LogStatus.FAIL, failureMessage, image);
		} catch (InterruptedException e) {
			logger.error("Issue in adding extra delay: ", e.fillInStackTrace());
		}
	}

	/**
	 * This function is the shortcut for screenshot when the Test is Passed
	 */
	public void takeScreenshotPass() {
		try {
			Thread.sleep(2000);
			screenshot_path = captureScreenshot(driver);
			image = loggernew.addScreenCapture(screenshot_path);
			loggernew.log(LogStatus.PASS, "Test Case Passed", image);
		} catch (InterruptedException e) {
			logger.error("Issue in adding extra delay", e.fillInStackTrace());
		}
	}

	public void takeScreenshot(int status) {
		screenshot_path = captureScreenshot(driver);
		image = loggernew.addScreenCapture(screenshot_path);
		if (status == 2) {
			loggernew.log(LogStatus.FAIL, "Test Case Failed", image);
		} else if (status == 1) {
			loggernew.log(LogStatus.PASS, "Test Case Passed", image);
		}
	}

	/**
	 * This function used to take the screenshot at the end
	 * 
	 * @throws DataSheetException
	 */
	public static String captureScreenshot(WebDriver driver) {
		System.out.println(reportscreenshotpath);
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String Dest = reportscreenshotpath + System.nanoTime() + ".png";
			File Destination = new File(Dest);
			FileUtils.copyFile(source, Destination);
			System.out.println("Screenshot taken");
			return Dest;
		} catch (Exception e) {
			System.out.println("Exception while taking screenshot " + e.getMessage());
			return e.getMessage();
		}
	}

	/**
	 * This function Starts WebDriver client based on the given Browser
	 * 
	 * @param browser
	 * @throws MalformedURLException
	 * @throws InvalidBrowserException
	 * @throws Exception
	 */
	protected void startWebDriverClient(String browser) throws MalformedURLException {
		try {
			if (browser.equalsIgnoreCase("InternetExplorer") || browser.equalsIgnoreCase("Edge")
					|| browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("Chrome")
					|| browser.equalsIgnoreCase("HTMLUnitDriver") || browser.equalsIgnoreCase("PhantomJS")) {
				driver = WebDriverFactory.getWebdriver(browser);
				// driver.manage().window().setSize(new Dimension(1583, 783));
				driver.manage().window().maximize();
				System.out.println("Window Size: " + driver.manage().window().getSize());
				driver.manage().timeouts().implicitlyWait(20000, TimeUnit.MILLISECONDS);
				logger.info("startWebDriverClient: Started WebDriver for " + driver);
			} else if (browser.equalsIgnoreCase("HeadlessChrome")) {
				driver = WebDriverFactory.getWebdriver(browser);
				System.out.println("Window Size: " + driver.manage().window().getSize());
				driver.manage().timeouts().implicitlyWait(20000, TimeUnit.MILLISECONDS);
				logger.info("startWebDriverClient: Started WebDriver for " + driver);
			} else {
				logger.error("Browser name" + " '" + browser + "' "
						+ "is invalid : Please give InternetExplorer, Edge, Firefox, Chrome, HeadlessChrome, HTMLUnitDriver or PhantomJS");
			}
		} catch (UnreachableBrowserException e) {
			logger.error("Could not start a new session. Possible causes are: "
					+ "\n\t 1. Selenium Grid is not proprly set up"
					+ "\n\t 2. Drivers are not properly added to the path "
					+ "\n\t 3. Port mentioned in the Grid properties file is different from port in which Remote server has started",
					e.fillInStackTrace());
			throw new UnreachableBrowserException("Could not start a new session. Possible causes are: "
					+ "\n\t 1. Selenium Grid is not proprly set up"
					+ "\n\t 2. Drivers are not properly added to the path "
					+ "\n\t 3. Port mentioned in the Grid properties file is different from port in which Remote server has started");
		} catch (WebDriverException e) {
			logger.error("Could not start a new session. Possible causes are : "
					+ "\n\t 1. Selenium Grid is not proprly set up"
					+ "\n\t 2. Drivers are not properly added to the path "
					+ "\n\t 3. Port mentioned in the Grid properties file is different from port in which Remote server has started",
					e.fillInStackTrace());
			throw new WebDriverException("Could not start a new session. Possible causes are: "
					+ "\n\t 1. Selenium Grid is not proprly set up"
					+ "\n\t 2. Drivers are not properly added to the path "
					+ "\n\t 3. Port mentioned in the Grid properties file is different from port in which Remote server has started");
		} catch (Exception e) {
			logger.error("Browser name" + " '" + browser + "' "
					+ "is invalid : Please give InternetExplorer, Edge, Firefox, Chrome, HeadlessChrome, HTMLUnitDriver or PhantomJS");
		}
	}

	/**
	 * This function retrieve the Base URL from Grid properties
	 * 
	 * @return - Returns the Application's Base URL
	 */
	public static String getBaseURL() {
		return webdriverUrl;
	}

	/**
	 * This function retrieve the Application URL from Global properties
	 * 
	 * @return - Returns the Application's Base URL
	 */
	public static String getApplicationURL() {

		if (usefakeauthUrl.equals("Y")) {
			return fakeauthUrl;
		} else if (usefakeauthUrl.equals("N")) {
			return applicationUrl;
		} else {
			return "";
		}
	}

	/**
	 * This method will return the browser names specified in global properties file
	 * 
	 * @return array of browser names
	 */
	public static String[] getBrowserName() {
		String[] browserArray = null;

		if (globalProperties.containsKey("browser_name")) {

			String browsers = globalProperties.getString("browser_name");
			if (browsers.contains(",")) {
				browserArray = browsers.trim().split(",");
			} else {
				browserArray = browsers.trim().split(" ");
			}
			for (String browser : browserArray) {
				if (browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("InternetExplorer")
						|| browser.equalsIgnoreCase("Chrome") || browser.equalsIgnoreCase("Android")
						|| browser.equalsIgnoreCase("PhantomJS")) {
					return browserArray;
				} else {
					logger.error(
							"Browser name specified in global properties file is invalid. Please give InternetExplorer,Chrome ,Firefox,Android or PhantomJS");

				}
			}

		} else {
			logger.error("No browser name specified for the test execution");

		}
		return browserArray;
	}

	public static String getGlobalBrowserFlag() {
		return globalProperties.getString("use_global_browser_forExcel");
	}

	public void takeScreenshot() throws ScreenshotException {
		WebDriver augmentedDriver = new Augmenter().augment(driver);
		File scrFile = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(
					System.getProperty("screenshot.directory") + "\\" + testName + "_" + System.nanoTime() + ".png"));
			logger.info("Screen shot has been taken");

		} catch (IOException e) {
			logger.error("The filename, directory name, or volume label syntax of screenshot is incorrect");
			e.printStackTrace();
		} catch (ScreenshotException e1) {
			throw new ScreenshotException("Unable to take screenshot");
		}
	}

	public String getValue(String key) {
		if (mapDataSheet.containsKey(key)) {
			return mapDataSheet.get(key);
		} else {
			logger.info("Column heading \'" + key
					+ "\' is not present in your data source. \n Please Check the Column Headings of your TestCase's data in your data source (Excel / Database)");
			throw new NullPointerException("Column heading \'" + key
					+ "\' is not present in your data source. \n Please Check the Column Headings of your TestCase's data in your data source (Excel / Database)");
		}
	}

	public void setValue(String key, String value) throws BiffException, IOException, JxlWriteException, JXLException {
		dataToBeWritten.put(key, value);
		addDataToOutputExcel(dataToBeWritten);
	}

	public Alert isAlertPresent() {
		try {
			Alert alert = driver.switchTo().alert();
			return alert;
		} catch (NoAlertPresentException e) {
			return null;
		}
	}

	// Method to write back the data from a test method stored as as a
	// Linked-hash-map
	public void addDataToOutputExcel(LinkedHashMap<String, String> outputData)
			throws BiffException, IOException, JxlWriteException, JXLException {

		Workbook workbook = Workbook.getWorkbook(new File(outputDataSheetPath));
		WritableWorkbook copy = Workbook.createWorkbook(new File(outputDataSheetPath), workbook);

		WritableSheet sheet2 = copy.getSheet(testName);

		WritableCellFormat cellFormatHeading = new WritableCellFormat();
		cellFormatHeading.setBackground(Colour.GRAY_25);

		WritableCellFormat cellFormat = new WritableCellFormat();
		cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
		cellFormat.setBackground(Colour.CORAL);
		cellFormat.setAlignment(Alignment.CENTRE);
		int coloumn = sheet2.getColumns();
		int increment = 0;

		for (String key : outputData.keySet()) {
			String data = outputData.get(key);
			boolean keyExist = false;
			for (int i = 0; i < coloumn; i++) {
				String content = sheet2.getCell(i, 0).getContents();
				if (content.equalsIgnoreCase(key)) {
					keyExist = true;
					if (data.equals("Pass")) {
						cellFormat.setBackground(Colour.GREEN);
						cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
						cellFormat.setAlignment(Alignment.CENTRE);
					} else if (data.equals("Fail")) {
						cellFormat.setBackground(Colour.RED);
						cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
						cellFormat.setAlignment(Alignment.CENTRE);
					}

					else if (key.equals("ReferenceNumber")) {
						cellFormat.setBackground(Colour.SEA_GREEN);
						cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
						cellFormat.setAlignment(Alignment.CENTRE);
					}

					else if (key.equals("MoneyGram Plus Number")) {
						cellFormat.setBackground(Colour.SEA_GREEN);
						cellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
						cellFormat.setAlignment(Alignment.CENTRE);
					}

					Label label2 = new Label(i, rowCount, data, cellFormat);
					sheet2.addCell(label2);

					// Updating Hash-map
					if (mapDataSheet.containsKey(key)) {
						mapDataSheet.put(key, data);
					}

					outputData.clear();

					break;

				} // if
			} // for

			if (keyExist == false) {
				// Inserting a new key in excel
				Label label = new Label(coloumn + increment, 0, key, cellFormatHeading);
				Label label2 = new Label(coloumn + increment, rowCount, data, cellFormat);

				sheet2.addCell(label);

				sheet2.addCell(label2);

				increment++;

				// Inserting new Key Value in Hashmap
				mapDataSheet.put(key, data);

			} // if
		} // for

		copy.write();
		copy.close();

		if (workbook != null) {
			workbook.close();
		}
	}// addDataToOutputExcel
}
