package com.saf.base;

import java.net.MalformedURLException;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.saf.utilities.Log;

import io.github.bonigarcia.wdm.WebDriverManager;

//import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDriverFactory extends BaseTest {
	
	public static String curProj = System.getProperty("user.dir");

	/**
	 * This function initialize a WebDriver object using on the Browser type
	 * @param browserType
	 * @return WebDriver
	 * @throws MalformedURLException
	 */	
	public static WebDriver getWebdriver(String browserType) throws MalformedURLException {
		WebDriver webdriver = null;		
		Logger logger = Log.getInstance(Thread.currentThread().getStackTrace()[1].getClassName());
		logger.info("Browser type is: " + browserType);		
		
		if (browserType.equalsIgnoreCase("InternetExplorer")) {
			try {						
				//System.setProperty("webdriver.ie.driver", curProj+"\\drivers\\IEDriverServer.exe");	
				WebDriverManager.iedriver().setup();
				InternetExplorerOptions options = new InternetExplorerOptions();				
				options.destructivelyEnsureCleanSession();
				options.introduceFlakinessByIgnoringSecurityDomains();
				options.ignoreZoomSettings();
				options.requireWindowFocus();
				options.withInitialBrowserUrl("about:blank");
				webdriver = new InternetExplorerDriver(options);	
				//logger.info("getWebDriver - Setting webdriver.ie.driver system property as: " + System.getProperty("webdriver.ie.driver"));				
			} catch(IllegalStateException e) {
				//logger.error("The path to the driver executable must be set by the webdriver.ie.driver system property. ",e.fillInStackTrace());
				//throw new IllegalStateException("The path to the driver executable must be set by the webdriver.ie.driver system property.");
			}
		}
		else if (browserType.equalsIgnoreCase("Edge")) {
			try{
				//System.setProperty("webdriver.edge.driver", curProj+"\\drivers\\geckodriver.exe");
				WebDriverManager.edgedriver().setup();
				webdriver = new EdgeDriver();
				//logger.info("getWebDriver - Setting webdriver.gecko.driver system property as: " + System.getProperty("webdriver.gecko.driver"));
			}catch(IllegalStateException e) {
				//logger.error("The path to the driver executable must be set by the webdriver.gecko.driver system property. ",e.fillInStackTrace());
				//throw new IllegalStateException("The path to the driver executable must be set by the webdriver.gecko.driver system property.");
			}			
		}
		else if (browserType.equalsIgnoreCase("Firefox")) {
			try{
				//System.setProperty("webdriver.gecko.driver", curProj+"\\drivers\\geckodriver.exe");
				WebDriverManager.firefoxdriver().setup();
				webdriver = new FirefoxDriver();
				//logger.info("getWebDriver - Setting webdriver.gecko.driver system property as: " + System.getProperty("webdriver.gecko.driver"));
			}catch(IllegalStateException e) {
				//logger.error("The path to the driver executable must be set by the webdriver.gecko.driver system property. ",e.fillInStackTrace());
				//throw new IllegalStateException("The path to the driver executable must be set by the webdriver.gecko.driver system property.");
			}			
		}
		else if (browserType.equalsIgnoreCase("Chrome")) {
			try	{								
				//System.setProperty("webdriver.chrome.driver", curProj+"\\drivers\\chromedriver.exe");
				WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("useAutomationExtension", false);
				options.addArguments("disable-infobars");				
				//options.addArguments(chromeProfile);
				//options.addArguments("--disable-print-preview");
				webdriver = new ChromeDriver(options);
				//webdriver = new ChromeDriver();
				//logger.info("getWebDriver - Setting webdriver.chrome.driver system property as: " + System.getProperty("webdriver.chrome.driver"));
			}
			catch(IllegalStateException e) {
				//logger.error("The path to the driver executable must be set by the webdriver.chrome.driver system property. ",e.fillInStackTrace());
				//throw new IllegalStateException("The path to the driver executable must be set by the webdriver.chrome.driver system property.");
			}			
		}else if (browserType.equalsIgnoreCase("HeadlessChrome")) {
			try	{								
				//System.setProperty("webdriver.chrome.driver", curProj+"\\drivers\\chromedriver.exe");
				WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				//options.addArguments("window-size=1936,1056"); //Original
				options.addArguments("window-size=1366,768"); //Laptop and Google Analytics favorite
				//options.addArguments("window-size=1920,1080"); //Our current monitor settings
				options.addArguments("headless");
				webdriver = new ChromeDriver(options);				
				//logger.info("getWebDriver - Setting webdriver.chrome.driver system property as: " + System.getProperty("webdriver.chrome.driver"));
			}
			catch(IllegalStateException e) {
				//logger.error("The path to the driver executable must be set by the webdriver.chrome.driver system property. ",e.fillInStackTrace());
				//throw new IllegalStateException("The path to the driver executable must be set by the webdriver.chrome.driver system property.");
			}			
		}else if (browserType.equalsIgnoreCase("HTMLUnitDriver")) {						
				webdriver = new HtmlUnitDriver();						
		}
		else if (browserType.equalsIgnoreCase("PhantomJS")) {			
			//DesiredCapabilities caps = new DesiredCapabilities();
			//caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, System.setProperty("phantomjs.binary.path", curProj+"\\drivers\\phantomjs.exe"));
			//webdriver = new PhantomJSDriver(caps);			
			WebDriverManager.phantomjs().setup();
			webdriver = new PhantomJSDriver();				
			//logger.info("getWebDriver - Setting phantomjs.binary.path system property as: " + System.getProperty("phantomjs.binary.path"));			
		}		
		else if(browserType.equals("")) {
			logger.error("Browser type is empty, Unable to instantiate new webDriver. Unrecognized browser type. "+ browserType+"/n possible causes, \n\t1.'Browser' column in data sheet is empty.");			
		}
		else {
			logger.error("Unable to instantiate new webDriver. Unrecognised browser type. "+ browserType);
		}	
		return webdriver;
	}	
}
