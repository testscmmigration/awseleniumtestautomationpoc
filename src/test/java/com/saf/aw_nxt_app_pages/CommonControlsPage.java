package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class CommonControlsPage extends BasePage {
	
	//Buttons
	private By nextBtn = getLocator("button.btn.btn-lg.mg-btn.height-38.width-85pc.mg-btn-primary",BY_TYPE.BY_CSSSELECTOR);									 
	private By backBtn = getLocator("button.btn.btn-lg.mg-btn.height-38.width-85pc.mg-btn-secondary",BY_TYPE.BY_CSSSELECTOR);		
	
	private By sendMoneyBtn = getLocator("ok", BY_TYPE.BY_ID);
	private By finishedBtn = getLocator("ok", BY_TYPE.BY_ID);
	
	//Text
	private By referenceNumberText = getLocator("//p/strong", BY_TYPE.BY_XPATH);
	private By moneyGramPlusNumberText = getLocator("//span[2]", BY_TYPE.BY_XPATH);
									 
	public CommonControlsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean backBtn_Presence() {
		return isPresent(backBtn);
	}
	
	public boolean nextBtn_Presence() {
		return isPresent(nextBtn);
	}
	
	public boolean sendMoneyBtn_Presence() {
		return isPresent(sendMoneyBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public boolean moneyGramPlusNumberText_Presence() {
		return isPresent(moneyGramPlusNumberText);
	}
	
	public boolean referenceNumberText_Presence() {
		return isPresent(referenceNumberText);
	}
	
	public void click_backBtn(){		
		Assert.assertTrue(this.backBtn_Presence(), "Common 'Back' button is absent on the page.");
		takeScreenshot("Click on common 'Back' button on the page.");
		click(backBtn);			
	}
	
	public void click_nextBtn(){		
		Assert.assertTrue(this.nextBtn_Presence(), "Common 'Next' button is absent on the page.");
		takeScreenshot("Click on common 'Next' button on the page.");
		click(nextBtn);			
	}
	
	public void click_sendMoneyBtn(){		
		Assert.assertTrue(this.sendMoneyBtn_Presence(), "'Send Money' button is absent on the 'Collect Funds' popup.");	
		takeScreenshot("Click on 'Send Money' button on the 'Collect Funds' popup.");
		click(sendMoneyBtn);		
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "'Finished' button is absent on the 'Success' popup.");	
		takeScreenshot("Click on 'Finished' button on the 'Success' popup.");
		click(finishedBtn);		
	}
	
	public String getMoneyGramPlusNumber() {
		Assert.assertTrue(this.moneyGramPlusNumberText_Presence(), "MoneyGram Plus Number text is absent on the 'Collect Funds' popup.");
		return getText(moneyGramPlusNumberText);
	}
	
	public String getReferenceNumber() {
		Assert.assertTrue(this.referenceNumberText_Presence(), "Reference Number text is absent on the 'Success' popup.");
		return getText(referenceNumberText);
	}
}
