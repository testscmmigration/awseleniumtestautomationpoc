package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class TranDetailsPage extends BasePage {
	
	//Buttons
	private By amendBtn = getLocator("button.btn.btn-lg.mg-btn.height-38.width-85pc.mg-btn-primary", BY_TYPE.BY_CSSSELECTOR);
	private By refundBtn =	getLocator("#ActionBarMainEditTransferTranDetailsRefund > button.btn.btn-lg.mg-btn.height-38.width-85pc.mg-btn-primary", BY_TYPE.BY_CSSSELECTOR);
	private By reversalBtn = getLocator("//div[3]/button", BY_TYPE.BY_XPATH);
	private By finishBtn = getLocator("button.btn.btn-lg.mg-btn.height-38.width-85pc.mg-btn-primary", BY_TYPE.BY_CSSSELECTOR);										   
	private By closeBtn = getLocator("//*[@class='btn clear']", BY_TYPE.BY_XPATH);
	private By yesContinueBtn = getLocator("//div[2]/button", BY_TYPE.BY_XPATH);
	private By finishedBtn = getLocator("//div[3]/div/button", BY_TYPE.BY_XPATH);	
	
	//Text
	private By transactionStatusText = getLocator("//div/strong", BY_TYPE.BY_XPATH);
	
	//Drop downs
	private By selectReversalReason  = getLocator("receive_ReversalReason", BY_TYPE.BY_ID);	
			

	public TranDetailsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean amendBtn_Presence() {
		return isPresent(amendBtn);
	}
	
	public boolean refundBtn_Presence() {
		return isPresent(refundBtn);
	}
	
	public boolean transactionStatusText_Presence() {
		return isPresent(transactionStatusText);
	}
	
	public boolean finishBtn_Presence() {
		return isPresent(finishBtn);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}
	
	public boolean reversalBtn_Presence() {
		return isPresent(reversalBtn);
	}
	
	public boolean selectReversalReason_Presence() {
		return isPresent(selectReversalReason);
	}
	
	public boolean yesContinueBtn_Presence() {
		return isPresent(yesContinueBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public void click_amendBtn(){		
		Assert.assertTrue(this.amendBtn_Presence(), "'Amend' button is absent on the transaction details page.");
		takeScreenshot("Click 'Amend' button on the transaction details page.");
		click(amendBtn);			
	}
	
	public void click_refundBtn(){		
		Assert.assertTrue(this.refundBtn_Presence(), "'Refund' button is absent on the transaction details page.");
		takeScreenshot("Click 'Refund' button on the transaction details page.");
		click(refundBtn);			
	}
	
	public void click_finishBtn(){		
		Assert.assertTrue(this.finishBtn_Presence(), "'Finish' button is absent on the transaction details page.");
		takeScreenshot("Click 'Finish' button on the transaction details page.");
		click(finishBtn);			
	}
	
	public String getTransactionStatus() {
		Assert.assertTrue(this.transactionStatusText_Presence(), "Transaction status is absent on the transaction details page.");
		return getText(transactionStatusText);
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "Close button on the toaster message is absent on the edit-transfer tran-details page.");
		takeScreenshot("Click on Close button on the toaster message on the edit-transfer tran-details page.");
		click(closeBtn);			
	}
	
	public void click_reversalBtn(){		
		Assert.assertTrue(this.reversalBtn_Presence(), "'Reversal' button is absent on the transaction details page.");
		takeScreenshot("Click 'Reversal' button on the transaction details page.");
		click(reversalBtn);			
	}
	
	public void select_selectReversalReason(){		
		Assert.assertTrue(this.selectReversalReason_Presence(), "Reversal Reason dropdown is absent on the receive-reversal-trans-details page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 5)); 
		selectDropDownByIndex(selectReversalReason, index);
		takeScreenshot("Select Reversal Reason.");
	}
	
	public String get_selectedReversalReason() {		
		return getSelectedDropDownValue(selectReversalReason);
	}
	
	public void click_yesContinueBtn(){		
		Assert.assertTrue(this.yesContinueBtn_Presence(), "'Yes, continue' button is absent on the receive-reversal-trans-details page.");
		takeScreenshot("Click 'Yes, continue' button on the receive-reversal-trans-details page.");
		click(yesContinueBtn);			
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "'Finshed' button is absent on the receive-reversal-trans-details page.");
		takeScreenshot("Click 'Finished' button on the receive-reversal-trans-details page.");
		click(finishedBtn);			
	}
}
