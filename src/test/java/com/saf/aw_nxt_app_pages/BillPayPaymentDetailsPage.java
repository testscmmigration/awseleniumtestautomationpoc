package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class BillPayPaymentDetailsPage extends BasePage { 
	
	//Text boxes
	private By paymentAmountTxt = getLocator("STRKEY_PAYMENT_AMOUNT", BY_TYPE.BY_ID);
	private By customerAccountNumberTxt = getLocator("biller_AccountNumber", BY_TYPE.BY_ID);
	private By addMessage1Txt = getLocator("messageField1", BY_TYPE.BY_ID);
	private By addMessage2Txt = getLocator("messageField2", BY_TYPE.BY_ID);
	
	//Buttons
	private By closeBtn = getLocator("//*[@class='btn clear']", BY_TYPE.BY_XPATH);
	private By clsBtn = getLocator("btnErrorModalSubmit", BY_TYPE.BY_ID);
	private By yesBtn = getLocator("ok", BY_TYPE.BY_ID);

	public BillPayPaymentDetailsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean paymentAmountTxt_Presence() {
		return isPresent(paymentAmountTxt);
	}
	
	public boolean customerAccountNumberTxt_Presence() {
		return isPresent(customerAccountNumberTxt);
	}
	
	public boolean addMessage1Txt_Presence() {
		return isPresent(addMessage1Txt);
	}
	
	public boolean addMessage2Txt_Presence() {
		return isPresent(addMessage2Txt);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}
	
	public boolean clsBtn_Presence() {
		return isPresent(clsBtn);
	}
	
	public boolean yesBtn_Presence() {
		return isPresent(yesBtn);
	}
	
	public void type_paymentAmountTxt(String value) {
		Assert.assertTrue(this.paymentAmountTxt_Presence(), "Payment amount textbox is absent on the bill pay payment details page.");
		type(paymentAmountTxt, value);		
		takeScreenshot("Enter Payment amount.");
	}
	
	public void type_customerAccountNumberTxt(String value) {
		Assert.assertTrue(this.customerAccountNumberTxt_Presence(), "Customer account number textbox is absent on the bill pay payment details page.");
		type(customerAccountNumberTxt, value);		
		takeScreenshot("Enter Customer account number.");
	}

	public void type_addMessage1Txt(String value) {
		Assert.assertTrue(this.addMessage1Txt_Presence(), "Add message first textbox is absent on the bill pay payment details page.");
		type(addMessage1Txt, value);		
		takeScreenshot("Enter Add message.");
	}
	
	public void type_addMessage2Txt(String value) {
		Assert.assertTrue(this.addMessage2Txt_Presence(), "Add message second textbox is absent on the bill pay payment details page.");
		type(addMessage2Txt, value);		
		takeScreenshot("Enter Add message.");
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "Close button on the toaster message is absent on the bill-pay payment details page.");
		takeScreenshot("Click on Close button on the toaster message on bill-pay payment details page.");
		click(closeBtn);			
	}
	
	public void click_clsBtn(){		
		Assert.assertTrue(this.clsBtn_Presence(), "Close button on the error model is absent on the bill-pay payment details page.");
		takeScreenshot("Click on Close button on the error model on bill-pay payment details page.");
		click(clsBtn);			
	}
	
	public void click_yesBtn(){		
		Assert.assertTrue(this.yesBtn_Presence(), "'Yes' button on the 'Confirmation required' popup is absent on the bill-pay payment details page.");
		takeScreenshot("Click on 'Yes' button on the 'Confirmation required' popup on the bill-pay payment details page.");
		click(yesBtn);			
	}
}
