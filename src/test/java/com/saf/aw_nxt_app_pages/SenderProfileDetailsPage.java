package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class SenderProfileDetailsPage extends BasePage {
	
	//Labels
	private By otherLbl = getLocator("//div[4]/strong", BY_TYPE.BY_XPATH);	

	public SenderProfileDetailsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean otherLbl_Presence() {
		return isPresent(otherLbl);
	}
	
	public void scrollTo_otherLbl(){		
		Assert.assertTrue(this.otherLbl_Presence(), "'Other:' label is absent on the sender-profile-details page.");		
		scrollToElementUsingJavascriptExecutor(otherLbl);
		takeScreenshot("Sender Profile: 2");
	}
}
