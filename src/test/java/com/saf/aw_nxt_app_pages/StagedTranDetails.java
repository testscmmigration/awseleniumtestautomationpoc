package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class StagedTranDetails extends BasePage {	
	
	//Buttons
	private By sendMoneyBtn = getLocator("ok", BY_TYPE.BY_ID);
	private By finishedBtn = getLocator("//button[@type='submit']", BY_TYPE.BY_XPATH);
	private By closeBtn = getLocator("//*[@class='btn clear']", BY_TYPE.BY_XPATH);
	private By payoutBtn = getLocator("//div[2]/button", BY_TYPE.BY_XPATH);
	private By finishBtn = getLocator("//button[@type='submit']", BY_TYPE.BY_XPATH);									   	 
	
	//Text
	private By referenceNumberText = getLocator("//p/strong", BY_TYPE.BY_XPATH);
		
	public StagedTranDetails(WebDriver driver) {
		super(driver);		
	}
	
	public boolean sendMoneyBtn_Presence() {
		return isPresent(sendMoneyBtn);
	}
		
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public boolean referenceNumberText_Presence() {
		return isPresent(referenceNumberText);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}
	
	public boolean payoutBtn_Presence() {
		return isPresent(payoutBtn);
	}
	
	public boolean finishBtn_Presence() {
		return isPresent(finishBtn);
	}
	
	public void click_sendMoneyBtn(){		
		Assert.assertTrue(this.sendMoneyBtn_Presence(), "Send Money button is absent on the staged-tran details page.");	
		takeScreenshot("Click on send money button on the staged-tran details page.");
		click(sendMoneyBtn);		
	}
		
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "Finshed button is absent on the staged-tran details page.");
		takeScreenshot("Click on Finished button on the staged-tran details page.");
		click(finishedBtn);			
	}
	
	public String getReferenceNumber() {
		Assert.assertTrue(this.referenceNumberText_Presence(), "Reference Number text is absent on the staged-tran details page.");
		return getText(referenceNumberText);
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "Close button on the toaster message is absent on the staged-tran details page.");
		takeScreenshot("Click on Close button on the toaster message on staged-tran details page.");
		click(closeBtn);			
	}
	
	public void click_payoutBtn(){		
		Assert.assertTrue(this.payoutBtn_Presence(), "Payout button on the 'Do you wish to proceed?' popup is absent on the receive staged details page.");
		takeScreenshot("Click on Payout button on the 'Do you wish to proceed?' popup on the receive staged details page.");
		click(payoutBtn);			
	}	
	
	public void click_finishBtn(){		
		Assert.assertTrue(this.finishBtn_Presence(), "'Finish' button is absent on the transaction details page.");
		takeScreenshot("Click 'Finish' button on the transaction details page.");
		click(finishBtn);			
	}
}
