package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class BillerSearchResultsPage extends BasePage {
	
	//Biller search result
	private By billerSearchResult = getLocator("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/bill-pay-biller-search-results/content-body/div/shared-scope-transclude/div/div/div/div[2]/div/div[2]/div/span/i", BY_TYPE.BY_XPATH);

	public BillerSearchResultsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean billerSearchResult_Presence() {
		return isPresent(billerSearchResult);
	}
	
	public void click_billerSearchResult(){		
		Assert.assertTrue(this.billerSearchResult_Presence(), "Biller search result is absent on the biller search results page.");		
		takeScreenshot("Click on the biller search result.");
		click(billerSearchResult);					
	}
}
