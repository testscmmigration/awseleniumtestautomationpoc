package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.WebDriver;

import com.saf.base.BasePage;

public class LaunchApplicationUrl extends BasePage {
	
  public LaunchApplicationUrl(WebDriver driver) {
	  super(driver);		
	}  	

	public void launchBasePage() {
		launchBaseURL();		
	}
	
	public void ClearCooke() {
		deleteAllCookie();
	}	
}
