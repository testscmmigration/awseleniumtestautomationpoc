package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class SendDestinationAmountPage extends BasePage {
	
	//Drop downs
	private By selectCurrency = getLocator("STRKEY_CURRENCY", BY_TYPE.BY_ID);
	private By selectFeeType = getLocator("STRKEY_FEE_TYPE", BY_TYPE.BY_ID);
	
	//Text boxes
	private By sendAmountTxt = getLocator("STRKEY_SEND_AMT", BY_TYPE.BY_ID);
	private By destinationCountryTxt = getLocator("STRKEY_DEST_COUNTRY", BY_TYPE.BY_ID);
	private By destinationStateOrProvinceTxt = getLocator("STRKEY_DEST_STATE", BY_TYPE.BY_ID);
	private By promoCode1Txt = getLocator("STRKEY_PROMO_CODE_1", BY_TYPE.BY_ID);
	private By promoCode2Txt = getLocator("STRKEY_PROMO_CODE_2", BY_TYPE.BY_ID);
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
	//Buttons
	private By closeBtn = getLocator("//*[@class='btn clear']", BY_TYPE.BY_XPATH);
	private By yesBtn = getLocator("ok", BY_TYPE.BY_ID);

	public SendDestinationAmountPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean sendAmountTxt_Presence() {
		return isPresent(sendAmountTxt);
	}
	
	public boolean selectCurrency_Presence() {
		return isPresent(selectCurrency);
	}
	
	public boolean selectFeeType_Presence() {
		return isPresent(selectFeeType);
	}	
	
	public boolean destinationCountryTxt_Presence() {
		return isPresent(destinationCountryTxt);
	}
	
	public boolean destinationStateOrProvinceTxt_Presence() {
		return isPresent(destinationStateOrProvinceTxt);
	}
	
	public boolean promoCode1Txt_Presence() {
		return isPresent(promoCode1Txt);
	}
	
	public boolean promoCode2Txt_Presence() {
		return isPresent(promoCode2Txt);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}
	
	public boolean yesBtn_Presence() {
		return isPresent(yesBtn);
	}
	
	public void type_sendAmountTxt(String value) {
		Assert.assertTrue(this.sendAmountTxt_Presence(), "Send Amount textbox is absent on the send destination-amount page.");
		type(sendAmountTxt, value);		
		takeScreenshot("Enter send amount.");
	}
	
	public void select_selectCurrency(String value){		
		Assert.assertTrue(this.selectCurrency_Presence(), "Currency dropdown is absent on the send destination-amount page.");
		selectDropDownByVisibleText(selectCurrency, value);
		takeScreenshot("Select currency.");
	}
	
	public void select_selectFeeType(String value){		
		Assert.assertTrue(this.selectFeeType_Presence(), "Fee Type dropdown is absent on the send destination-amount page.");
		selectDropDownByVisibleText(selectFeeType, value);
		takeScreenshot("Select fee type.");
	}
	
	public void type_destinationCountryTxt(String value) {
		Assert.assertTrue(this.destinationCountryTxt_Presence(), "Destination Country textbox is absent on the send destination-amount page.");
		type(destinationCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter destination country.");
	}
	
	public void type_destinationStateOrProvinceTxt(String value) {
		Assert.assertTrue(this.destinationStateOrProvinceTxt_Presence(), "Destination state/province textbox is absent on the send destination-amount page.");
		type(destinationStateOrProvinceTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter destination state or province.");
	}
	
	public void type_promoCode1Txt(String value) {
		Assert.assertTrue(this.promoCode1Txt_Presence(), "Promo 1ode1 is absent on the send destination-amount page.");
		type(promoCode1Txt, value);
		takeScreenshot("Enter promo code 1.");
	}
	
	public void type_promoCode2Txt(String value) {
		Assert.assertTrue(this.promoCode2Txt_Presence(), "Promo code2 textbox is absent on the send destination-amount page.");
		type(promoCode2Txt, value);		
		takeScreenshot("Enter promo code 2.");
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "Close button on the toaster message is absent on the send destination-amount page.");
		takeScreenshot("Click on Close button on the toaster message on send destination-amount page.");
		click(closeBtn);			
	}
	
	public void click_yesBtn(){		
		Assert.assertTrue(this.yesBtn_Presence(), "'Yes' button on the 'Confirmation required' popup is absent on the send destination-amount page.");
		takeScreenshot("Click on 'Yes' button on the 'Confirmation required' popup on send destination-amount page.");
		click(yesBtn);			
	}
}
