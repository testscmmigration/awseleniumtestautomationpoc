package com.saf.aw_nxt_app_pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.saf.base.BasePage;

public class DashboardPage extends BasePage {
	
	//Links
	private By approfileIconLnk = getLocator("dropdownMenuButton", BY_TYPE.BY_ID);
	private By profileIconLnk = getLocator("//a/i", BY_TYPE.BY_XPATH);	
	private By aplogoutLnk = getLocator("//div[2]/div[2]/div[2]/div", BY_TYPE.BY_XPATH);
	private By logoutLnk = getLocator("//span[contains(text(),'Logout')]", BY_TYPE.BY_XPATH);
	private By versionLnk = getLocator("Version", BY_TYPE.BY_LINKTEXT);
	private By dailyTransactionActivityLnk = getLocator("//span[contains(text(),'Daily Transaction Activity')]", BY_TYPE.BY_XPATH);
	private By findLocationLnk = getLocator("//span[contains(text(),'Find Locations')]", BY_TYPE.BY_XPATH);	
	private By proprietaryandconfidentialLnk = getLocator("//span[contains(text(),'Proprietary and confidential')]", BY_TYPE.BY_XPATH);
	private By termsofUselLnk = getLocator("//span[contains(text(),'Terms of Use')]", BY_TYPE.BY_XPATH);
			
	//Drop downs
	private By selectAgentPOS  = getLocator("agentId", BY_TYPE.BY_ID);	
	
	//Headers
	private By newsHeaderLnk = getLocator("//span[contains(text(),'News')]", BY_TYPE.BY_XPATH);
	private By transactHeaderLnk = getLocator("//span[contains(text(),'Transact')]", BY_TYPE.BY_XPATH);
	private By reportsHeaderLnk = getLocator("//span[contains(text(),'Reports')]", BY_TYPE.BY_XPATH);
		
	//Buttons
	private By sendMoneyBtn = getLocator("sendDashboardBtn", BY_TYPE.BY_ID);
	private By receiveMoneyBtn = getLocator("receiveDashboardBtn", BY_TYPE.BY_ID);
	private By estimateFeeBtn = getLocator("efeeDashboardBtn", BY_TYPE.BY_ID);
	private By editTransferBtn = getLocator("editTransferDashboardBtn", BY_TYPE.BY_ID);
	private By completeMobileOrKioskTransactionBtn = getLocator("stagedCompletionDashboardBtn", BY_TYPE.BY_ID);	
	private By moneyOrderBtn = getLocator("moneyOrderDashboardBtn", BY_TYPE.BY_ID);
	private By payaVendorBtn = getLocator("vendorPaymentDashboardBtn", BY_TYPE.BY_ID);
	private By payaBillBtn = getLocator("billPayDashboardBtn", BY_TYPE.BY_ID);
	private By viewStatusBtn = getLocator("viewStatusDashboardBtn", BY_TYPE.BY_ID);	
	private By loadacardBtn = getLocator("cardLoadDashboardBtn", BY_TYPE.BY_ID);	
	private By clearCacheBtn = getLocator("//button[@class='btn btn-lg btn-block mg-btn mg-btn-secondary']", BY_TYPE.BY_XPATH);
	private By closeBtn = getLocator("button.btn.btn-lg.btn-block.mg-btn.mg-btn-primary", BY_TYPE.BY_CSSSELECTOR);	
	private By AWProprietaryandConfidentialCloseBtn = getLocator("//button[@type='button']", BY_TYPE.BY_XPATH);
	
	//Spinner
	//private By spinner = getLocator("i.fa.fa-spinner.fa-pulse.fa-5x.fa-fw.fa-inverse.global-spinner",BY_TYPE.BY_CSSSELECTOR);
	private By spinner = getLocator("globalSpinner",BY_TYPE.BY_ID);
     
    WebDriverWait wait = new WebDriverWait(driver, 50);   

	public DashboardPage(WebDriver driver) {
		super(driver);
	}
	
	public boolean approfileIconLnk_Presence() {
		return isPresent(approfileIconLnk);
	}
	
	public boolean profileIconLnk_Presence() {
		return isPresent(profileIconLnk);
	}
	
	public boolean aplogoutLnk_Presence() {
		return isPresent(aplogoutLnk);
	}
	
	public boolean logoutLnk_Presence() {
		return isPresent(logoutLnk);
	}
	
	public boolean versionLnk_Presence() {
		return isPresent(versionLnk);
	}
	
	public boolean dailyTransactionActivityLnk_Presence() {
		return isPresent(dailyTransactionActivityLnk);
	}
	
	public boolean findLocationLnk_Presence() {
		return isPresent(findLocationLnk);
	}
	
	public boolean proprietaryandconfidentialLnk_Presence() {
		return isPresent(proprietaryandconfidentialLnk);
	}
	
	public boolean termsofUselLnk_Presence() {
		return isPresent(termsofUselLnk);
	}
	
	public boolean newsHeaderLnk_Presence() {
		return isPresent(newsHeaderLnk);
	}
	
	public boolean transactHeaderLnk_Presence() {
		return isPresent(transactHeaderLnk);
	}

	public boolean reportsHeaderLnk_Presence() {
		return isPresent(reportsHeaderLnk);
	}
	
	public boolean selectAgentPOS_Presence() {
		return isPresent(selectAgentPOS);
	}
	
	public boolean sendMoneyBtn_Presence() {
		return isPresent(sendMoneyBtn);
	}
	
	public boolean receiveMoneyBtn_Presence() {
		return isPresent(receiveMoneyBtn);
	}
	
	public boolean estimateFeeBtn_Presence() {
		return isPresent(estimateFeeBtn);
	}
	
	public boolean editTransferBtn_Presence() {
		return isPresent(editTransferBtn);
	}
	
	public boolean completeMobileOrKioskTransactionBtn_Presence() {
		return isPresent(completeMobileOrKioskTransactionBtn);
	}
	
	public boolean moneyOrderBtn_Presence() {
		return isPresent(moneyOrderBtn);
	}
	
	public boolean payaVendorBtn_Presence() {
		return isPresent(payaVendorBtn);
	}
	
	public boolean payaBillBtn_Presence() {
		return isPresent(payaBillBtn);
	}
	
	public boolean viewStatusBtn_Presence() {
		return isPresent(viewStatusBtn);
	}
	
	public boolean loadacardBtn_Presence() {
		return isPresent(loadacardBtn);
	}
	
	public boolean clearCacheBtn_Presence() {
		return isPresent(clearCacheBtn);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}	
	
	public boolean AWProprietaryandConfidentialCloseBtn_Presence() {
		return isPresent(AWProprietaryandConfidentialCloseBtn);
	}
	
	public void select_selectAgentPOS(String value){		
		Assert.assertTrue(this.selectAgentPOS_Presence(), "'Agent/POS' dropdown is absent on the dashboard page.");
		selectDropDownByVisibleText(selectAgentPOS, value);
		takeScreenshot("Select 'Agent/POS'.");
	}
	
	public void click_approfileIconLnk(){		
		Assert.assertTrue(this.approfileIconLnk_Presence(), "'Profile Icon' link is absent on the dashboard page.");
		takeScreenshot("Click on the 'Profile Icon' link on the dashboard page.");
		click(approfileIconLnk);			
	}
	
	public void click_profileIconLnk(){		
		Assert.assertTrue(this.profileIconLnk_Presence(), "'Profile Icon' link is absent on the dashboard page.");
		takeScreenshot("Click on the 'Profile Icon' link on the dashboard page.");
		click(profileIconLnk);			
	}
	
	public void click_aplogoutLnk(){		
		Assert.assertTrue(this.aplogoutLnk_Presence(), "'Logout' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Logout' link on the dashboard page.");
		click(aplogoutLnk);			
	}
	
	public void click_logoutLnk(){		
		Assert.assertTrue(this.logoutLnk_Presence(), "'Logout' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Logout' link on the dashboard page.");
		click(logoutLnk);			
	}
	
	public void click_versionLnk(){		
		Assert.assertTrue(this.versionLnk_Presence(), "'Version' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Version' link on the dashboard page.");
		click(versionLnk);			
	}
	
	public void click_newsHeaderLnk(){			
		Assert.assertTrue(this.newsHeaderLnk_Presence(), "'News' header link is absent on the dashboard page.");
		takeScreenshot("Click on 'News' header link on the dashboard page.");
		click(newsHeaderLnk);			
	}
	
	public void click_transactHeaderLnk(){			
		Assert.assertTrue(this.transactHeaderLnk_Presence(), "'Transact' header link is absent on the dashboard page.");
		takeScreenshot("Click on 'Transact' header link on the dashboard page.");
		click(transactHeaderLnk);			
	}
	
	public void click_sendMoneyBtn(){		
		Assert.assertTrue(this.sendMoneyBtn_Presence(), "'SendMoney' button is absent on the dashboard page.");
		takeScreenshot("Click on 'SendMoney' button on the dashboard page.");
		click(sendMoneyBtn);			
	}
	
	public void click_receiveMoneyBtn(){		
		Assert.assertTrue(this.receiveMoneyBtn_Presence(), "'ReceiveMoney' button is absent on the dashboard page.");
		takeScreenshot("Click on 'ReceiveMoney' button on the dashboard page.");
		click(receiveMoneyBtn);		
	}
	
	public void click_estimateFeeBtn(){		
		Assert.assertTrue(this.estimateFeeBtn_Presence(), "'Estimate Fee' button is absent on the dashboard page.");		
		takeScreenshot("Click on 'Estimate Fee' button on the dashboard page.");
		click(estimateFeeBtn);	
	}
	
	public void click_editTransferBtn(){		
		Assert.assertTrue(this.editTransferBtn_Presence(), "'Edit Transfer' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Edit Transfer' button on the dashboard page.");
		click(editTransferBtn);			
	}
	
	public void click_completeMobileOrKioskTransactionBtn(){		
		Assert.assertTrue(this.completeMobileOrKioskTransactionBtn_Presence(), "'Complete Mobile Or Kiosk Transaction' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Complete Mobile Or Kiosk Transaction' button on the dashboard page.");
		click(completeMobileOrKioskTransactionBtn);		
	}
	
	public void click_moneyOrderBtn(){		
		Assert.assertTrue(this.moneyOrderBtn_Presence(), "'Money Order' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Money Order' button on the dashboard page.");
		click(moneyOrderBtn);		
	}
	
	public void click_payaVendorBtn(){		
		Assert.assertTrue(this.payaVendorBtn_Presence(), "'Pay a Vendor' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Pay a Vendor' button on the dashboard page.");
		click(payaVendorBtn);		
	}
	
	public void click_payaBillBtn(){		
		Assert.assertTrue(this.payaBillBtn_Presence(), "'Pay a Bill' button is absent on the dashboard page.");		
		takeScreenshot("Click on 'Pay a Bill' button on the dashboard page.");
		click(payaBillBtn);	
	}
	
	public void click_viewStatusBtn(){		
		Assert.assertTrue(this.viewStatusBtn_Presence(), "'View Status' button is absent on the dashboard page.");			
		takeScreenshot("Click on 'View Status' button on the dashboard page.");
		click(viewStatusBtn);
	}
	
	public void click_loadacardBtn(){		
		Assert.assertTrue(this.loadacardBtn_Presence(), "'Load a Card' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Load a Card' button on the dashboard page.");
		click(loadacardBtn);		
	}	
	
	public void click_clearCacheBtn(){		
		Assert.assertTrue(this.clearCacheBtn_Presence(), "'Clear cache' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Clear cache' button on the dashboard page.");
		click(clearCacheBtn);		
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "'Close' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Close' button on the dashboard page.");
		click(closeBtn);		
	}	
	
	public void click_AWProprietaryandConfidentialCloseBtn(){		
		Assert.assertTrue(this.AWProprietaryandConfidentialCloseBtn_Presence(), "'Close' button is absent on the dashboard page.");
		takeScreenshot("Click on 'Close' button on the dashboard page.");
		click(AWProprietaryandConfidentialCloseBtn);		
	}	
	
	public void check_ForSpinner() {
		waitForElementToBeGone(driver.findElement(spinner),60);
	}	
	
	public void mouseOver_reportsHeaderLnk(){		
		Assert.assertTrue(this.reportsHeaderLnk_Presence(), "'Reports' header link is absent on the dashboard page.");
		takeScreenshot("Mouse over on 'Reports' header link on the dashboard page.");
		mouseOver(reportsHeaderLnk);		
	}	
	
	public void click_dailyTransactionActivityLnk(){		
		Assert.assertTrue(this.dailyTransactionActivityLnk_Presence(), "'Daily Transaction Activity' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Daily Transaction Activity' link on the dashboard page.");
		click(dailyTransactionActivityLnk);			
	}
	
	public void click_proprietaryandconfidentialLnk(){		
		Assert.assertTrue(this.proprietaryandconfidentialLnk_Presence(), "'Proprietary and confidential' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Proprietary and confidential' link on the dashboard page.");
		click(proprietaryandconfidentialLnk);		
		takeScreenshot("'Proprietary and confidential'.");
	}
	
	public void click_termsofUselLnk(){		
		Assert.assertTrue(this.termsofUselLnk_Presence(), "'Terms of Use' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Terms of Use' link on the dashboard page.");
		click(termsofUselLnk);
		takeScreenshot("'Terms of Use'.");
	}
	
	public void click_findLocationLnk(){		
		Assert.assertTrue(this.findLocationLnk_Presence(), "'Find Locations' link is absent on the dashboard page.");
		takeScreenshot("Click on 'Find Locations' link on the dashboard page.");
		click(findLocationLnk);			
	}
	
	public void switchToFindLocations() throws AWTException{	
		switchToOtherBrowserTab();
		takeScreenshot("Find Locations.");
		closeOtherBrowserTab();
	}
	
	public void logout() {
		click_profileIconLnk();
		click_logoutLnk();
	}
	
	public void aplogout() {
		click_approfileIconLnk();
		click_aplogoutLnk();
	}
}
