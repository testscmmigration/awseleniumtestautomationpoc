package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class DailyTranReportsPage extends BasePage {
	
	//Drop downs
	private By selectReport  = getLocator("transactionReports", BY_TYPE.BY_ID);	
	
	//Text boxes
	private By selectActivityDateTxt = getLocator("datePicker", BY_TYPE.BY_NAME);
	
	//Select POS(s)
	private By pOSList = getLocator("//*[@class='col-4 col-sm-4 col-md-4 no-outline']", BY_TYPE.BY_XPATH);
	
	//Buttons
	private By runReportBtn = getLocator("//div[@id='ActionBarMainNextRunReport']/button", BY_TYPE.BY_XPATH);

	public DailyTranReportsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectReport_Presence() {
		return isPresent(selectReport);
	}
	
	public boolean selectActivityDateTxt_Presence() {
		return isPresent(selectActivityDateTxt);
	}
	
	public boolean pOSList_Presence() {
		return isPresent(pOSList);
	}
	
	public boolean runReportBtn_Presence() {
		return isPresent(runReportBtn);
	}
	
	public void select_selectReport(String value){		
		Assert.assertTrue(this.selectReport_Presence(), "Select 'Select report' dropdown is absent on the daily tran reports page.");
		selectDropDownByVisibleText(selectReport, value);
		takeScreenshot("Select 'Select report'.");
	}
	
	public void type_selectActivityDateTxt(String value) {
		Assert.assertTrue(this.selectActivityDateTxt_Presence(), "'Select activity date' textbox is absent on the daily tran reports page.");
		type(selectActivityDateTxt, value);		
		takeScreenshot("Enter 'Select activity date'.");
	}
	
	public void choose_pOSList(String value) {
		String[] pOSs = value.split("\\s");		
		Assert.assertTrue(this.pOSList_Presence(), "POS list is absent");		
		List<WebElement> myElements = driver.findElements(pOSList);
		if(myElements.size()>0) {			
			for(int i=0; i<pOSs.length; i++) {
				String epos = pOSs[i];
				for(int j=1; j<=myElements.size(); j++) {					
					WebElement myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/div/div/div/div/div/div/div/content-body/div/shared-scope-transclude/form/div[3]/div[2]/div[2]/div[2]/div/div["+j+"]/div/strong"));
					String apos = myElement.getText();					
					if(epos.equals(apos)) {					
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);						
						myElement.click();	
						takeScreenshot("Select the POS "+ epos +" from the list of POSs.");
						break;
					}					
				}
			}
		}
	}
	
	public void click_runReportBtn(){		
		Assert.assertTrue(this.runReportBtn_Presence(), "'Run Report' button is absent on the daily tran reports page.");
		takeScreenshot("Click on 'Run Report' button on the daily tran reports page.");
		click(runReportBtn);			
	}
}