package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class ReceiverContactInfoPage extends BasePage{
	
	//Text Boxes
	private By receiverPrimaryPhoneCountryCodeTxt = getLocator("Primary Phone Country Code", BY_TYPE.BY_ID);
	private By receiverPrimaryPhoneTxt = getLocator("//input[@id='receiver_PrimaryPhone']", BY_TYPE.BY_XPATH);
	private By receiverEmailTxt = getLocator("receiver_Email", BY_TYPE.BY_ID);	
	
	//Check boxes
	private By receiverMobilePhoneChk = getLocator("receiver_PrimaryPhoneSMSEnabled_", BY_TYPE.BY_ID);
	private By rceeiverEnrollInPlusProgramChk = getLocator("receiver_rewards_EnrollmentFlag_", BY_TYPE.BY_ID);
	
	//Drop downs
	private By selectReceiverReceiveTransactionStatus = getLocator("receiver_Transaction_NotificationOptedCode", BY_TYPE.BY_ID);
	private By selectReceiverReceiveOffers = getLocator("receiver_Marketing_NotificationOptedCode", BY_TYPE.BY_ID);
	private By selectReceiverPreferredLanguage = getLocator("receiver_PreferredLanguage", BY_TYPE.BY_ID);	
	
	//Country list
	private By selectCountryList = getLocator("uib-typeahead-match", BY_TYPE.BY_CLASSNAME);
	
	public ReceiverContactInfoPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean receiverPrimaryPhoneCountryCodeTxt_Presence() {
		return isPresent(receiverPrimaryPhoneCountryCodeTxt);
	}
	
	public boolean receiverPrimaryPhoneTxt_Presence() {
		return isPresent(receiverPrimaryPhoneTxt);
	}
	
	public boolean receiverMobilePhoneChk_Presence() {
		return isPresent(receiverMobilePhoneChk);
	}
	
	public boolean receiverEmailTxt_Presence() {
		return isPresent(receiverEmailTxt);
	}	
	
	public boolean selectReceiverReceiveTransactionStatus_Presence() {
		return isPresent(selectReceiverReceiveTransactionStatus);
	}
	
	public boolean selectReceiverReceiveOffers_Presence() {
		return isPresent(selectReceiverReceiveOffers);
	}
	
	public boolean selectReceiverPreferredLanguage_Presence() {
		return isPresent(selectReceiverPreferredLanguage);
	}
	
	public boolean receiverEnrollInPlusProgramChk_Presence() {
		return isPresent(rceeiverEnrollInPlusProgramChk);
	}
	
	public void type_receiverPrimaryPhoneCountryCodeTxt(String value) {
		Assert.assertTrue(this.receiverPrimaryPhoneCountryCodeTxt_Presence(), "Primary Phone Country Code textbox is absent on the receiver contact info page.");
		type(receiverPrimaryPhoneCountryCodeTxt, value);	
		takeScreenshot("Enter Rceeiver Primary Phone Country Code.");
	}
	
	public void selectRequiredCountry(String value) {
		List<WebElement> myElements = driver.findElements(selectCountryList);
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				WebElement myElement = driver.findElement(By.xpath("//li["+i+"]/a/div/div[2]/span[1]"));
				String country = myElement.getText();
				//System.out.println(country);
				if(value.equals(country)) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Click on the country required: "+value);
					myElement.click();
					break;
				}
			}
		}
	}
	
	public void type_receiverPrimaryPhoneTxt(String value) {
		Assert.assertTrue(this.receiverPrimaryPhoneTxt_Presence(), "Primary Phone Number textbox is absent on the receiver contact info page.");
		type(receiverPrimaryPhoneTxt, value);	
		takeScreenshot("Enter Receiver Primary Phone Number.");
	}
	
	public void check_receiverMobilePhoneChk() throws InterruptedException {		
		Assert.assertTrue(this.receiverMobilePhoneChk_Presence(), "Receiver mobile Phone checkbox is absent on the receiver contact info page.");
		click(receiverMobilePhoneChk);	
		takeScreenshot("Check Mobile Phone.");
	}
	
	public void type_receiverEmailTxt(String value) {
		Assert.assertTrue(this.receiverEmailTxt_Presence(), "Email textbox is absent on the receiver contact info page.");
		type(receiverEmailTxt, value);	
		takeScreenshot("Enter Receiver Email.");
	}
	
	public void select_selectReceiverReceiveTransactionStatus(String value){		
		Assert.assertTrue(this.selectReceiverReceiveTransactionStatus_Presence(), "'Receive Transaction Status' dropdown is absent on the receiver contact info page.");
		selectDropDownByVisibleText(selectReceiverReceiveTransactionStatus, value);
		takeScreenshot("Select 'Receive Transaction Status'.");
	}
	
	public void select_selectReceiverReceiveOffers(String value){		
		Assert.assertTrue(this.selectReceiverReceiveOffers_Presence(), "'Receive Offers' dropdown is absent on the receiver contact info page.");
		selectDropDownByVisibleText(selectReceiverReceiveOffers, value);
		takeScreenshot("Select 'Receive Offers'.");
	}
	
	public void select_selectReceiverPreferredLanguage(String value){		
		Assert.assertTrue(this.selectReceiverPreferredLanguage_Presence(), "'Preferred Language' dropdown is absent on the receiver contact info page.");
		selectDropDownByVisibleText(selectReceiverPreferredLanguage, value);
		takeScreenshot("Select 'Preferred Language'.");
	}
	
	public void check_receiverEnrollInPlusProgramChk() throws InterruptedException {		
		Assert.assertTrue(this.receiverEnrollInPlusProgramChk_Presence(), "Receiver Enroll in Plus Program checkbox is absent on the receiver contact info page.");
		click(rceeiverEnrollInPlusProgramChk);	
		takeScreenshot("Check Enroll in Plus Program.");
	}
}
