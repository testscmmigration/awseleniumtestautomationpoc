package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class SenderIDsPage extends BasePage {
	
	//Drop Downs	
	private By selectPhotoIDType  = getLocator("sender_PersonalId1_Type", BY_TYPE.BY_ID);
	private By selectSecondFormOfIDType  = getLocator("sender_PersonalId2_Type", BY_TYPE.BY_ID);
	private By selectPhotoIDChoice  = getLocator("sender_PersonalId1_Choice", BY_TYPE.BY_ID);
	private By selectSecondFormOfIDChoice  = getLocator("sender_PersonalId2_Choice", BY_TYPE.BY_ID);
		
	private By selectDOBYear = getLocator("//date-dropdown[@id='sender_DOB']/div/div[1]/select", BY_TYPE.BY_XPATH);
	private By selectDOBMonth = getLocator("//date-dropdown[@id='sender_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);
	private By selectDOBDay = getLocator("//date-dropdown[@id='sender_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
	
	private By selectOccupation  = getLocator("sender_Occupation", BY_TYPE.BY_ID);	
			
	//Text boxes	
	private By photoIDNumberTxt  = getLocator("sender_PersonalId1_Number", BY_TYPE.BY_ID);
	private By photoIDIssueCountryTxt  = getLocator("ID Issue Country", BY_TYPE.BY_ID);
	private By photoIDIssueStateOrProvinceTxt  = getLocator("ID Issue State/Province", BY_TYPE.BY_ID);
	private By photoIDLast4digitsofIDTxt = getLocator("sender_PersonalId1_VerificationStr", BY_TYPE.BY_ID);	
	
	private By secondFormOfIDNumberTxt  = getLocator("sender_PersonalId2_Number", BY_TYPE.BY_ID);
	private By secondFormOfIDLast4digitsofIDTxt  = getLocator("sender_PersonalId2_VerificationStr", BY_TYPE.BY_ID);	
	
	private By birthCountryTxt  = getLocator("Birth Country", BY_TYPE.BY_ID);	
	private By otherOccupationTxt = getLocator("sender_OccupationOther", BY_TYPE.BY_ID);
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
		
	public SenderIDsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectPhotoIDType_Presence() {
		return isPresent(selectPhotoIDType);
	}
	
	public boolean selectSecondFormOfIDType_Presence() {
		return isPresent(selectSecondFormOfIDType);
	}
	
	public boolean selectPhotoIDChoice_Presence() {
		return isPresent(selectPhotoIDChoice);
	}
	
	public boolean selectSecondFormOfIDChoice_Presence() {
		return isPresent(selectSecondFormOfIDChoice);
	}
	
	public boolean selectDOBYear_Presence() {
		return isPresent(selectDOBYear);
	}
	
	public boolean selectDOBMonth_Presence() {
		return isPresent(selectDOBMonth);
	}
	
	public boolean selectDOBDay_Presence() {
		return isPresent(selectDOBDay);
	}
	
	public boolean selectOccupation_Presence() {
		return isPresent(selectOccupation);
	}
	
	public boolean otherOccupationTxt_Presence() {
		return isPresent(otherOccupationTxt);
	}
	
	public boolean photoIDNumberTxt_Presence() {
		return isPresent(photoIDNumberTxt);
	}
	
	public boolean photoIDIssueCountryTxt_Presence() {
		return isPresent(photoIDIssueCountryTxt);
	}
	
	public boolean photoIDIssueStateOrProvinceTxt_Presence() {
		return isPresent(photoIDIssueStateOrProvinceTxt);
	}
	
	public boolean photoIDLast4digitsofIDTxt_Presence() {
		return isPresent(photoIDLast4digitsofIDTxt);
	}
	
	public boolean secondFormOfIDNumberTxt_Presence() {
		return isPresent(secondFormOfIDNumberTxt);
	}
	
	public boolean secondFormOfIDLast4digitsofIDTxt_Presence() {
		return isPresent(secondFormOfIDLast4digitsofIDTxt);
	}
	
	public boolean birthCountryTxt_Presence() {
		return isPresent(birthCountryTxt);
	}
	
	public void select_selectPhotoIDType(String value){		
		Assert.assertTrue(this.selectPhotoIDType_Presence(), "Photo ID: ID Type, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectPhotoIDType, value);
		takeScreenshot("Select Photo ID: ID Type.");
	}
	
	public void select_selectSecondFormOfIDType(String value){		
		Assert.assertTrue(this.selectSecondFormOfIDType_Presence(), "Second form of ID: Secondary ID Type, dropdown is absent on the sneder IDs page.");
		selectDropDownByVisibleText(selectSecondFormOfIDType, value);
		takeScreenshot("Select Second form of ID: Secondary ID Type.");
	}
	
	public void select_selectPhotoIDChoice(String value){		
		Assert.assertTrue(this.selectPhotoIDChoice_Presence(), "Photo ID: ID Choice, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectPhotoIDChoice, value);
		takeScreenshot("Select Photo ID: ID Choice.");
	}
	
	public void select_selectSecondFormOfIDChoice(String value){		
		Assert.assertTrue(this.selectSecondFormOfIDChoice_Presence(), "Second form of ID: Secondary ID Choice, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectSecondFormOfIDChoice, value);
		takeScreenshot("Select Second form of ID: Secondary ID Choice.");
	}
	
	public void select_selectDOBYear(String value){		
		Assert.assertTrue(this.selectDOBYear_Presence(), "Sender Date of Birth: Year, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectDOBYear, value);
		takeScreenshot("Select sender Date of Birth: Year.");
	}
	
	public void select_selectDOBMonth(String value){		
		Assert.assertTrue(this.selectDOBMonth_Presence(), "Sender Date of Birth: Month, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectDOBMonth, value);
		takeScreenshot("Select sender Date of Birth: Month.");
	}
	
	public void select_selectDOBDay(String value){		
		Assert.assertTrue(this.selectDOBDay_Presence(), "Sender Date of Birth: Day, dropdown is absent on the sender IDs page.");
		selectDropDownByVisibleText(selectDOBDay, value);
		takeScreenshot("Select sender Date of Birth: Day.");
	}
	
	public void select_selectOccupation(){		
		Assert.assertTrue(this.selectOccupation_Presence(), "Sender occupation dropdown is absent on the sneder IDs page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 24));
		if(index == 24) {
			selectDropDownByIndex(selectOccupation, index);
			takeScreenshot("Select sender occupation.");
			type_otherOccupationTxt("GOD");			
		}else {
			selectDropDownByIndex(selectOccupation, index);
			takeScreenshot("Select sender occupation.");
		}
	}
	
	public String get_selectedOccupation(){
		return getSelectedDropDownValue(selectOccupation);
	}
	
	public void type_photoIDNumberTxt(String value) {
		Assert.assertTrue(this.photoIDNumberTxt_Presence(), "Photo ID: ID Number, textbox is absent on the sender IDs page.");
		type(photoIDNumberTxt, value);		
		takeScreenshot("Enter Photo ID: ID Number.");
	}
	
	public void type_photoIDIssueCountryTxt(String value) {
		Assert.assertTrue(this.photoIDIssueCountryTxt_Presence(), "Photo ID: ID Issue Country, textbox is absent on the sender IDs page.");
		type(photoIDIssueCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue Country.");
	}
	
	public void type_photoIDIssueStateOrProvinceTxt(String value) {
		Assert.assertTrue(this.photoIDIssueStateOrProvinceTxt_Presence(), "Photo ID: ID Issue State/Province, textbox is absent on the sender IDs page.");
		type(photoIDIssueStateOrProvinceTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue State/Province.");
	}
	
	public void type_photoIDLast4digitsofIDTxt(String value) {
		Assert.assertTrue(this.photoIDLast4digitsofIDTxt_Presence(), "Photo ID: Last 4 digits of ID, textbox is absent on the sender IDs page.");
		type(photoIDLast4digitsofIDTxt, value);			
		takeScreenshot("Enter Photo ID: Last 4 digits of ID.");
	}
	
	public void type_secondFormOfIDNumberTxt(String value) {
		Assert.assertTrue(this.secondFormOfIDNumberTxt_Presence(), "Second form of ID: Secondary ID Number, textbox is absent on the sender IDs page.");
		type(secondFormOfIDNumberTxt, value);		
		takeScreenshot("Enter Second form of ID: Secondary ID Number.");
	}
	
	public void type_secondFormOfIDLast4digitsofIDTxt(String value) {
		Assert.assertTrue(this.secondFormOfIDLast4digitsofIDTxt_Presence(), "Second form of ID: Last 4 digits of ID, textbox is absent on the sender IDs page.");
		type(secondFormOfIDLast4digitsofIDTxt, value);		
		takeScreenshot("Enter Second form of ID: Last 4 digits of ID.");
	}
	
	public void type_birthCountryTxt(String value) {
		Assert.assertTrue(this.birthCountryTxt_Presence(), "Sender Birth Country, textbox is absent on the sender IDs page.");
		type(birthCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter sender birth country.");
	}
	
	public void type_otherOccupationTxt(String value) {
		Assert.assertTrue(this.otherOccupationTxt_Presence(), "Sender Other Occupation, textbox is absent on the sender IDs page.");
		type(otherOccupationTxt, value);		
		takeScreenshot("Enter sender other occupation.");
	}	
}
