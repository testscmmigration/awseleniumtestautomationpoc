package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class DeliveryOptionsPage extends BasePage {
	
	//Links
	private By cashOptionsLink = getLocator("Cash Options", BY_TYPE.BY_LINKTEXT);
	private By accountDepositLink = getLocator("Account Deposit", BY_TYPE.BY_LINKTEXT);
	private By homeDeliveryLink = getLocator("Home Delivery", BY_TYPE.BY_LINKTEXT);
	
	//Delivery options
	private By min10DlvrOpt = getLocator("//strong[contains(text(),'10 Minute Service')]",BY_TYPE.BY_XPATH);
	private By acctDepDlvrOpt = getLocator("//strong[contains(text(),'Account Deposit - PAYPAL')]",BY_TYPE.BY_XPATH);
	private By homeDlvrOpt = getLocator("//strong[contains(text(),'Home Delivery - DONGA BANK')]",BY_TYPE.BY_XPATH);
	private By hr24DlvrOpt = getLocator("//strong[contains(text(),'24 Hour Service')]",BY_TYPE.BY_XPATH);
	private By hr48DlvrOpt = getLocator("//strong[contains(text(),'48 Hour Service')]",BY_TYPE.BY_XPATH);
	
	public DeliveryOptionsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean cashOptionsLink_Presence() {
		return isPresent(cashOptionsLink);
	}
	
	public boolean min10DlvrOpt_Presence() {
		return isPresent(min10DlvrOpt);
	}
	
	public boolean hr24DlvrOpt_Presence() {
		return isPresent(hr24DlvrOpt);
	}
	
	public boolean hr48DlvrOpt_Presence() {
		return isPresent(hr48DlvrOpt);
	}
	
	public boolean accountDepositLink_Presence() {
		return isPresent(accountDepositLink);
	}
	
	public boolean acctDepDlvrOpt_Presence() {
		return isPresent(acctDepDlvrOpt);
	}
	
	public boolean homeDeliveryLink_Presence() {
		return isPresent(homeDeliveryLink);
	}
	
	public boolean homeDlvrOpt_Presence() {
		return isPresent(homeDlvrOpt);
	}
	
	public void click_cashOptionsLink(){		
		Assert.assertTrue(this.cashOptionsLink_Presence(), "'Cash Options' link is absent on the delivery-options page.");
		takeScreenshot("Click on 'Cash Options' link on the delivery-options page.");
		click(cashOptionsLink);			
	}
	
	public void click_min10DlvrOpt() {		
		Assert.assertTrue(this.min10DlvrOpt_Presence(), "Delivery Option '10 Minute Service' is absent on the delivery-options page.");
		takeScreenshot("Select '10 Minute Service' delivery option.");
		click(min10DlvrOpt);		
	}
	
	public void click_hr24DlvrOpt() {		
		Assert.assertTrue(this.hr24DlvrOpt_Presence(), "Delivery Option '24 Hour Service' is absent on the delivery-options page.");
		takeScreenshot("Select '24 Hour Service' delivery option.");
		click(hr24DlvrOpt);		
	}
	
	public void click_hr48DlvrOpt() {		
		Assert.assertTrue(this.hr48DlvrOpt_Presence(), "Delivery Option '48 Hour Service' is absent on the delivery-options page.");
		takeScreenshot("Select '48 Hour Service' delivery option.");
		click(hr48DlvrOpt);		
	}
	
	public void click_accountDepositLink(){		
		Assert.assertTrue(this.accountDepositLink_Presence(), "'Account Deposit' link is absent on the delivery-options page.");
		takeScreenshot("Click on 'Account Deposit' link on the delivery-options page.");
		click(accountDepositLink);			
	}
	
	public void click_acctDepDlvrOpt() {		
		Assert.assertTrue(this.acctDepDlvrOpt_Presence(), "Delivery Option 'Account Deposit - PAYPAL' is absent on the delivery-options page.");
		takeScreenshot("Select 'Account Deposit - PAYPAL' delivery option.");
		click(acctDepDlvrOpt);		
	}
	
	public void click_homeDeliveryLink(){		
		Assert.assertTrue(this.homeDeliveryLink_Presence(), "'Home Delivery' link is absent on the delivery-options page.");
		takeScreenshot("Click on 'Home Delivery' link on the delivery-options page.");
		click(homeDeliveryLink);			
	}
	
	public void click_homeDlvrOpt() {		
		Assert.assertTrue(this.homeDlvrOpt_Presence(), "Delivery Option 'Home Delivery - DONGA BANK' is absent on the delivery-options page.");
		takeScreenshot("Select 'Home Delivery - DONGA BANK' delivery option.");
		click(homeDlvrOpt);		
	}	
}
