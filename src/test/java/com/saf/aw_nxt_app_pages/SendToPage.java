package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class SendToPage extends BasePage{	
	
	//Links
	private By sendToANewReceiverLnk = getLocator("lnkNewReceiver", BY_TYPE.BY_ID);
	
	//Previous receivers list
	private By prvReceiverSearchResultList = getLocator("//*[@class='table-borders cursor-pointer padding-5 margin-bottom-10 highlight-border mg-button-shadow']", BY_TYPE.BY_XPATH);
	
	public SendToPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean sendToANewReceiverLnk_Presence() {
		return isPresent(sendToANewReceiverLnk);
	}
	
	public boolean prvReceiverSearchResultList_Presence() {
		return isPresent(prvReceiverSearchResultList);
	}
	
	public void click_sendToANewReceiverLnk() {
		Assert.assertTrue(this.sendToANewReceiverLnk_Presence(), "'Send to a new receiver' link is absent on the send-to page.");
		click(sendToANewReceiverLnk);		
	}
	
	public void choose_prvReceiverSearchResultList(String tranType, String value) {
		Assert.assertTrue(this.prvReceiverSearchResultList_Presence(), "Search result for the receiver is absent");		
		List<WebElement> myElements = driver.findElements(prvReceiverSearchResultList);
		WebElement myElement = null;
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				if(tranType.equalsIgnoreCase("send")) {
					myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/send-send-to/content-body/div/shared-scope-transclude/div/div[2]/div["+i+"]/div/div/div[2]/div/strong"));
				}
				else if(tranType.equalsIgnoreCase("efee")){
					myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/efee-send-to/content-body/div/shared-scope-transclude/div/div[2]/div["+i+"]/div/div/div[2]/div/strong"));
				}
				String receiverName = myElement.getText();
				if(value.equalsIgnoreCase(receiverName)) {					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Select the receiver from the list of previous receivers."+value);
					myElement.click();
					break;
				}
			}
		}						
	}	
}
