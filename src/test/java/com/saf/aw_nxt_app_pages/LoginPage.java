package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class LoginPage extends BasePage{
	
	//Text boxes
	private By userIDTxt  = getLocator("IDToken1", BY_TYPE.BY_ID);
	private By passwordTxt  = getLocator("IDToken2", BY_TYPE.BY_ID);
	
	//Buttons
	private By signInBtn  = getLocator("Login.Submit", BY_TYPE.BY_NAME);
	
	//Link
	private By forgotPasswordLink  = getLocator("Forgot password?", BY_TYPE.BY_LINKTEXT);
	
	//Drop Down
	private By selectLanguage  = getLocator("language", BY_TYPE.BY_ID);

	public LoginPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean userIDTxt_Presence() {
		return isPresent(userIDTxt);
	}
	
	public boolean PasswordTxt_Presence() {
		return isPresent(passwordTxt);
	}
	
	public boolean signInBtn_Presence() {
		return isPresent(signInBtn);
	}
	
	public boolean forgotPasswordLink_Presence() {
		return isPresent(forgotPasswordLink);
	}
	
	public boolean selectLanguage_Presence() {
		return isPresent(selectLanguage);
	}
	
	public void type_userIDTxt(String value) {
		Assert.assertTrue(this.userIDTxt_Presence(), "'User ID' textbox is absent on the login page.");
		type(userIDTxt, value);		
		takeScreenshot("Enter UserID.");
	}
	
	public void type_passwordTxt(String value) {
		Assert.assertTrue(this.PasswordTxt_Presence(), "'Password' textbox is absent on the login page.");
		type(passwordTxt, value);		
		takeScreenshot("Enter Password.");
	}
	
	public void click_signIn(){		
		Assert.assertTrue(this.signInBtn_Presence(), "SignIn button is absent on the login page.");
		takeScreenshot("Click on 'Sign In' button on the dashboard page.");
		click(signInBtn);			
	}
}
