package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class ThirdPartyInfoPage extends BasePage {
	
	//Radio Buttons
	private By thirdPartyInformationNoneRad = getLocator("//div[@id='STRKEY_IS_CUST_RECEIVING_radio']/div/label/span", BY_TYPE.BY_XPATH);
	private By thirdPartyInformationOrganizationRad = getLocator("//div[@id='STRKEY_IS_CUST_RECEIVING_radio']/div[2]/label/span", BY_TYPE.BY_XPATH);
	private By thirdPartyInformationPersonRad = getLocator("//div[@id='STRKEY_IS_CUST_RECEIVING_radio']/div[3]/label/span", BY_TYPE.BY_XPATH);
	
	//Text Boxes
	private By thirdPartySenderOrganizationNameTxt = getLocator("thirdParty_Sender_Organization", BY_TYPE.BY_ID);
	private By thirdPartyReceiverOrganizationNameTxt = getLocator("thirdParty_Receiver_Organization", BY_TYPE.BY_ID);
	private By thirdPartySenderPersonLastOrFamilyNameTxt = getLocator("thirdParty_Sender_LastName", BY_TYPE.BY_ID);
	private By thirdPartyReceiverPersonLastOrFamilyNameTxt = getLocator("thirdParty_Receiver_LastName", BY_TYPE.BY_ID);
	private By thirdPartySenderPersonFirstOrGivenNameTxt = getLocator("thirdParty_Sender_FirstName", BY_TYPE.BY_ID);
	private By thirdPartyReceiverPersonFirstOrGivenNameTxt = getLocator("thirdParty_Receiver_FirstName", BY_TYPE.BY_ID);
	private By thirdPartySenderPersonMiddleNameTxt = getLocator("thirdParty_Sender_MiddleName", BY_TYPE.BY_ID);	
	private By thirdPartyReceiverPersonMiddleNameTxt = getLocator("thirdParty_Receiver_MiddleName", BY_TYPE.BY_ID);
		
	private By thirdPartyCountryTxt = getLocator("Country", BY_TYPE.BY_ID);
	private By thirdPartySenderAddressTxt = getLocator("thirdParty_Sender_Address", BY_TYPE.BY_ID);
	private By thirdPartyReceiverAddressTxt = getLocator("thirdParty_Receiver_Address", BY_TYPE.BY_ID);
	private By thirdPartySenderCityTxt = getLocator("thirdParty_Sender_City", BY_TYPE.BY_ID);
	private By thirdPartyReceiverCityTxt = getLocator("thirdParty_Receiver_City", BY_TYPE.BY_ID);
	private By thirdPartyStateOrProvinceTxt = getLocator("State/Province", BY_TYPE.BY_ID);
	private By thirdPartySenderPostalCodeTxt = getLocator("thirdParty_Sender_PostalCode", BY_TYPE.BY_ID);
	private By thirdPartyReceiverPostalCodeTxt = getLocator("thirdParty_Receiver_PostalCode", BY_TYPE.BY_ID);
	private By thirdPartySenderIDNumberTxt = getLocator("thirdParty_Sender_PersonalId2_Number", BY_TYPE.BY_ID);
	private By thirdPartyReceiverIDNumberTxt = getLocator("thirdParty_Receiver_PersonalId2_Number", BY_TYPE.BY_ID);
	private By thirdPartySenderOtherOccupationTxt = getLocator("thirdParty_Sender_OccupationOther", BY_TYPE.BY_ID);
	private By thirdPartyReceiverOtherOccupationTxt = getLocator("thirdParty_Receiver_OccupationOther", BY_TYPE.BY_ID);
	
	
	//Drop Boxes
	private By selectThirdPartySenderIDType = getLocator("thirdParty_Sender_PersonalId2_Type", BY_TYPE.BY_ID);
	private By selectThirdPartyReceiverIDType = getLocator("thirdParty_Receiver_PersonalId2_Type", BY_TYPE.BY_ID);	
	private By selectThirdPartySenderDOBYear = getLocator("//date-dropdown[@id='thirdParty_Sender_DOB']/div/div/select", BY_TYPE.BY_XPATH);
	private By selectThirdPartyReceiverDOBYear = getLocator("//date-dropdown[@id='thirdParty_Receiver_DOB']/div/div[1]/select", BY_TYPE.BY_XPATH);	
	private By selectThirdPartySenderDOBMonth = getLocator("//date-dropdown[@id='thirdParty_Sender_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);
	private By selectThirdPartyReceiverDOBMonth = getLocator("//date-dropdown[@id='thirdParty_Receiver_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);	
	private By selectThirdPartySenderDOBDay = getLocator("//date-dropdown[@id='thirdParty_Sender_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
	private By selectThirdPartyReceiverDOBDay = getLocator("//date-dropdown[@id='thirdParty_Receiver_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
	private By selectThirdPartySenderOccupation = getLocator("thirdParty_Sender_Occupation", BY_TYPE.BY_ID);
	private By selectThirdPartyReceiverOccupation = getLocator("thirdParty_Receiver_Occupation", BY_TYPE.BY_ID);	
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);

	public ThirdPartyInfoPage(WebDriver driver) {
		super(driver);		
	}	
	
	public boolean thirdPartyInformationNoneRad_Presence() {
		return isPresent(thirdPartyInformationNoneRad);
	}
	
	public boolean thirdPartyInformationOrganizationRad_Presence() {
		return isPresent(thirdPartyInformationOrganizationRad);
	}
	
	public boolean thirdPartyInformationPersonRad_Presence() {
		return isPresent(thirdPartyInformationPersonRad);
	}
	
	public boolean thirdPartySenderOrganizationNameTxt_Presence() {
		return isPresent(thirdPartySenderOrganizationNameTxt);
	}
	
	public boolean thirdPartyReceiverOrganizationNameTxt_Presence() {
		return isPresent(thirdPartyReceiverOrganizationNameTxt);
	}
	
	public boolean thirdPartySenderPersonLastOrFamilyNameTxt_Presence() {
		return isPresent(thirdPartySenderPersonLastOrFamilyNameTxt);
	}
	
	public boolean thirdPartyReceiverPersonLastOrFamilyNameTxt_Presence() {
		return isPresent(thirdPartyReceiverPersonLastOrFamilyNameTxt);
	}
	
	public boolean thirdPartySenderPersonFirstOrGivenNameTxt_Presence() {
		return isPresent(thirdPartySenderPersonFirstOrGivenNameTxt);
	}
	
	public boolean thirdPartyReceiverPersonFirstOrGivenNameTxt_Presence() {
		return isPresent(thirdPartyReceiverPersonFirstOrGivenNameTxt);
	}
	
	public boolean thirdPartySenderPersonMiddleNameTxt_Presence() {
		return isPresent(thirdPartySenderPersonMiddleNameTxt);
	}	
	
	public boolean thirdPartyReceiverPersonMiddleNameTxt_Presence() {
		return isPresent(thirdPartyReceiverPersonMiddleNameTxt);
	}
	
	public boolean thirdPartyCountryTxt_Presence() {
		return isPresent(thirdPartyCountryTxt);
	}
	
	public boolean thirdPartySenderAddressTxt_Presence() {
		return isPresent(thirdPartySenderAddressTxt);
	}
	
	public boolean thirdPartyReceiverAddressTxt_Presence() {
		return isPresent(thirdPartyReceiverAddressTxt);
	}
	
	public boolean thirdPartySenderCityTxt_Presence() {
		return isPresent(thirdPartySenderCityTxt);
	}
	
	public boolean thirdPartyReceiverCityTxt_Presence() {
		return isPresent(thirdPartyReceiverCityTxt);
	}
	
	public boolean thirdPartySenderStateOrProvinceTxt_Presence() {
		return isPresent(thirdPartyStateOrProvinceTxt);
	}
	
	public boolean thirdPartySenderPostalCodeTxt_Presence() {
		return isPresent(thirdPartySenderPostalCodeTxt);
	}
	
	public boolean thirdPartyReceiverPostalCodeTxt_Presence() {
		return isPresent(thirdPartyReceiverPostalCodeTxt);
	}
	
	public boolean thirdPartySenderIDNumberTxt_Presence() {
		return isPresent(thirdPartySenderIDNumberTxt);
	}
	
	public boolean thirdPartyReceiverIDNumberTxt_Presence() {
		return isPresent(thirdPartyReceiverIDNumberTxt);
	}
	
	public boolean thirdPartySenderOtherOccupationTxt_Presence() {
		return isPresent(thirdPartySenderOtherOccupationTxt);
	}
	
	public boolean thirdPartyReceiverOtherOccupationTxt_Presence() {
		return isPresent(thirdPartyReceiverOtherOccupationTxt);
	}
	
	public boolean selectThirdPartySenderIDType_Presence() {
		return isPresent(selectThirdPartySenderIDType);
	}
	
	public boolean selectThirdPartyReceiverIDType_Presence() {
		return isPresent(selectThirdPartyReceiverIDType);
	}
	
	public boolean selectThirdPartySenderDOBYear_Presence() {
		return isPresent(selectThirdPartySenderDOBYear);
	}
	
	public boolean selectThirdPartyReceiverDOBYear_Presence() {
		return isPresent(selectThirdPartyReceiverDOBYear);
	}
	
	public boolean selectThirdPartySenderDOBMonth_Presence() {
		return isPresent(selectThirdPartySenderDOBMonth);
	}
	
	public boolean selectThirdPartyReceiverDOBMonth_Presence() {
		return isPresent(selectThirdPartyReceiverDOBMonth);
	}
	
	public boolean selectThirdPartySenderDOBDay_Presence() {
		return isPresent(selectThirdPartySenderDOBDay);
	}
	
	public boolean selectThirdPartyReceiverDOBDay_Presence() {
		return isPresent(selectThirdPartyReceiverDOBDay);
	}
	
	public boolean selectThirdPartySenderOccupation_Presence() {
		return isPresent(selectThirdPartySenderOccupation);
	}
	
	public boolean selectThirdPartyReceiverOccupation_Presence() {
		return isPresent(selectThirdPartyReceiverOccupation);
	}
	
	public void click_thirdPartyInformationNoneRad() {		
		Assert.assertTrue(this.thirdPartyInformationNoneRad_Presence(), "'None' radio button is absent on the third party information page.");		
		click(thirdPartyInformationNoneRad);
		takeScreenshot("Click on 'Third Party Information: None' radio button on the third party information page.");
	}
	
	public void click_thirdPartyInformationOrganizationRad() {		
		Assert.assertTrue(this.thirdPartyInformationOrganizationRad_Presence(), "'Organization' radio button is absent on the third party information page.");		
		click(thirdPartyInformationOrganizationRad);
		takeScreenshot("Click on 'Third Party Information: Organization' radio button on the third party information page.");
	}
	
	public void click_thirdPartyInformationPersonRad() {		
		Assert.assertTrue(this.thirdPartyInformationPersonRad_Presence(), "'Person' radio button is absent on the third party information page.");		
		click(thirdPartyInformationPersonRad);	
		takeScreenshot("Click on 'Third Party Information: Person' radio button on the third party information page.");
	}
	
	public void type_thirdPartySenderOrganizationNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderOrganizationNameTxt_Presence(), "Third party organization name textbox is absent on the third party information page.");
		type(thirdPartySenderOrganizationNameTxt, value);		
		takeScreenshot("Enter third party organization name.");
	}
	
	public void type_thirdPartyReceiverOrganizationNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverOrganizationNameTxt_Presence(), "Third party organization name textbox is absent on the third party information page.");
		type(thirdPartyReceiverOrganizationNameTxt, value);		
		takeScreenshot("Enter third party organization name.");
	}
	
	public void type_thirdPartySenderPersonLastOrFamilyNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderPersonLastOrFamilyNameTxt_Presence(), "Third party person last/family name textbox is absent on the third party information page.");
		type(thirdPartySenderPersonLastOrFamilyNameTxt, value);		
		takeScreenshot("Enter third party person last/family name.");
	}
	
	public void type_thirdPartyReceiverPersonLastOrFamilyNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverPersonLastOrFamilyNameTxt_Presence(), "Third party person last/family name textbox is absent on the third party information page.");
		type(thirdPartyReceiverPersonLastOrFamilyNameTxt, value);		
		takeScreenshot("Enter third party person last/family name.");
	}
	
	public void type_thirdPartySenderPersonFirstOrGivenNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderPersonFirstOrGivenNameTxt_Presence(), "Third party person first/given name textbox is absent on the third party information page.");
		type(thirdPartySenderPersonFirstOrGivenNameTxt, value);		
		takeScreenshot("Enter third party person first/given name.");
	}
	
	public void type_thirdPartyReceiverPersonFirstOrGivenNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverPersonFirstOrGivenNameTxt_Presence(), "Third party person first/given name textbox is absent on the third party information page.");
		type(thirdPartyReceiverPersonFirstOrGivenNameTxt, value);		
		takeScreenshot("Enter third party person first/given name.");
	}
	
	public void type_thirdPartySenderPersonMiddleNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderPersonMiddleNameTxt_Presence(), "Third party person middle name textbox is absent on the third party information page.");
		type(thirdPartySenderPersonMiddleNameTxt, value);		
		takeScreenshot("Enter third party person middle name.");
	}
	
	public void type_thirdPartyReceiverPersonMiddleNameTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverPersonMiddleNameTxt_Presence(), "Third party person middle name textbox is absent on the third party information page.");
		type(thirdPartyReceiverPersonMiddleNameTxt, value);		
		takeScreenshot("Enter third party person middle name.");
	}
	
	public void type_thirdPartyCountryTxt(String value) {		
		Assert.assertTrue(this.thirdPartyCountryTxt_Presence(), "Third party country textbox is absent on the third party information page.");
		type(thirdPartyCountryTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter third party country.");
	}
	
	public void type_thirdPartySenderAddressTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderAddressTxt_Presence(), "Third party address textbox is absent on the third party information page.");
		type(thirdPartySenderAddressTxt, value);		
		takeScreenshot("Enter third party address.");
	}
	
	public void type_thirdPartyReceiverAddressTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverAddressTxt_Presence(), "Third party address textbox is absent on the third party information page.");
		type(thirdPartyReceiverAddressTxt, value);		
		takeScreenshot("Enter third party address.");
	}
	
	public void type_thirdPartySenderCityTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderCityTxt_Presence(), "Third party city textbox is absent on the third party information page.");
		type(thirdPartySenderCityTxt, value);		
		takeScreenshot("Enter third party city.");
	}
	
	public void type_thirdPartyReceiverCityTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverCityTxt_Presence(), "Third party city textbox is absent on the third party information page.");
		type(thirdPartyReceiverCityTxt, value);		
		takeScreenshot("Enter third party city.");
	}
	
	public void type_thirdPartyStateOrProvinceTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderStateOrProvinceTxt_Presence(), "Third party state/province textbox is absent on the third party information page.");
		type(thirdPartyStateOrProvinceTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter third party state/province.");
	}
	
	public void type_thirdPartySenderPostalCodeTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderPostalCodeTxt_Presence(), "Third party postal code textbox is absent on the third party information page.");
		type(thirdPartySenderPostalCodeTxt, value);		
		takeScreenshot("Enter third party postal code.");
	}
	
	public void type_thirdPartyReceiverPostalCodeTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverPostalCodeTxt_Presence(), "Third party postal code textbox is absent on the third party information page.");
		type(thirdPartyReceiverPostalCodeTxt, value);		
		takeScreenshot("Enter third party postal code.");
	}
	
	public void type_thirdPartySenderIDNumberTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderIDNumberTxt_Presence(), "Third party id number textbox is absent on the third party information page.");
		type(thirdPartySenderIDNumberTxt, value);		
		takeScreenshot("Enter third party id number.");
	}
	
	public void type_thirdPartyReceiverIDNumberTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverIDNumberTxt_Presence(), "Third party id number textbox is absent on the third party information page.");
		type(thirdPartyReceiverIDNumberTxt, value);		
		takeScreenshot("Enter third party id number.");
	}
	
	public void type_thirdPartySenderOtherOccupationTxt(String value) {		
		Assert.assertTrue(this.thirdPartySenderOtherOccupationTxt_Presence(), "Third party other occupation textbox is absent on the third party information page.");
		type(thirdPartySenderOtherOccupationTxt, value);		
		takeScreenshot("Enter third party other occupation.");
	}
	
	public void type_thirdPartyReceiverOtherOccupationTxt(String value) {		
		Assert.assertTrue(this.thirdPartyReceiverOtherOccupationTxt_Presence(), "Third party other occupation textbox is absent on the third party information page.");
		type(thirdPartyReceiverOtherOccupationTxt, value);		
		takeScreenshot("Enter third party other occupation.");
	}
	
	public void select_selectThirdPartySenderIDType(String value) {		
		Assert.assertTrue(this.selectThirdPartySenderIDType_Presence(), "Third Party ID Type, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartySenderIDType, value);
		takeScreenshot("Select Third Party ID Type.");
	}
	
	public void select_selectThirdPartyReceiverIDType(String value) {		
		Assert.assertTrue(this.selectThirdPartyReceiverIDType_Presence(), "Third Party ID Type, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartyReceiverIDType, value);
		takeScreenshot("Select Third Party ID Type.");
	}
	
	public void select_selectThirdPartySenderDOBYear(String value) {		
		Assert.assertTrue(this.selectThirdPartySenderDOBYear_Presence(), "Third Party DOB year, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartySenderDOBYear, value);
		takeScreenshot("Select Third Party DOB year.");
	}
	
	public void select_selectThirdPartyReceiverDOBYear(String value) {		
		Assert.assertTrue(this.selectThirdPartyReceiverDOBYear_Presence(), "Third Party DOB year, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartyReceiverDOBYear, value);
		takeScreenshot("Select Third Party DOB year.");
	}
	
	public void select_selectThirdPartySenderDOBMonth(String value) {		
		Assert.assertTrue(this.selectThirdPartySenderDOBMonth_Presence(), "Third Party DOB month, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartySenderDOBMonth, value);
		takeScreenshot("Select Third Party DOB month.");
	}
	
	public void select_selectThirdPartyReceiverDOBMonth(String value) {		
		Assert.assertTrue(this.selectThirdPartyReceiverDOBMonth_Presence(), "Third Party DOB month, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartyReceiverDOBMonth, value);
		takeScreenshot("Select Third Party DOB month.");
	}
	
	public void select_selectThirdPartySenderDOBDay(String value) {		
		Assert.assertTrue(this.selectThirdPartySenderDOBDay_Presence(), "Third Party DOB day, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartySenderDOBDay, value);
		takeScreenshot("Select Third Party DOB day.");
	}
	
	public void select_selectThirdPartyReceiverDOBDay(String value) {		
		Assert.assertTrue(this.selectThirdPartyReceiverDOBDay_Presence(), "Third Party DOB day, dropdown is absent on the third party information page.");
		selectDropDownByVisibleText(selectThirdPartyReceiverDOBDay, value);
		takeScreenshot("Select Third Party DOB day.");
	}
	
	public void select_selectThirdPartySenderOccupation() {		
		Assert.assertTrue(this.selectThirdPartySenderOccupation_Presence(), "Third Party occupation, dropdown is absent on the third party information page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 24));
		if(index == 24) {
			selectDropDownByIndex(selectThirdPartySenderOccupation, index);
			type_thirdPartySenderOtherOccupationTxt("GOD");
			takeScreenshot("Select Third Party occupation.");
		}else {
			selectDropDownByIndex(selectThirdPartySenderOccupation, index);
			takeScreenshot("Select Third Party occupation.");
		}
	}
	
	public String get_selectedThirdPartySenderOccupation() {	
		return getSelectedDropDownValue(selectThirdPartySenderOccupation);
	}
	
	public void select_selectThirdPartyReceiverOccupation() {		
		Assert.assertTrue(this.selectThirdPartyReceiverOccupation_Presence(), "Third Party occupation, dropdown is absent on the third party information page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 24));
		if(index == 24) {
			selectDropDownByIndex(selectThirdPartyReceiverOccupation, index);
			type_thirdPartyReceiverOtherOccupationTxt("GOD");
			takeScreenshot("Select Third Party occupation.");
		}else {
			selectDropDownByIndex(selectThirdPartyReceiverOccupation, index);
			takeScreenshot("Select Third Party occupation.");
		}
	}
	
	public String get_selectedThirdPartyReceiverOccupation() {
		return getSelectedDropDownValue(selectThirdPartyReceiverOccupation);
	}
}
