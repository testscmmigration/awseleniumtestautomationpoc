package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class BillPaySenderInfoPage extends BasePage {

	//Text boxes
	private By lastOrFamilyNameTxt = getLocator("sender_LastName", BY_TYPE.BY_ID);
	private By firstOrGivenNameTxt = getLocator("sender_FirstName", BY_TYPE.BY_ID);
	private By middleNameTxt = getLocator("sender_MiddleName", BY_TYPE.BY_ID);
	private By countryTxt = getLocator("Country", BY_TYPE.BY_ID);
	private By addressTxt = getLocator("sender_Address", BY_TYPE.BY_ID);
	private By cityTxt = getLocator("sender_City", BY_TYPE.BY_ID);
	private By stateOrProvinceTxt = getLocator("State/Province", BY_TYPE.BY_ID);
	private By postalCodeTxt = getLocator("sender_PostalCode", BY_TYPE.BY_ID);
	private By primaryPhoneCountryCodeTxt = getLocator("Primary Phone Country Code", BY_TYPE.BY_ID);
	private By primaryPhoneNumberTxt = getLocator("sender_PrimaryPhone", BY_TYPE.BY_ID);
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
	//Text
	private By referenceNumberText = getLocator("//p/strong", BY_TYPE.BY_XPATH);
	
	//Buttons
	private By sendMoneyBtn = getLocator("ok", BY_TYPE.BY_ID);
	private By finishedBtn = getLocator("//div[2]/button", BY_TYPE.BY_XPATH);
	
	//Country list
	private By selectCountryList = getLocator("uib-typeahead-match", BY_TYPE.BY_CLASSNAME);
	
	public BillPaySenderInfoPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean lastOrFamilyNameTxt_Presence() {
		return isPresent(lastOrFamilyNameTxt);
	}
	
	public boolean firstOrGivenNameTxt_Presence() {
		return isPresent(firstOrGivenNameTxt);
	}
	
	public boolean middleNameTxt_Presence() {
		return isPresent(middleNameTxt);
	}
	
	public boolean countryTxt_Presence() {
		return isPresent(countryTxt);
	}
	
	public boolean addressTxt_Presence() {
		return isPresent(addressTxt);
	}
	
	public boolean cityTxt_Presence() {
		return isPresent(cityTxt);
	}
	
	public boolean stateOrProvinceTxt_Presence() {
		return isPresent(stateOrProvinceTxt);
	}
	
	public boolean postalCodeTxt_Presence() {
		return isPresent(postalCodeTxt);
	}
	
	public boolean primaryPhoneCountryCodeTxt_Presence() {
		return isPresent(primaryPhoneCountryCodeTxt);
	}
	
	public boolean sendMoneyBtn_Presence() {
		return isPresent(sendMoneyBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public boolean referenceNumberText_Presence() {
		return isPresent(referenceNumberText);
	}
	
	public boolean primaryPhoneNumberTxt_Presence() {
		return isPresent(primaryPhoneNumberTxt);
	}
	
	public void type_lastOrFamilyNameTxt(String value) {
		Assert.assertTrue(this.lastOrFamilyNameTxt_Presence(), "Last/Family Name textbox is absent on the bill pay sender info page.");
		type(lastOrFamilyNameTxt, value);		
		takeScreenshot("Enter Last/Family Name.");
	}
	
	public void type_firstOrGivenNameTxt(String value) {
		Assert.assertTrue(this.firstOrGivenNameTxt_Presence(), "First/Given Name  textbox is absent on the bill pay sender info page.");
		type(firstOrGivenNameTxt, value);		
		takeScreenshot("Enter First/Given Name.");
	}
	
	public void type_middleNameTxt(String value) {
		Assert.assertTrue(this.middleNameTxt_Presence(), "Middle Name textbox is absent on the bill pay sender info page.");
		type(middleNameTxt, value);		
		takeScreenshot("Enter Middle Name.");
	}
	
	public void type_countryTxt(String value) {
		Assert.assertTrue(this.countryTxt_Presence(), "Country textbox is absent on the bill pay sender info page.");
		type(countryTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter Country.");
	}
	
	public void type_addressTxt(String value) {
		Assert.assertTrue(this.addressTxt_Presence(), "Address textbox is absent on the bill pay sender info page.");
		type(addressTxt, value);		
		takeScreenshot("Enter Address.");
	}
	
	public void type_cityTxt(String value) {
		Assert.assertTrue(this.cityTxt_Presence(), "City textbox is absent on the bill pay sender info page.");
		type(cityTxt, value);		
		takeScreenshot("Enter City.");
	}
	
	public void type_stateOrProvinceTxt(String value) {
		Assert.assertTrue(this.stateOrProvinceTxt_Presence(), "State/Province textbox is absent on the bill pay sender info page.");
		type(stateOrProvinceTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter State/Province.");
	}
	
	public void type_postalCodeTxt(String value) {
		Assert.assertTrue(this.postalCodeTxt_Presence(), "Postal Code textbox is absent on the bill pay sender info page.");
		type(postalCodeTxt, value);		
		takeScreenshot("Enter Postal Code.");
	}
	
	public void type_primaryPhoneCountryCodeTxt(String value) {
		Assert.assertTrue(this.primaryPhoneCountryCodeTxt_Presence(), "Primary Phone Country Code textbox is absent on the bill pay sender info page.");
		type(primaryPhoneCountryCodeTxt, value);		
		takeScreenshot("Enter Primary Phone Country Code.");
	}
	
	public void type_primaryPhoneNumberTxt(String value) {
		Assert.assertTrue(this.primaryPhoneNumberTxt_Presence(), "Primary Phone Number textbox is absent on the bill pay sender info page.");
		type(primaryPhoneNumberTxt, value);		
		takeScreenshot("Enter Primary Phone Number.");
	}
	
	public void click_sendMoneyBtn(){		
		Assert.assertTrue(this.sendMoneyBtn_Presence(), "Send Money button is absent on the bill pay sender info page.");	
		takeScreenshot("Click on send money button on the bill pay sender info page.");
		click(sendMoneyBtn);		
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "Finished button is absent on the bill pay sender info page.");	
		takeScreenshot("Click on finished button on the bill pay sender info page.");
		click(finishedBtn);		
	}
	
	public String getReferenceNumber() {
		Assert.assertTrue(this.referenceNumberText_Presence(), "Reference Number text is absent on the bill pay sender info page.");
		return getText(referenceNumberText);
	}
	
	public void selectRequiredCountry(String value) {		
		List<WebElement> myElements = driver.findElements(selectCountryList);
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				WebElement myElement = driver.findElement(By.xpath("//li["+i+"]/a/div/div[2]/span[1]"));
				String country = myElement.getText();
				//System.out.println(country);
				if(value.equals(country)) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Click on the country required: "+value);
					myElement.click();
					break;
				}
			}
		}
	}
}
