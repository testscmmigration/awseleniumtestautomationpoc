package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class BillPaySendFromPage extends BasePage {	
	
	//Search results
	private By prvSenderSearchResultList = getLocator("//*[@class='table-borders cursor-pointer padding-5 margin-bottom-10 highlight-border mg-button-shadow']", BY_TYPE.BY_XPATH);
	
	public BillPaySendFromPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean prvSenderSearchResultList_Presence() {
		return isPresent(prvSenderSearchResultList);
	}	
	
	public void choose_prvSenderSearchResultList(String value) {
		Assert.assertTrue(this.prvSenderSearchResultList_Presence(), "Search result for the sender phone or plus number provided is absent");		
		List<WebElement> myElements = driver.findElements(prvSenderSearchResultList);
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				WebElement myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/bill-pay-send-from/content-body/div/shared-scope-transclude/div/div[2]/div["+i+"]/div/div/div/strong"));
				String senderName = myElement.getText();
				if(value.equals(senderName)) {					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Select the sender from the returned search results."+value);
					myElement.click();
					break;
				}
			}
		}						
	}
}
