package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class FindTranPage extends BasePage{
	//Text boxes
	private By referenceNumberTxt =  getLocator("STRKEY_REF_NUM",BY_TYPE.BY_ID);
	
	//Drop Downs
	private By selectDOBYear = getLocator("//date-dropdown[@id='receiver_DOB']/div/div/select", BY_TYPE.BY_XPATH);
	private By selectDOBMonth = getLocator("//date-dropdown[@id='receiver_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);
	private By selectDOBDay = getLocator("//date-dropdown[@id='receiver_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
	
	//Buttons
	private By nextBtn = getLocator("//button[@type='submit']",BY_TYPE.BY_XPATH);	
	private By closeBtn = getLocator("//*[@class='btn clear']", BY_TYPE.BY_XPATH);
	
	public FindTranPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean referenceNumberTxt_Presence() {
		return isPresent(referenceNumberTxt);
	}
	
	public boolean selectDOBYear_Presence() {
		return isPresent(selectDOBYear);
	}
	
	public boolean selectDOBMonth_Presence() {
		return isPresent(selectDOBMonth);
	}
	
	public boolean selectDOBDay_Presence() {
		return isPresent(selectDOBDay);
	}
	
	public boolean nextBtn_Presence() {
		return isPresent(nextBtn);
	}
	
	public boolean closeBtn_Presence() {
		return isPresent(closeBtn);
	}
	
	public void type_referenceNumberTxt(String value) {
		Assert.assertTrue(this.referenceNumberTxt_Presence(), "Reference Number textbox is absent on the find transaction page.");
		type(referenceNumberTxt, value);		
		takeScreenshot("Enter Reference Number.");
	}
	
	public void select_selectDOBYear(String value){		
		Assert.assertTrue(this.selectDOBYear_Presence(), "Sender Date of Birth: Year, dropdown is absent on the receiver info page.");
		selectDropDownByVisibleText(selectDOBYear, value);
		takeScreenshot("Select receiver Date of Birth: Year.");
	}
	
	public void select_selectDOBMonth(String value){		
		Assert.assertTrue(this.selectDOBMonth_Presence(), "receiver Date of Birth: Month, dropdown is absent on the receiver info page.");
		selectDropDownByVisibleText(selectDOBMonth, value);
		takeScreenshot("Select receiver Date of Birth: Month.");
	}
	
	public void select_selectDOBDay(String value){		
		Assert.assertTrue(this.selectDOBDay_Presence(), "receiver Date of Birth: Day, dropdown is absent on the receiver info page.");
		selectDropDownByVisibleText(selectDOBDay, value);
		takeScreenshot("Select receiver Date of Birth: Day.");
	}
	
	public void click_nextBtn(){		
		Assert.assertTrue(this.nextBtn_Presence(), "'Next' button is absent on the find transaction page.");
		takeScreenshot("Click 'Next' button on the find transaction page.");
		click(nextBtn);			
	}
	
	public void click_closeBtn(){		
		Assert.assertTrue(this.closeBtn_Presence(), "Close button on the toaster message is absent on the send destination-amount page.");
		takeScreenshot("Click on Close button on the toaster message on send destination-amount page.");
		click(closeBtn);			
	}
}
