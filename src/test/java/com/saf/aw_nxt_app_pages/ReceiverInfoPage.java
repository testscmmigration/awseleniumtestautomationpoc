package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class ReceiverInfoPage extends BasePage {
	
	//Text Boxes
	private By receiverLastOrFamilyNameTxt = getLocator("receiver_LastName", BY_TYPE.BY_ID);
	private By receiverSecondLastOrFamilyNameTxt = getLocator("receiver_LastName2", BY_TYPE.BY_ID);
	private By receiverFirsOrGivenNameTxt = getLocator("receiver_FirstName", BY_TYPE.BY_ID);
	private By receiverMiddleNameTxt = getLocator("receiver_MiddleName", BY_TYPE.BY_ID);
	private By messageField1Txt = getLocator("messageField1", BY_TYPE.BY_ID);
	private By messageField2Txt = getLocator("messageField2", BY_TYPE.BY_ID);
	private By testQuestionTxt = getLocator("testQuestion", BY_TYPE.BY_ID);
	private By testAnswerTxt = getLocator("testAnswer", BY_TYPE.BY_ID);
	private By receiverBirthCountryTxt = getLocator("Birth Country", BY_TYPE.BY_ID);
	private By receiverCountryTxt = getLocator("Country", BY_TYPE.BY_ID);
	private By receiverAddressTxt = getLocator("receiver_Address", BY_TYPE.BY_ID);
	private By receiverAddressLine2Txt = getLocator("receiver_Address2", BY_TYPE.BY_ID);
	private By receiverAddressLine3Txt = getLocator("receiver_Address3", BY_TYPE.BY_ID);
	private By receiverCityTxt = getLocator("receiver_City", BY_TYPE.BY_ID);
	private By receiverStateOrProvinceTxt = getLocator("State/Province", BY_TYPE.BY_ID);
	private By receiverPostalCodeTxt = getLocator("receiver_PostalCode", BY_TYPE.BY_ID);		
	private By receiverOtherNameSuffixTxt = getLocator("receiver_NameSuffixOther", BY_TYPE.BY_ID);
	private By receiverOtherOccupationTxt = getLocator("receiver_OccupationOther", BY_TYPE.BY_ID);
	
	//Drop Downs
	private By selectReceiverTitle = getLocator("receiver_Title", BY_TYPE.BY_ID);	
	private By selectReceiverSuffix = getLocator("receiver_NameSuffix", BY_TYPE.BY_ID);	
	private By selectReceiverOccupation = getLocator("receiver_Occupation", BY_TYPE.BY_ID);	
		
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
	//Buttons
	private By payoutBtn = getLocator("//div[2]/button", BY_TYPE.BY_XPATH);
	private By finishedBtn = getLocator("//div[2]/button", BY_TYPE.BY_XPATH);
	
	
	public ReceiverInfoPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectReceiverTitle_Presence() {
		return isPresent(selectReceiverTitle);
	}
	
	public boolean receiverLastOrFamilyNameTxt_Presence() {
		return isPresent(receiverLastOrFamilyNameTxt);
	}
	
	public boolean receiverSecondLastOrFamilyNameTxt_Presence() {
		return isPresent(receiverSecondLastOrFamilyNameTxt);
	}
	
	public boolean receiverFirsOrGivenNameTxt_Presence() {
		return isPresent(receiverFirsOrGivenNameTxt);
	}
	
	public boolean receiverMiddleNameTxt_Presence() {
		return isPresent(receiverMiddleNameTxt);
	}
	
	public boolean selectReceiverSuffix_Presence() {
		return isPresent(selectReceiverSuffix);
	}
	
	public boolean receiverOtherNameSuffixTxt_Presence() {
		return isPresent(receiverOtherNameSuffixTxt);
	}	
	
	public boolean messageField1Txt_Presence() {
		return isPresent(messageField1Txt);
	}
	
	public boolean messageField2Txt_Presence() {
		return isPresent(messageField2Txt);
	}
	
	public boolean testQuestionTxt_Presence() {
		return isPresent(testQuestionTxt);
	}
	
	public boolean testAnswerTxt_Presence() {
		return isPresent(testAnswerTxt);
	}	
	
	public boolean selectReceiverOccupation_Presence() {
		return isPresent(selectReceiverOccupation);
	}
	
	public boolean receiverBirthCountryTxt_Presence() {
		return isPresent(receiverBirthCountryTxt);
	}
	
	public boolean receiverOtherOccupationTxt_Presence() {
		return isPresent(receiverOtherOccupationTxt);
	}
	
	public boolean receiverCountryTxt_Presence() {
		return isPresent(receiverCountryTxt);
	}
	
	public boolean receiverAddressTxt_Presence() {
		return isPresent(receiverAddressTxt);
	}
	
	public boolean receiverAddressLine2Txt_Presence() {
		return isPresent(receiverAddressLine2Txt);
	}
	
	public boolean receiverAddressLine3Txt_Presence() {
		return isPresent(receiverAddressLine3Txt);
	}
	
	public boolean receiverCityTxt_Presence() {
		return isPresent(receiverCityTxt);
	}
	
	public boolean receiverStateOrProvinceTxt_Presence() {
		return isPresent(receiverStateOrProvinceTxt);
	}
	
	public boolean receiverPostalCodeTxt_Presence() {
		return isPresent(receiverPostalCodeTxt);
	}	
	
	public boolean payoutBtn_Presence() {
		return isPresent(payoutBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public void select_selectReceiverTitle() {
		Assert.assertTrue(this.selectReceiverTitle_Presence(), "Title dropdown is absent on the receiver info page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(0, 23));		
		selectDropDownByIndex(selectReceiverTitle, index);
		takeScreenshot("Select Receiver Title.");		
	}
	
	public String get_selectedReceiverTitle() {		
		return getSelectedDropDownValue(selectReceiverTitle);
	}
	
	public void type_receiverFirsOrGivenNameTxt(String value) {
		Assert.assertTrue(this.receiverFirsOrGivenNameTxt_Presence(), "First/Given Name textbox is absent on the receiver info page.");
		type(receiverFirsOrGivenNameTxt, value);	
		takeScreenshot("Enter Receiver First/Given Name.");		
	}
	
	public void type_receiverMiddleNameTxt(String value) {
		Assert.assertTrue(this.receiverMiddleNameTxt_Presence(), "Middle Name textbox is absent on the receiver info page.");
		type(receiverMiddleNameTxt, value);	
		takeScreenshot("Enter Receiver Middle Name.");
	}
	
	public void type_receiverLastOrFamilyNameTxt(String value) {
		Assert.assertTrue(this.receiverLastOrFamilyNameTxt_Presence(), "Last/Family Name textbox is absent on the receiver info page.");
		type(receiverLastOrFamilyNameTxt, value);	
		takeScreenshot("Enter Receiver Last/Family Name.");
	}
	
	public void type_receiverSecondLastOrFamilyNameTxt(String value) {
		Assert.assertTrue(this.receiverSecondLastOrFamilyNameTxt_Presence(), "Second Last/Family Name textbox is absent on the receiver info page.");
		type(receiverSecondLastOrFamilyNameTxt, value);	
		takeScreenshot("Enter Receiver Second Last/Family Name.");
	}	
	
	public void select_selectReceiverSuffix() {
		Assert.assertTrue(this.selectReceiverSuffix_Presence(), "Suffix dropdown is absent on the receiver info page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(0, 9));
		if(index == 9) {
			selectDropDownByIndex(selectReceiverSuffix, index);
			type_receiverOtherNameSuffixTxt("OMG");
			takeScreenshot("Select Receiver Suffix.");
		}else {
			selectDropDownByIndex(selectReceiverSuffix, index);
			takeScreenshot("Select Receiver Suffix.");
		}
	}
	
	public void select_selectReceiverSuffix(String value) {
		Assert.assertTrue(this.selectReceiverSuffix_Presence(), "Suffix dropdown is absent on the receiver info page.");
		selectDropDownByVisibleText(selectReceiverSuffix, value);
		takeScreenshot("Select Receiver Suffix.");
	}
	
	public String get_selectedReceiverSuffix() {
		return getSelectedDropDownValue(selectReceiverSuffix);
	}
	
	public void type_receiverOtherNameSuffixTxt(String value) {
		Assert.assertTrue(this.receiverOtherNameSuffixTxt_Presence(), "Other Name Suffix textbox is absent on the receiver info page.");
		type(receiverOtherNameSuffixTxt, value);	
		takeScreenshot("Enter Receiver Other Name Suffix.");
	}	
	
	public void type_messageField1Txt(String value) {
		Assert.assertTrue(this.messageField1Txt_Presence(), "Message Field 1 textbox is absent on the receiver info page.");
		type(messageField1Txt, value);	
		takeScreenshot("Enter Message Field 1.");
	}
	
	public void type_messageField2Txt(String value) {
		Assert.assertTrue(this.messageField2Txt_Presence(), "Message Field 2 textbox is absent on the receiver info page.");
		type(messageField2Txt, value);	
		takeScreenshot("Enter Message Field 2.");
	}
	
	public void type_testQuestionTxt(String value) {
		Assert.assertTrue(this.testQuestionTxt_Presence(), "Test Question textbox is absent on the receiver info page.");
		type(testQuestionTxt, value);	
		takeScreenshot("Enter Test Question.");
	}
	
	public void type_testAnswerTxt(String value) {
		Assert.assertTrue(this.testAnswerTxt_Presence(), "Test Answer textbox is absent on the receiver info page.");
		type(testAnswerTxt, value);	
		takeScreenshot("Enter Test Answer.");
	}
	
	public void type_receiverBirthCountryTxt(String value) {
		Assert.assertTrue(this.receiverBirthCountryTxt_Presence(), "Birth Country textbox is absent on the receiver info page.");
		type(receiverBirthCountryTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter Receiver Birth Country.");
	}
	
	public void type_receiverOtherOccupationTxt(String value) {
		Assert.assertTrue(this.receiverOtherOccupationTxt_Presence(), "Other occupation textbox is absent on the receiver info page.");
		type(receiverOtherOccupationTxt, value);	
		takeScreenshot("Enter Receiver Other Occupation.");
	}
	
	public void type_receiverCountryTxt(String value) {
		Assert.assertTrue(this.receiverCountryTxt_Presence(), "Country textbox is absent on the receiver info page.");
		scrollToElementUsingJavascriptExecutor(receiverCountryTxt);		
		type(receiverCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Receiver Country.");
	}
	
	public void type_receiverAddressTxt(String value) {
		Assert.assertTrue(this.receiverAddressTxt_Presence(), "Address textbox is absent on the receiver info page.");
		type(receiverAddressTxt, value);	
		takeScreenshot("Enter Receiver Address.");
	}
	
	public void type_receiverAddressLine2Txt(String value) {
		Assert.assertTrue(this.receiverAddressLine2Txt_Presence(), "Address Line 2 textbox is absent on the receiver info page.");
		type(receiverAddressLine2Txt, value);	
		takeScreenshot("Enter Receiver Address Line 2.");
	}
	
	public void type_receiverAddressLine3Txt(String value) {
		Assert.assertTrue(this.receiverAddressLine3Txt_Presence(), "Address Line 3 textbox is absent on the receiver info page.");
		type(receiverAddressLine3Txt, value);	
		takeScreenshot("Enter Receiver Address Line 3.");
	}
	
	public void type_receiverCityTxt(String value) {
		Assert.assertTrue(this.receiverCityTxt_Presence(), "City textbox is absent on the receiver info page.");
		type(receiverCityTxt, value);	
		takeScreenshot("Enter Receiver City.");
	}
	
	public void type_receiverStateOrProvinceTxt(String value) {
		Assert.assertTrue(this.receiverStateOrProvinceTxt_Presence(), "State/Province textbox is absent on the receiver info page.");
		type(receiverStateOrProvinceTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Receiver State/Province.");
	}
	
	public void type_receiverPostalCodeTxt(String value) {
		Assert.assertTrue(this.receiverPostalCodeTxt_Presence(), "Postal Code textbox is absent on the receiver info page.");
		type(receiverPostalCodeTxt, value);	
		takeScreenshot("Enter Receiver Postal Code.");
	}		
	
	public void select_selectReceiverOccupation(){		
		Assert.assertTrue(this.selectReceiverOccupation_Presence(), "Occupation, dropdown is absent on the receiver info page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 24));
		if(index == 24) {
			selectDropDownByIndex(selectReceiverOccupation, index);
			type_receiverOtherOccupationTxt("GOD");
			takeScreenshot("Select receiver Occupation.");
		}else {
			selectDropDownByIndex(selectReceiverOccupation, index);
			takeScreenshot("Select receiver Occupation.");
		}
	}
	
	public String get_selectedReceiverOccupation(){	
		return getSelectedDropDownValue(selectReceiverOccupation);
	}
	
	public void click_payoutBtn(){		
		Assert.assertTrue(this.payoutBtn_Presence(), "Payout button is absent on the receiver info page.");	
		takeScreenshot("Click on Payout button on the receiver info page.");
		click(payoutBtn);		
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "Finished button is absent on the receiver info page.");	
		takeScreenshot("Click on Finished button on the receiver info page.");
		click(finishedBtn);		
	}
}
