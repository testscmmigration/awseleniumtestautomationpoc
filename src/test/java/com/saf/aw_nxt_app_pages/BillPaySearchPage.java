package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class BillPaySearchPage extends BasePage {
	
	//Links
	private By newCustomerLnk = getLocator("//div/a/span", BY_TYPE.BY_XPATH);
	
	//Text boxes
	private By phoneNumberTxt = getLocator("STRKEY_PHONE", BY_TYPE.BY_ID);
	private By plusNumberTxt = getLocator("STRKEY_PLUS_NUMBER", BY_TYPE.BY_ID);
	private By accountNumberTxt = getLocator("STRKEY_ACCOUNT_NUMBER", BY_TYPE.BY_ID);
	private By billerNameCodeOrKeywordTxt = getLocator("STRKEY_BILLER_NAME_CODE_KEYWORD", BY_TYPE.BY_ID);
	

	public BillPaySearchPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean newCustomerLnk_Presence() {
		return isPresent(newCustomerLnk);
	}
	
	public boolean phoneNumberTxt_Presence() {
		return isPresent(phoneNumberTxt);
	}
	
	public boolean plusNumberTxt_Presence() {
		return isPresent(plusNumberTxt);
	}
	
	public boolean accountNumberTxt_Presence() {
		return isPresent(accountNumberTxt);
	}
	
	public boolean billerNameCodeOrKeywordTxt_Presence() {
		return isPresent(billerNameCodeOrKeywordTxt);
	}
	
	public void click_newCustomerLink() {
		Assert.assertTrue(this.newCustomerLnk_Presence(), "'New customer' link is absent on the bill-pay customer-search page.");
		click(newCustomerLnk);		
	}
	
	public void type_phoneNumberTxt(String value) {
		Assert.assertTrue(this.phoneNumberTxt_Presence(), "Phone Number textbox is absent on the bill-pay search page.");
		type(phoneNumberTxt, value);
		takeScreenshot("Enter Phone Number.");
	}
	
	public void type_plusNumberTxt(String value) {
		Assert.assertTrue(this.plusNumberTxt_Presence(), "Plus Number textbox is absent on the bill-pay search page.");
		type(plusNumberTxt, value);
		takeScreenshot("Enter Plus Number.");
	}
	
	public void type_accountNumberTxt(String value) {
		Assert.assertTrue(this.accountNumberTxt_Presence(), "Account Number textbox is absent on the bill-pay search page.");
		type(accountNumberTxt, value);
		takeScreenshot("Enter Account Number.");
	}
	
	public void type_billerNameCodeOrKeywordTxt(String value) {
		Assert.assertTrue(this.billerNameCodeOrKeywordTxt_Presence(), "Biller name, code, or keyword textbox is absent on the bill-pay search page.");
		type(billerNameCodeOrKeywordTxt, value);
		takeScreenshot("Enter Biller name, code, or keyword .");
	}
}
