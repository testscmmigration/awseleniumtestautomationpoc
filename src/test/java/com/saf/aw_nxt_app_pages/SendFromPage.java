package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class SendFromPage extends BasePage{
	
	//Links	
	private By newCustomerLnk = getLocator("//div/a/span", BY_TYPE.BY_XPATH);
	
	//Text Boxes
	private By senderLastOrFamilyNameTxt = getLocator("consumer_LastName", BY_TYPE.BY_ID);
	private By senderPhoneNumberTxt = getLocator("consumer_PrimaryPhone", BY_TYPE.BY_ID);
	private By senderLastOrFamilyName2Txt = getLocator("//div[3]/div[2]/div/agg-field/div/div/div/div/agg-text-field/div/input", BY_TYPE.BY_XPATH);
	private By senderPlusNumberTxt = getLocator("mgiRewardsNumber", BY_TYPE.BY_ID);
	
	//Drop Downs
	private By selectDOBYear = getLocator("//date-dropdown[@id='consumer_DOB']/div/div[1]/select", BY_TYPE.BY_XPATH);
	private By selectDOBMonth = getLocator("//date-dropdown[@id='consumer_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);
	private By selectDOBDay = getLocator("//date-dropdown[@id='consumer_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
		
	//Button	
	private By nextBtn = getLocator("findCustomerBtn", BY_TYPE.BY_ID);
	
	//Sender search results
	private By prvSenderSearchResultList = getLocator("//*[@class='table-borders cursor-pointer padding-5 margin-bottom-10 highlight-border']", BY_TYPE.BY_XPATH);
	
	public SendFromPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean newCustomerLink_Presence() {
		return isPresent(newCustomerLnk);
	}
	
	public boolean senderLastOrFamilyNameTxt_Presence() {
		return isPresent(senderLastOrFamilyNameTxt);
	}
	
	public boolean selectDOBYear_Presence() {
		return isPresent(selectDOBYear);
	}
	
	public boolean selectDOBMonth_Presence() {
		return isPresent(selectDOBMonth);
	}
	
	public boolean selectDOBDay_Presence() {
		return isPresent(selectDOBDay);
	}
	
	public boolean senderPhoneNumberTxt_Presence() {
		return isPresent(senderPhoneNumberTxt);
	}
	
	public boolean senderLastOrFamilyName2Txt_Presence() {
		return isPresent(senderLastOrFamilyName2Txt);
	}
	
	public boolean senderPlusNumberTxt_Presence() {
		return isPresent(senderPlusNumberTxt);
	}
	
	public boolean nextBtn_Presence() {
		return isPresent(nextBtn);
	}
	
	public boolean prvSenderSearchResultList_Presence() {
		return isPresent(prvSenderSearchResultList);
	}
	
	public void click_newCustomerLink() {
		scrollToElementUsingJavascriptExecutor(newCustomerLnk);
		Assert.assertTrue(this.newCustomerLink_Presence(), "New customer link is absent on the send-from page.");
		click(newCustomerLnk);		
	}
	
	public void type_senderLastOrFamilyNameTxt(String value) {
		Assert.assertTrue(this.senderLastOrFamilyNameTxt_Presence(), "Sender Last/family name textbox is absent on the send-from page.");
		type(senderLastOrFamilyNameTxt,value);
		takeScreenshot("Enter Sender Last/family name.");		
	}
	
	public void type_senderPhoneNumberTxt(String value) {
		Assert.assertTrue(this.senderPhoneNumberTxt_Presence(), "Sender Phone number textbox is absent on the send-from page.");
		type(senderPhoneNumberTxt,value);
		takeScreenshot("Enter Sender Phone number .");		
	}
	
	public void type_senderLastOrFamilyName2Txt(String value) {
		Assert.assertTrue(this.senderLastOrFamilyName2Txt_Presence(), "Sender Last/family name textbox is absent on the send-from page.");
		type(senderLastOrFamilyName2Txt,value);
		takeScreenshot("Enter Sender Last/family name.");		
	}
	
	public void type_senderPlusNumberTxt(String value) {
		Assert.assertTrue(this.senderPlusNumberTxt_Presence(), "Sender Plus number textbox is absent on the send-from page.");
		type(senderPlusNumberTxt,value);
		takeScreenshot("Enter Sender Plus number.");		
	}
	
	public void select_selectDOBYear(String value) {		
		Assert.assertTrue(this.selectDOBYear_Presence(), "Sender Date of Birth: Year, dropdown is absent on the send-from page.");
		selectDropDownByVisibleText(selectDOBYear, value);
		takeScreenshot("Select sender Date of Birth: Year.");
	}
	
	public void select_selectDOBMonth(String value) {		
		Assert.assertTrue(this.selectDOBMonth_Presence(), "Sender Date of Birth: Month, dropdown is absent on the send-from page.");
		selectDropDownByVisibleText(selectDOBMonth, value);
		takeScreenshot("Select sender Date of Birth: Month.");
	}
	
	public void select_selectDOBDay(String value) {		
		Assert.assertTrue(this.selectDOBDay_Presence(), "Sender Date of Birth: Day, dropdown is absent on the send-from page.");
		selectDropDownByVisibleText(selectDOBDay, value);
		takeScreenshot("Select sender Date of Birth: Day.");
	}
	
	public void click_nextBtn() {
		Assert.assertTrue(this.nextBtn_Presence(), "Next button is absent on the send-from page.");
		click(nextBtn);		
	}
	
	public void choose_prvSenderSearchResultList(String tranType, String value) {
		Assert.assertTrue(this.prvSenderSearchResultList_Presence(), "Sender search result for the provided search criteria is absent");		
		List<WebElement> myElements = driver.findElements(prvSenderSearchResultList);
		WebElement myElement = null;
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				if(tranType.equalsIgnoreCase("send")) {
					myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/send-send-from/content-body/div/shared-scope-transclude/div[3]/div[2]/div["+i+"]/div/div/div/strong"));
				}else if(tranType.equalsIgnoreCase("efee")) {
					myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/efee-send-from/content-body/div/shared-scope-transclude/div[3]/div[2]/div["+i+"]/div/div/div/strong"));
				}
				String senderName = myElement.getText();
				if(value.equalsIgnoreCase(senderName)) {					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Select the sender from the returned search results."+value);
					myElement.click();
					break;
				}
			}
		}						
	}
}
