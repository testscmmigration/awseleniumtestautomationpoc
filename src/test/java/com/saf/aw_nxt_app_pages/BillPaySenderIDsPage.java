package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class BillPaySenderIDsPage extends BasePage {
	
	//Drop downs
	private By selectPhotoIDType  = getLocator("sender_PersonalId1_Type", BY_TYPE.BY_ID);
	private By selectSecondaryIDType  = getLocator("sender_PersonalId2_Type", BY_TYPE.BY_ID);
	private By selectDOBYear = getLocator("//date-dropdown[@id='sender_DOB']/div/div/select", BY_TYPE.BY_XPATH);
	private By selectDOBMonth = getLocator("//date-dropdown[@id='sender_DOB']/div/div[2]/select", BY_TYPE.BY_XPATH);
	private By selectDOBDay = getLocator("//date-dropdown[@id='sender_DOB']/div/div[3]/select", BY_TYPE.BY_XPATH);
	private By selectOccupation = getLocator("sender_Occupation", BY_TYPE.BY_ID);
	
	//Text Boxes		
	private By photoIDNumberTxt  = getLocator("sender_PersonalId1_Number", BY_TYPE.BY_ID);
	private By PhotoIDIssueCountryTxt  = getLocator("ID Issue Country", BY_TYPE.BY_ID);
	private By PhotoIDIssueStateOrProvinceTxt  = getLocator("ID Issue State/Province", BY_TYPE.BY_ID);
	private By secondFormOfIDNumberTxt  = getLocator("sender_PersonalId2_Number", BY_TYPE.BY_ID);
	private By otherOccupationTxt = getLocator("sender_OccupationOther", BY_TYPE.BY_ID);
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
		
	public BillPaySenderIDsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectPhotoIDType_Presence() {
		return isPresent(selectPhotoIDType);
	}
	
	public boolean photoIDNumberTxt_Presence() {
		return isPresent(photoIDNumberTxt);
	}
	
	public boolean PhotoIDIssueCountryTxt_Presence() {
		return isPresent(PhotoIDIssueCountryTxt);
	}
	
	public boolean PhotoIDIssueStateOrProvinceTxt_Presence() {
		return isPresent(PhotoIDIssueStateOrProvinceTxt);
	}
	
	public boolean selectSecondaryIDType_Presence() {
		return isPresent(selectSecondaryIDType);
	}
	
	public boolean secondFormOfIDNumberTxt_Presence() {
		return isPresent(secondFormOfIDNumberTxt);
	}
	
	public boolean selectDOBYear_Presence() {
		return isPresent(selectDOBYear);
	}
	
	public boolean selectDOBMonth_Presence() {
		return isPresent(selectDOBMonth);
	}
	
	public boolean selectDOBDay_Presence() {
		return isPresent(selectDOBDay);
	}
	
	public boolean selectOccupation_Presence() {
		return isPresent(selectOccupation);
	}
	
	public boolean otherOccupationTxt_Presence() {
		return isPresent(otherOccupationTxt);
	}
	
	public void select_selectPhotoIDType(String value){		
		Assert.assertTrue(this.selectPhotoIDType_Presence(), "Photo ID: ID Type, dropdown is absent on the bill pay sender IDs page.");
		selectDropDownByVisibleText(selectPhotoIDType, value);
		takeScreenshot("Select Photo ID: ID Type.");
	}
	
	public void type_photoIDNumberTxt(String value) {
		Assert.assertTrue(this.photoIDNumberTxt_Presence(), "Photo ID: ID Number, textbox is absent on the bill pay sender IDs page.");
		type(photoIDNumberTxt, value);		
		takeScreenshot("Enter Photo ID: ID Number.");
	}
	
	public void type_photoIDIssueCountryTxt(String value) {
		Assert.assertTrue(this.PhotoIDIssueCountryTxt_Presence(), "Photo ID: ID Issue Country, textbox is absent on the bill pay sender IDs page.");
		type(PhotoIDIssueCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue Country.");
	}
	
	public void type_photoIDIssueStateOrProvinceTxt(String value) {
		Assert.assertTrue(this.PhotoIDIssueStateOrProvinceTxt_Presence(), "Photo ID: ID Issue State/Province, textbox is absent on the bill pay sender IDs page.");
		type(PhotoIDIssueStateOrProvinceTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue State/Province.");
	}
	
	public void select_selectSecondaryIDType(String value){		
		Assert.assertTrue(this.selectSecondaryIDType_Presence(), "Second form of ID: Secondary ID Type, dropdown is absent on the bill pay sender IDs page.");
		selectDropDownByVisibleText(selectSecondaryIDType, value);
		takeScreenshot("Select Second form of ID: Secondary ID Type.");
	}
	
	public void type_secondFormOfIDNumberTxt(String value) {
		Assert.assertTrue(this.secondFormOfIDNumberTxt_Presence(), "Second form of ID: Secondary ID Number, textbox is absent on the bill pay sender IDs page.");
		type(secondFormOfIDNumberTxt, value);		
		takeScreenshot("Enter Second form of ID: Secondary ID Number.");
	
	}
	
	public void select_selectDOBYear(String value){		
		Assert.assertTrue(this.selectDOBYear_Presence(), "Date of Birth: Year, dropdown is absent on the bill pay sender IDs page.");
		selectDropDownByVisibleText(selectDOBYear, value);
		takeScreenshot("Select Date of Birth: Year.");
	}
	
	public void select_selectDOBMonth(String value){		
		Assert.assertTrue(this.selectDOBMonth_Presence(), "Date of Birth: Month, dropdown is absent on the bill pay sender IDs page.");
		selectDropDownByVisibleText(selectDOBMonth, value);
		takeScreenshot("Select Date of Birth: Month.");
	}
	
	public void select_selectDOBDay(String value){		
		Assert.assertTrue(this.selectDOBDay_Presence(), "Date of Birth: Day, dropdown is absent on the bill pay sender IDs page.");
		selectDropDownByVisibleText(selectDOBDay, value);
		takeScreenshot("Select Date of Birth: Day.");
	}
	
	public void select_selectOccupation(){		
		Assert.assertTrue(this.selectOccupation_Presence(), "Ocuupation, dropdown is absent on the bill pay sender IDs page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 24));
		if(index == 24) {
			selectDropDownByIndex(selectOccupation, index);
			takeScreenshot("Select occupation.");
			type_otherOccupationTxt("GOD");			
		}else {
			selectDropDownByIndex(selectOccupation, index);
			takeScreenshot("Select sender occupation.");
		}
	}
	
	public String get_selectedOccupation(){
		return getSelectedDropDownValue(selectOccupation);
	}
	
	public void type_otherOccupationTxt(String value) {
		Assert.assertTrue(this.otherOccupationTxt_Presence(), "Other Occupation, textbox is absent on the bill pay sender IDs page.");
		type(otherOccupationTxt, value);		
		takeScreenshot("Enter sender other occupation.");
	}
}
