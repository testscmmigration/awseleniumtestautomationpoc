package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class SendReversalTransactionDetailsPage extends BasePage{

	//Drop downs
	private By selectReasonForRefundTransaction  = getLocator("send_ReversalReason", BY_TYPE.BY_ID);
	
	//Buttons
	private By yesContinueBtn =  getLocator("button.btn.btn-lg.mg-btn.mg-btn-primary.width-100pc",BY_TYPE.BY_CSSSELECTOR);
	private By finishedBtn = getLocator("button.btn.btn-lg.btn-block.mg-btn.mg-btn-primary",BY_TYPE.BY_CSSSELECTOR);	
	
	//Radio Buttons
	private By refundYesRad = getLocator("//div[2]/div/label/span",BY_TYPE.BY_XPATH);
	private By refundNoRad = getLocator("//div[2]/label/span",BY_TYPE.BY_XPATH);
	
	public SendReversalTransactionDetailsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectReasonForRefundTransaction_Presence() {
		return isPresent(selectReasonForRefundTransaction);
	}
	
	public boolean refundYesRad_Presence() {
		return isPresent(refundYesRad);
	}
	
	public boolean refundNoRad_Presence() {
		return isPresent(refundNoRad);
	}
	
	public boolean yesContinueBtn_Presence() {
		return isPresent(yesContinueBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public void select_selectReasonForRefundTransaction(){		
		Assert.assertTrue(this.selectReasonForRefundTransaction_Presence(), "'Reason for refund transaction' dropdown is absent on the dashboard page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(1, 15)); 
		selectDropDownByIndex(selectReasonForRefundTransaction, index);
		takeScreenshot("Reason for refund transaction.");		
	}
	
	public String get_selectedReasonForRefundTransaction() {		
		return getSelectedDropDownValue(selectReasonForRefundTransaction);
	}
	
	public void click_refundYesRad(){		
		Assert.assertTrue(this.refundYesRad_Presence(), "'Yes, refund amount' radio button is absent on the send reversal transaction details page.");
		takeScreenshot("Click on Refund fee? Yes, refund amount radio button on the send reversal transaction details page.");
		click(refundYesRad);			
	}
	
	public void click_refundNoRad(){		
		Assert.assertTrue(this.refundNoRad_Presence(), "'No' radio button is absent on the send reversal transaction details page.");
		takeScreenshot("Click on Refund fee? 'No' radio button on the send reversal transaction details page.");
		click(refundNoRad);			
	}
	
	public void click_yesContinueBtn(){		
		Assert.assertTrue(this.yesContinueBtn_Presence(), "'Yes, continue' button is absent on the send reversal transaction details page.");
		takeScreenshot("Click 'Yes, continue' button on the send reversal transaction details page.");
		click(yesContinueBtn);			
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "'Finished' button is absent on the send reversal transaction details page.");
		takeScreenshot("Click 'Finished' button on the send reversal transaction details page.");
		click(finishedBtn);			
	}
}
