package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class ReceiveProfileFoundPage extends BasePage{
	
	//Receiver search results
	private By prvReceiverSearchResultList = getLocator("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/receive-profiles-found/content-body/div/shared-scope-transclude/div/div[2]/div", BY_TYPE.BY_XPATH);

	public ReceiveProfileFoundPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean prvReceiverSearchResultList_Presence() {
		return isPresent(prvReceiverSearchResultList);
	}
	
	public void choose_prvReceiverSearchResultList(String value) {
		Assert.assertTrue(this.prvReceiverSearchResultList_Presence(), "Receiver search result for the provided search criteria is absent");		
		List<WebElement> myElements = driver.findElements(prvReceiverSearchResultList);
		WebElement myElement = null;
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {				
				myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/receive-profiles-found/content-body/div/shared-scope-transclude/div/div[2]/div["+i+"]/div/div/div/strong"));
			
				String senderName = myElement.getText();
				if(value.equalsIgnoreCase(senderName)) {					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Select the receiver from the returned search results."+value);
					myElement.click();
					break;
				}
			}
		}						
	}
}


