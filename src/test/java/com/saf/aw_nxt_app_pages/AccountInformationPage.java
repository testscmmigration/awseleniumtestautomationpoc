package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class AccountInformationPage extends BasePage {
	
	//Text boxes	
	private By payPalID_EmailorMobileTxt = getLocator("accountNumber", BY_TYPE.BY_ID);

	public AccountInformationPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean payPalID_EmailorMobileTxt_Presence() {
		return isPresent(payPalID_EmailorMobileTxt);
	}
	
	public void type_payPalID_EmailorMobileTxt(String value) throws InterruptedException {		
		Assert.assertTrue(this.payPalID_EmailorMobileTxt_Presence(), "PayPal ID (Email or Mobile) textbox is absent on the acct-info page.");
		type(payPalID_EmailorMobileTxt, value);		
		takeScreenshot("Enter PayPal ID (Email or Mobile).");
	}
}
