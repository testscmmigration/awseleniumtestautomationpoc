package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class StagedTranListPage extends BasePage {
	
	//Staged transaction list
	private By stagedTransactionList = getLocator("//*[@class='table-borders cursor-pointer padding-5 margin-bottom-10 highlight-border mg-button-shadow']", BY_TYPE.BY_XPATH);	

	public StagedTranListPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean stagedTransactionList_Presence() {
		return isPresent(stagedTransactionList);
	}	
		
	public void select_stagedTransaction(String value) {
		Assert.assertTrue(this.stagedTransactionList_Presence(), "Staged transaction list is absent on the staged-tran tran-list page.");		
		List<WebElement> myElements = driver.findElements(stagedTransactionList);
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				WebElement myElement = driver.findElement(By.xpath("//div[@id='ui-view-container']/div/root/div/div/common-container/div/div/div/div/div/staged-tran-list/content-body/div/shared-scope-transclude/div/div[2]/div[2]/div/div["+i+"]/div/div[2]/div/div/strong"));
				String senderName = myElement.getText();				
				if(value.equalsIgnoreCase(senderName)) {					
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Click on the staged transaction with the sender name: "+value);
					myElement.click();
					break;
				}
			}
		}
	}
}
