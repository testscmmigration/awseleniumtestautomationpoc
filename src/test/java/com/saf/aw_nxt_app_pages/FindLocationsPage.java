package com.saf.aw_nxt_app_pages;

import java.util.Set;

import org.openqa.selenium.WebDriver;

import com.saf.base.BasePage;

public class FindLocationsPage extends BasePage {

	public FindLocationsPage(WebDriver driver) {
		super(driver);		
	}
	
	public Set<String> getWindowHandles() {
		return driver.getWindowHandles();
	}
}
