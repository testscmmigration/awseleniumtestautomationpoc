package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class HomePage extends BasePage {	

	//Buttons
	private By APProprietaryandConfidentialCloseBtn = getLocator("//button[@type='submit']", BY_TYPE.BY_XPATH);	

	public HomePage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean APProprietaryandConfidentialCloseBtn_Presence() {
		return isPresent(APProprietaryandConfidentialCloseBtn);
	}
	
	public void click_APProprietaryandConfidentialCloseBtn(){		
		Assert.assertTrue(this.APProprietaryandConfidentialCloseBtn_Presence(), "'Close' button is absent on home page.");
		takeScreenshot("Click on 'Close' button on the home page.");
		click(APProprietaryandConfidentialCloseBtn);			
	}
}
