package com.saf.aw_nxt_app_pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.saf.base.BasePage;

public class SenderContactInfoPage extends BasePage {
	
	//Labels
	private By moneygramPlusNumberLbl = getLocator("//div[11]/div/agg-field/div/div/label/span", BY_TYPE.BY_XPATH);
	
	//Text boxes
	private By senderPrimaryPhoneCountryCodeTxt = getLocator("Primary Phone Country Code", BY_TYPE.BY_ID);
	private By senderPrimaryPhoneTxt = getLocator("sender_PrimaryPhone", BY_TYPE.BY_ID);
	private By senderEmailTxt = getLocator("sender_Email", BY_TYPE.BY_ID);
		
	//Check boxes
	private By senderMobilePhoneChk = getLocator("sender_PrimaryPhoneSMSEnabled_", BY_TYPE.BY_ID);
	private By senderEnrollInPlusProgramChk = getLocator("sender_rewards_EnrollmentFlag_", BY_TYPE.BY_ID);
	
	//Drop downs
	private By selectSenderReceiveTransactionStatus = getLocator("sender_Transaction_NotificationOptedCode", BY_TYPE.BY_ID);
	private By selectSenderReceiveOffers = getLocator("sender_Marketing_NotificationOptedCode", BY_TYPE.BY_ID);
	private By selectSenderPreferredLanguage = getLocator("sender_PreferredLanguage", BY_TYPE.BY_ID);	
	
	//Country list
	private By selectCountryList = getLocator("uib-typeahead-match", BY_TYPE.BY_CLASSNAME);
		
	public SenderContactInfoPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean moneygramPlusNumberLbl_Presence() {
		return isPresent(moneygramPlusNumberLbl);
	}
	
	public boolean senderPrimaryPhoneCountryCodeTxt_Presence() {
		return isPresent(senderPrimaryPhoneCountryCodeTxt);
	}
	
	public boolean senderPrimaryPhoneTxt_Presence() {
		return isPresent(senderPrimaryPhoneTxt);
	}
	
	public boolean senderMobilePhoneChk_Presence() {
		return isPresent(senderMobilePhoneChk);
	}
	
	public boolean senderEmailTxt_Presence() {
		return isPresent(senderEmailTxt);
	}
	
	public boolean selectSenderReceiveTransactionStatus_Presence() {
		return isPresent(selectSenderReceiveTransactionStatus);
	}
	
	public boolean selectSenderReceiveOffers_Presence() {
		return isPresent(selectSenderReceiveOffers);
	}
	
	public boolean selectSenderPreferredLanguage_Presence() {
		return isPresent(selectSenderPreferredLanguage);
	}
	
	public boolean senderEnrollInPlusProgramChk_Presence() {
		return isPresent(senderEnrollInPlusProgramChk);
	}
	
	public boolean selectCountryList_Presence() {
		return isPresent(selectCountryList);
	}
	
	public void type_primaryPhoneCountryCodeTxt(String value)  {		
		Assert.assertTrue(this.senderPrimaryPhoneCountryCodeTxt_Presence(), "Primary phone country code textbox is absent on the sender contact info page.");		
		type(senderPrimaryPhoneCountryCodeTxt, value);
		while(!selectCountryList_Presence()) {
			type(senderPrimaryPhoneCountryCodeTxt, value);
		}
		takeScreenshot("Enter primary phone country code.");
	}
	
	public void selectRequiredCountry(String value) {		
		List<WebElement> myElements = driver.findElements(selectCountryList);
		if(myElements.size()>0) {
			for(int i=1; i<=myElements.size();i++) {
				WebElement myElement = driver.findElement(By.xpath("//li["+i+"]/a/div/div[2]/span[1]"));
				String country = myElement.getText();
				//System.out.println(country);
				if(value.equals(country)) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", myElement);
					takeScreenshot("Click on the country required: "+value);
					myElement.click();
					break;
				}
			}
		}
	}
	
	public void type_senderPrimaryPhoneTxt(String value)  {		
		Assert.assertTrue(this.senderPrimaryPhoneTxt_Presence(), "Sender primary phone textbox is absent on the sender contact info page.");
		type(senderPrimaryPhoneTxt, value);		
		takeScreenshot("Enter sender primary phone.");
	}
	
	public void check_senderMobilePhoneChk()  {		
		Assert.assertTrue(this.senderMobilePhoneChk_Presence(), "Sender mobile Phone checkbox is absent on the sender contact info page.");
		click(senderMobilePhoneChk);	
		takeScreenshot("Check Mobile Phone.");
	}	
	
	public void type_senderEmailTxt(String value)  {		
		Assert.assertTrue(this.senderEmailTxt_Presence(), "Sender email textbox is absent on the sender contact info page.");
		type(senderEmailTxt, value);		
		takeScreenshot("Enter sender email.");
	}
	
	public void select_selectSenderReceiveTransactionStatus(String value){		
		Assert.assertTrue(this.selectSenderReceiveTransactionStatus_Presence(), "'Receive Transaction Status' dropdown is absent on the sender contact info page.");
		selectDropDownByVisibleText(selectSenderReceiveTransactionStatus, value);
		takeScreenshot("Select 'Receive Transaction Status'.");
	}
	
	public void select_selectSenderReceiveOffers(String value){		
		Assert.assertTrue(this.selectSenderReceiveOffers_Presence(), "'Receive Offers' dropdown is absent on the sender contact info page.");
		selectDropDownByVisibleText(selectSenderReceiveOffers, value);
		takeScreenshot("Select 'Receive Offers'.");
	}
	
	public void select_selectSenderPreferredLanguage(String value){		
		Assert.assertTrue(this.selectSenderPreferredLanguage_Presence(), "'Preferred Language' dropdown is absent on the sender contact info page.");
		selectDropDownByVisibleText(selectSenderPreferredLanguage, value);
		takeScreenshot("Select 'Preferred Language'.");
	}
	
	public void check_senderEnrollInPlusProgramChk()  {		
		Assert.assertTrue(this.senderEnrollInPlusProgramChk_Presence(), "Sender Enroll in Plus Program checkbox is absent on the sender contact info page.");
		click(senderEnrollInPlusProgramChk);	
		takeScreenshot("Check Enroll in Plus Program.");
	}	
	
	public void  scrollTo_moneygramPlusNumberLbl() {
		Assert.assertTrue(this.moneygramPlusNumberLbl_Presence(), "'Other:' label is absent on the sender-profile-details page.");		
		scrollToElementUsingJavascriptExecutor(moneygramPlusNumberLbl);
		takeScreenshot("Sender Contact Information: 2");
	}	
}
