package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class AmendTransactionDetailsPage extends BasePage {
	
	//Text boxes
	private By lastOrFamilyNameTxt =  getLocator("receiver_LastName",BY_TYPE.BY_ID);
	private By firstOrGivenNameTxt =  getLocator("receiver_FirstName",BY_TYPE.BY_ID);
	private By secondLastOrFamilyTxt =  getLocator("receiver_LastName2",BY_TYPE.BY_ID);
	private By middleNameTxt =  getLocator("receiver_MiddleName",BY_TYPE.BY_ID);
	
	//Buttons
	private By yesContinueBtn = getLocator("button.btn.btn-lg.mg-btn.mg-btn-primary.width-100pc",BY_TYPE.BY_CSSSELECTOR);											 
	private By finishedBtn = getLocator("ok",BY_TYPE.BY_ID);

	public AmendTransactionDetailsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean lastOrFamilyNameTxt_Presence() {
		return isPresent(lastOrFamilyNameTxt);
	}
	
	public boolean firstOrGivenNameTxt_Presence() {
		return isPresent(firstOrGivenNameTxt);
	}
	
	public boolean secondLastOrFamilyTxt_Presence() {
		return isPresent(secondLastOrFamilyTxt);
	}
	
	public boolean middleNameTxt_Presence() {
		return isPresent(middleNameTxt);
	}
	
	public boolean yesContinueBtn_Presence() {
		return isPresent(yesContinueBtn);
	}
	
	public boolean finishedBtn_Presence() {
		return isPresent(finishedBtn);
	}
	
	public void type_lastOrFamilyNameTxt(String value) {
		Assert.assertTrue(this.lastOrFamilyNameTxt_Presence(), "'Last/family name' textbox is absent on the amend transaction details page.");
		type(lastOrFamilyNameTxt, value);		
		takeScreenshot("Enter Last/family name.");
	}
	
	public void type_firstOrGivenNameTxt(String value) {
		Assert.assertTrue(this.firstOrGivenNameTxt_Presence(), "'First/given name' textbox is absent on the amend transaction details page.");
		type(firstOrGivenNameTxt, value);		
		takeScreenshot("Enter First/given name.");
	}
	
	public void type_secondLastOrFamilyTxt(String value) {
		Assert.assertTrue(this.secondLastOrFamilyTxt_Presence(), "'Second last/family name' textbox is absent on the amend transaction details page.");
		type(secondLastOrFamilyTxt, value);		
		takeScreenshot("Enter Second last/family name.");
	}
	
	public void type_middleNameTxt(String value) {
		Assert.assertTrue(this.middleNameTxt_Presence(), "Middle name textbox is absent on the amend transaction details page.");
		type(middleNameTxt, value);		
		takeScreenshot("Enter Middle name.");
	}
	
	public void click_yesContinueBtn(){		
		Assert.assertTrue(this.yesContinueBtn_Presence(), "'Yes, continue' button is absent on the amend transaction details page.");
		takeScreenshot("Click 'Yes, continue' button on the amend transaction details page.");
		click(yesContinueBtn);			
	}
	
	public void click_finishedBtn(){		
		Assert.assertTrue(this.finishedBtn_Presence(), "'Finished' button is absent on the amend transaction details page.");
		takeScreenshot("Click 'Finished' button on the amend transaction details page.");
		click(finishedBtn);			
	}
}
