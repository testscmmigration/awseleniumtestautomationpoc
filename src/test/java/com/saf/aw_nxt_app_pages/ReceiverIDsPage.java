package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
 
public class ReceiverIDsPage extends BasePage {
	
	//Drop downs	
	private By selectPhotoIDType  = getLocator("receiver_PersonalId1_Type", BY_TYPE.BY_ID);
	private By selectSecondaryIDType  = getLocator("receiver_PersonalId2_Type", BY_TYPE.BY_ID);	
	private By selectPhotoIDChoice  = getLocator("receiver_PersonalId1_Choice", BY_TYPE.BY_ID);
	private By selectSecondFormOfIDChoice  = getLocator("receiver_PersonalId2_Choice", BY_TYPE.BY_ID);
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
	//Text boxes
	private By photoIDNumberTxt= getLocator("receiver_PersonalId1_Number", BY_TYPE.BY_ID);	
	private By photoIDIssueCountryTxt = getLocator("ID Issue Country", BY_TYPE.BY_ID);	
	private By photoIDIssueStateOrProvinceTxt = getLocator("ID Issue State/Province", BY_TYPE.BY_ID);
	private By photoIDLast4digitsofIDTxt = getLocator("receiver_PersonalId1_VerificationStr", BY_TYPE.BY_ID);
	
	
	private By secondaryIDNumberTxt = getLocator("receiver_PersonalId2_Number", BY_TYPE.BY_ID);	
	private By secondFormOfIDLast4digitsofIDTxt  = getLocator("receiver_PersonalId2_VerificationStr", BY_TYPE.BY_ID);		
	
	public ReceiverIDsPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean selectPhotoIDType_Presence() {
		return isPresent(selectPhotoIDType);
	}
	
	public boolean selectSecondaryIDType_Presence() {
		return isPresent(selectSecondaryIDType);
	}
	
	public boolean selectPhotoIDChoice_Presence() {
		return isPresent(selectPhotoIDChoice);
	}
	
	public boolean selectSecondFormOfIDChoice_Presence() {
		return isPresent(selectSecondFormOfIDChoice);
	}	
	
	public boolean photoIDNumberTxt_Presence() {
		return isPresent(photoIDNumberTxt);
	}
	
	public boolean photoIDIssueCountryTxt_Presence() {
		return isPresent(photoIDIssueCountryTxt);
	}
	
	public boolean photoIDIssueStateOrProvinceTxt_Presence() {
		return isPresent(photoIDIssueStateOrProvinceTxt);
	}
	
	public boolean photoIDLast4digitsofIDTxt_Presence() {
		return isPresent(photoIDLast4digitsofIDTxt);
	}
	
	public boolean secondaryIDNumberTxt_Presence() {
		return isPresent(secondaryIDNumberTxt);
	}
	
	public boolean secondFormOfIDLast4digitsofIDTxt_Presence() {
		return isPresent(secondFormOfIDLast4digitsofIDTxt);
	}
	
	public void select_selectPhotoIDType(String value){		
		Assert.assertTrue(this.selectPhotoIDType_Presence(), "Photo ID: ID Type, dropdown is absent on the receiver IDs page.");		
		selectDropDownByVisibleText(selectPhotoIDType , value);
		takeScreenshot("Select Photo ID: ID Type.");		
	}
	
	public void select_selectSecondFormOfIDType(String value){		
		Assert.assertTrue(this.selectSecondaryIDType_Presence(), "Second form of ID: Secondary ID Type, dropdown is absent on the receiver IDs page.");
		selectDropDownByVisibleText(selectSecondaryIDType , value);
		takeScreenshot("Select Photo ID: ID Type.");
	}
	
	public void select_selectPhotoIDChoice(String value){		
		Assert.assertTrue(this.selectPhotoIDChoice_Presence(), "Photo ID: ID Choice, dropdown is absent on the receiver IDs page.");
		selectDropDownByVisibleText(selectPhotoIDChoice, value);
		takeScreenshot("Select Photo ID: ID Choice.");
	}
	
	public void select_selectSecondFormOfIDChoice(String value){		
		Assert.assertTrue(this.selectSecondFormOfIDChoice_Presence(), "Second form of ID: Secondary ID Choice, dropdown is absent on the receiver IDs page.");
		selectDropDownByVisibleText(selectSecondFormOfIDChoice, value);
		takeScreenshot("Select Second form of ID: Secondary ID Choice.");
	}
	
	public void type_photoIDNumberTxt(String value) {
		Assert.assertTrue(this.photoIDNumberTxt_Presence(), "Photo ID: ID Number, textbox is absent on the receiver IDs page.");
		type(photoIDNumberTxt, value);
		takeScreenshot("Enter Photo ID: ID Number.");
	}
	
	public void type_photoIDIssueCountryTxt(String value) {
		Assert.assertTrue(this.photoIDIssueCountryTxt_Presence(), "Photo ID: ID Issue Country, textbox is absent on the receiver IDs page.");
		type(photoIDIssueCountryTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue Country.");
	}
	
	public void type_photoIDIssueStateOrProvinceTxt(String value) {
		Assert.assertTrue(this.photoIDIssueStateOrProvinceTxt_Presence(), "Photo ID: ID Issue State/Province, textbox is absent on the receiver IDs page.");		
		type(photoIDIssueStateOrProvinceTxt, value);
		click(dropDownValue);
		takeScreenshot("Enter Photo ID: ID Issue State/Province.");
	}
	
	public void type_photoIDLast4digitsofIDTxt(String value) {
		Assert.assertTrue(this.photoIDLast4digitsofIDTxt_Presence(), "Photo ID: Last 4 digits of ID, textbox is absent on the receiver IDs page.");
		type(photoIDLast4digitsofIDTxt, value);			
		takeScreenshot("Enter Photo ID: Last 4 digits of ID.");
	}
	
	public void type_secondaryIDNumberTxt(String value) {
		Assert.assertTrue(this.secondaryIDNumberTxt_Presence(), "Second form of ID: Secondary ID Number, textbox is absent on the receiver IDs page.");
		type(secondaryIDNumberTxt, value);
		takeScreenshot("Enter Second form of ID: Secondary ID Number.");
	}
	
	public void type_secondFormOfIDLast4digitsofIDTxt(String value) {
		Assert.assertTrue(this.secondFormOfIDLast4digitsofIDTxt_Presence(), "Second form of ID: Last 4 digits of ID, textbox is absent on the receiver IDs page.");
		type(secondFormOfIDLast4digitsofIDTxt, value);		
		takeScreenshot("Enter Second form of ID: Last 4 digits of ID.");
	}

}
