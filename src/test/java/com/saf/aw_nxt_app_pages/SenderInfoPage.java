package com.saf.aw_nxt_app_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;
import com.saf.base.BaseTest;

public class SenderInfoPage extends BasePage {
	
	//Text boxes	
	private By senderLastOrFamilyNameTxt = getLocator("sender_LastName", BY_TYPE.BY_ID);
	private By senderSecondLastOrFamilyNameTxt = getLocator("sender_LastName2", BY_TYPE.BY_ID);
	private By senderFirstOrGivenNameTxt = getLocator("sender_FirstName", BY_TYPE.BY_ID);
	private By senderMiddleNameTxt = getLocator("sender_MiddleName", BY_TYPE.BY_ID);
	private By senderCountryTxt = getLocator("Country", BY_TYPE.BY_ID);
	private By senderAddressTxt = getLocator("sender_Address", BY_TYPE.BY_ID);
	private By senderAddressLine2Txt = getLocator("sender_Address2", BY_TYPE.BY_ID);
	private By senderAddressLine3Txt = getLocator("sender_Address3", BY_TYPE.BY_ID);
	private By senderCityTxt = getLocator("sender_City", BY_TYPE.BY_ID);
	private By senderStateOrProvinceTxt = getLocator("State/Province", BY_TYPE.BY_ID);
	private By senderPostalCodeTxt = getLocator("sender_PostalCode", BY_TYPE.BY_ID);	
	private By senderOtherNameSuffixTxt = getLocator("sender_NameSuffixOther", BY_TYPE.BY_ID);	
	
	//Drop down value
	private By dropDownValue = getLocator("//a/strong", BY_TYPE.BY_XPATH);
	
	//Drop downs
	private By selectSenderTitle =  getLocator("sender_Title", BY_TYPE.BY_ID); 
	private By selectSenderNameSuffix =  getLocator("sender_NameSuffix", BY_TYPE.BY_ID);

	public SenderInfoPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean senderLastOrFamilyNameTxt_Presence() {
		return isPresent(senderLastOrFamilyNameTxt);
	}
	
	public boolean selectSenderTitle_Presence() {
		return isPresent(selectSenderTitle);
	}
	
	public boolean senderSecondLastOrFamilyNameTxt_Presence() {
		return isPresent(senderSecondLastOrFamilyNameTxt);
	}
	
	public boolean senderFirstOrGivenNameTxt_Presence() {
		return isPresent(senderFirstOrGivenNameTxt);
	}
	
	public boolean senderMiddleNameTxt_Presence() {
		return isPresent(senderMiddleNameTxt);
	}
	
	public boolean selectSenderNameSuffix_Presence() {
		return isPresent(selectSenderNameSuffix);
	}
	
	public boolean senderOtherNameSuffixTxt_Presence() {
		return isPresent(senderOtherNameSuffixTxt);
	}
	
	public boolean senderCountryTxt_Presence() {
		return isPresent(senderCountryTxt);
	}
	
	public boolean senderAddressTxt_Presence() {
		return isPresent(senderAddressTxt);
	}
	
	public boolean senderAddressLine2Txt_Presence() {
		return isPresent(senderAddressLine2Txt);
	}
	
	public boolean senderAddressLine3Txt_Presence() {
		return isPresent(senderAddressLine3Txt);
	}
	
	public boolean senderCityTxt_Presence() {
		return isPresent(senderCityTxt);
	}
	
	public boolean senderStateOrProvinceTxt_Presence() {
		return isPresent(senderStateOrProvinceTxt);
	}	
	
	public boolean senderPostalCodeTxt_Presence() {
		return isPresent(senderPostalCodeTxt);
	}	
	
	public void select_selectSenderTitle() {		
		Assert.assertTrue(this.selectSenderTitle_Presence(), "'Title' dropdown is absent on the sender info page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(0, 23)); 
		selectDropDownByIndex(selectSenderTitle, index);
		takeScreenshot("Select 'Title'.");
	}
	
	public String get_selectedSenderTitle() {		
		return getSelectedDropDownValue(selectSenderTitle);
	}
	
	public void type_senderLastOrFamilyNameTxt(String value) {		
		Assert.assertTrue(this.senderLastOrFamilyNameTxt_Presence(), "Sender last/family name textbox is absent on the sender info page.");
		type(senderLastOrFamilyNameTxt, value);		
		takeScreenshot("Enter sender last/family name.");
	}
	
	public void type_senderSecondLastOrFamilyNameTxt(String value) {		
		Assert.assertTrue(this.senderSecondLastOrFamilyNameTxt_Presence(), "Sender second last/family name textbox is absent on the sender info page.");
		type(senderSecondLastOrFamilyNameTxt, value);		
		takeScreenshot("Enter sender second last/family name.");
	}
	
	public void type_senderFirstOrGivenNameTxt(String value) {		
		Assert.assertTrue(this.senderFirstOrGivenNameTxt_Presence(), "Sender first or given name textbox is absent on the sender info page.");
		type(senderFirstOrGivenNameTxt, value);		
		takeScreenshot("Enter sender first or given name.");
	}
	
	public void type_senderMiddleNameTxt(String value) {		
		Assert.assertTrue(this.senderMiddleNameTxt_Presence(), "Sender middle name textbox is absent on the sender info page.");
		type(senderMiddleNameTxt, value);		
		takeScreenshot("Enter sender middle name.");
	}
	
	public void select_selectSenderNameSuffix() {		
		Assert.assertTrue(this.selectSenderNameSuffix_Presence(), "'Suffix' dropdown is absent on the sender info page.");
		int index = Integer.parseInt(BaseTest.generateRandomNumberWithinRange(0, 9));
		if(index == 9) {
			selectDropDownByIndex(selectSenderNameSuffix, index);
			type_senderOtherNameSuffixTxt("OMG");
			takeScreenshot("Select 'Suffix'.");
		}else {
			selectDropDownByIndex(selectSenderNameSuffix, index);
			takeScreenshot("Select 'Suffix'.");
		}	
	}
	
	public String get_selectedSenderNameSuffix() {		
		return getSelectedDropDownValue(selectSenderNameSuffix);
	}
	
	public void type_senderOtherNameSuffixTxt(String value) {		
		Assert.assertTrue(this.senderOtherNameSuffixTxt_Presence(), "Sender Other Name Suffix textbox is absent on the sender info page.");
		type(senderOtherNameSuffixTxt, value);		
		takeScreenshot("Enter sender other name suffix.");
	}
	
	public void type_senderCountryTxt(String value) {	
		Assert.assertTrue(this.senderCountryTxt_Presence(), "Sender country textbox is absent on the sender info page.");
		scrollToElementUsingJavascriptExecutor(senderCountryTxt);				
		type(senderCountryTxt, value);		
		click(dropDownValue);
		takeScreenshot("Enter sender country.");
	}
	
	public void type_senderAddressTxt(String value) {		
		Assert.assertTrue(this.senderAddressTxt_Presence(), "Sender address textbox is absent on the sender info page.");
		type(senderAddressTxt, value);		
		takeScreenshot("Enter sender address.");
	}
	
	public void type_senderAddressLine2Txt(String value) {		
		Assert.assertTrue(this.senderAddressLine2Txt_Presence(), "Sender address line 2 textbox is absent on the sender info page.");
		type(senderAddressLine2Txt, value);		
		takeScreenshot("Enter sender address line 3.");
	}
	
	public void type_senderAddressLine3Txt(String value) {		
		Assert.assertTrue(this.senderAddressLine3Txt_Presence(), "Sender address line 3 textbox is absent on the sender info page.");
		type(senderAddressLine3Txt, value);		
		takeScreenshot("Enter sender address line 3.");
	}
	
	public void type_senderCityTxt(String value) {		
		Assert.assertTrue(this.senderCityTxt_Presence(), "Sender city textbox is absent on the sender info page.");
		type(senderCityTxt, value);		
		takeScreenshot("Enter sender city.");
	}
	
	public void type_senderStateOrProvinceTxt(String value) { 		
		Assert.assertTrue(this.senderStateOrProvinceTxt_Presence(), "Sender state or province textbox is absent on the sender info page.");
		type(senderStateOrProvinceTxt, value);	
		click(dropDownValue);
		takeScreenshot("Enter state or province city.");
	}
	
	public void type_senderPostalCodeTxt(String value) {		
		Assert.assertTrue(this.senderPostalCodeTxt_Presence(), "Sender postal code textbox is absent on the sender info page.");
		type(senderPostalCodeTxt, value);		
		takeScreenshot("Enter sender postal code.");
	}		
}
