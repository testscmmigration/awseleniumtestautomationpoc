package com.saf.aw_nxt_app_pages;

import java.awt.AWTException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.saf.base.BasePage;

public class CloseReceiptsWindowPage extends BasePage {
	
	//Buttons
	private By printBtn = getLocator("#print-header > div > button.print.default", BY_TYPE.BY_CSSSELECTOR);
	private By saveBtn = getLocator("#print-header > div > button.print.default", BY_TYPE.BY_CSSSELECTOR);
	
	public CloseReceiptsWindowPage(WebDriver driver) {
		super(driver);		
	}
	
	public boolean printBtn_Presence() {
		return isPresent(printBtn);
	}
	
	public boolean saveBtn_Presence() {
		return isPresent(saveBtn);
	}
	
	public void click_printBtn(){		
		Assert.assertTrue(this.printBtn_Presence(), "'Print' button is absent on the dashboard page.");
		click(printBtn);	
		takeScreenshot("Click on 'Print' button on the dashboard page.");
	}
	
	public void click_saveBtn(){		
		Assert.assertTrue(this.saveBtn_Presence(), "'Save' button is absent on the dashboard page.");
		click(saveBtn);	
		takeScreenshot("Click on 'Save' button on the dashboard page.");
	}
	
	public void closeWindow() throws AWTException {				
		super.closeWindow();
	}
	
	public Set<String> getWindowHandles() {
		return driver.getWindowHandles();
	}	
}
