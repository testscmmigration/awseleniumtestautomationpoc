package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.DeliveryOptionsPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.ReceiverContactInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.SendDestinationAmountPage;
import com.saf.aw_nxt_app_pages.SendFromPage;
import com.saf.aw_nxt_app_pages.SenderContactInfoPage;
import com.saf.aw_nxt_app_pages.SenderIDsPage;
import com.saf.aw_nxt_app_pages.SenderInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USNewCusNewRcvr10Min_Low extends BaseTest {

	String sendAmount, senderFirstOrGivenName, senderMiddleName, senderLastOrFamilyName, senderSecondLastOrFamilyName,
			senderAddress, senderAddressLine2, senderAddressLine3, senderPrimaryPhone, DOBYear, DOBMonth, DOBDay,
			receiverFirsOrGivenName, receiverMiddleName, receiverLastOrFamilyName, receiverSecondLastOrFamilyName;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USNewCusNewRcvr10Min_Low() {
		super();
	}

	public USNewCusNewRcvr10Min_Low(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new USNewCusNewRcvr10Min_Low(this.getClass().getSimpleName(), mapDataSheet.get("Browser"),
				mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusNewRcvr10Min_Low_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		SendFromPage sendFromPage = new SendFromPage(getDriver());
		SendDestinationAmountPage sendDestinationAmountPage = new SendDestinationAmountPage(getDriver());
		DeliveryOptionsPage deliveryOptionsPage = new DeliveryOptionsPage(getDriver());
		SenderInfoPage senderInfoPage = new SenderInfoPage(getDriver());
		SenderIDsPage senderIDsPage = new SenderIDsPage(getDriver());
		SenderContactInfoPage senderContactInfoPage = new SenderContactInfoPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());
		ReceiverContactInfoPage receiverContactInfoPage = new ReceiverContactInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Send Money button
		dashboardPage.click_sendMoneyBtn();

		// Click on the New customer link
		sendFromPage.click_newCustomerLink();

		// Enter send amount
		sendAmount = generateRandomNumberWithinRange(senMinRangeA, senMinRangeB);
		dataToBeWritten.put("SendAmount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		sendDestinationAmountPage.type_sendAmountTxt(sendAmount);

		// Select currency
		sendDestinationAmountPage.select_selectCurrency(getValue("Currency"));

		// Select fee type
		sendDestinationAmountPage.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmountPage.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmountPage.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Enter promo code1
		if (!getValue("PromoCode1").equals(""))
			sendDestinationAmountPage.type_promoCode1Txt(getValue("PromoCode1"));

		// Enter promo code2
		if (!getValue("PromoCode2").equals(""))
			sendDestinationAmountPage.type_promoCode2Txt(getValue("PromoCode2"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the Cash Options
		deliveryOptionsPage.click_cashOptionsLink();

		// Select delivery Option
		deliveryOptionsPage.click_min10DlvrOpt();

		// Select Sender Title
		senderInfoPage.select_selectSenderTitle();
		dataToBeWritten.put("Sender_Title", senderInfoPage.get_selectedSenderTitle());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Sender First/Given Name
		senderFirstOrGivenName = generateRandomSenderFirstName();
		dataToBeWritten.put("Sender_FirstOrGiven Name", senderFirstOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderFirstOrGivenNameTxt(senderFirstOrGivenName);

		// Enter Sender Middle Name
		senderMiddleName = generateRandomSenderMiddleName();
		dataToBeWritten.put("Sender_Middle Name", senderMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderMiddleNameTxt(senderMiddleName);

		// Enter Sender Last/Family Name
		senderLastOrFamilyName = generateRandomSenderLastName();
		dataToBeWritten.put("Sender_LastOrFamily Name", senderLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderLastOrFamilyNameTxt(senderLastOrFamilyName);

		// Enter Sender Second Last/Family Name
		senderSecondLastOrFamilyName = generateRandomSenderSecondLastName();
		dataToBeWritten.put("Sender_Second LastOrFamily Name", senderSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderSecondLastOrFamilyNameTxt(senderSecondLastOrFamilyName);

		// Select Sender Suffix
		senderInfoPage.select_selectSenderNameSuffix();
		dataToBeWritten.put("Sender_Suffix", senderInfoPage.get_selectedSenderNameSuffix());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Sender Country
		senderInfoPage.type_senderCountryTxt(getValue("Sender_Country"));

		// Enter Sender Address
		senderAddress = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Sender_Address", senderAddress);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderAddressTxt(senderAddress);

		// Enter Sender Address Line 2
		senderAddressLine2 = getValue("Sender_AddressLine2");
		senderInfoPage.type_senderAddressLine2Txt(senderAddressLine2);

		// Enter Sender Address Line 3
		senderAddressLine3 = "Apt# " + BaseTest.generateRandomNumber(3);
		dataToBeWritten.put("Sender_AddressLine3", senderAddressLine3);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderAddressLine3Txt(senderAddressLine3);

		// Enter Sender City
		senderInfoPage.type_senderCityTxt(getValue("Sender_City"));

		// Enter Sender State/Province
		senderInfoPage.type_senderStateOrProvinceTxt(getValue("Sender_StateOrProvince"));

		// Enter Sender Postal Code
		senderInfoPage.type_senderPostalCodeTxt(getValue("Sender_Postal Code"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Select Sender Date of Birth: Year
		DOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("SenderDOBYear", DOBYear);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBYear(DOBYear);

		// Select Sender Date of Birth: Month
		DOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("SenderDOBMonth", DOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBMonth(DOBMonth);

		// Select Sender Date of Birth: Day
		DOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("SenderDOBDay", DOBDay);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBDay(DOBDay);

		// Enter Sender Birth Country
		senderIDsPage.type_birthCountryTxt(getValue("SenderBirthCountry"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Enter Sender primary phone country code
		senderContactInfoPage.type_primaryPhoneCountryCodeTxt(getValue("Sender_Primary Phone Country Code"));
		senderContactInfoPage.selectRequiredCountry(getValue("Sender_Primary Phone Country Name"));

		// Enter Sender Primary Phone Number
		senderPrimaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Sender_Primary Phone Number", senderPrimaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		senderContactInfoPage.type_senderPrimaryPhoneTxt(senderPrimaryPhone);

		// Select Mobile Phone
		if (!getValue("Seneder_Mobile Phone").equals("N"))
			senderContactInfoPage.check_senderMobilePhoneChk();

		// Type Email
		if (!getValue("Sender_Email").equals(""))
			senderContactInfoPage.type_senderEmailTxt(getValue("Sender_Email"));

		// Select Receive Transaction Status
		senderContactInfoPage
				.select_selectSenderReceiveTransactionStatus(getValue("Sender_Receive Transaction Status"));

		// Select Receive Offers
		senderContactInfoPage.select_selectSenderReceiveOffers(getValue("Sender_Receive Offers"));

		// Select Preferred Language
		senderContactInfoPage.select_selectSenderPreferredLanguage(getValue("Sender_Preferred Language"));

		// Select Enroll in Plus Program?
		if (!getValue("Sender_Enroll in Plus Program").equals("N"))
			senderContactInfoPage.check_senderEnrollInPlusProgramChk();

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Enter Receiver First/Given Name
		receiverFirsOrGivenName = generateRandomReceiverFirstName();
		dataToBeWritten.put("Receiver_First/Given Name", receiverFirsOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverFirsOrGivenNameTxt(receiverFirsOrGivenName);

		// Enter Receiver Middle Name
		receiverMiddleName = generateRandomReceiverMiddleName();
		dataToBeWritten.put("Receiver_Middle Name", receiverMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverMiddleNameTxt(receiverMiddleName);

		// Enter Receiver Last/Family Name
		receiverLastOrFamilyName = generateRandomReceiverLastName();
		dataToBeWritten.put("Receiver_LastOrFamily Name", receiverLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverLastOrFamilyNameTxt(receiverLastOrFamilyName);

		// Enter Receiver Second Last/Family Name
		receiverSecondLastOrFamilyName = generateRandomReceiverSecondLastName();
		dataToBeWritten.put("Receiver_Second LastOrFamily Name", receiverSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverSecondLastOrFamilyNameTxt(receiverSecondLastOrFamilyName);

		// Select Receiver Suffix
		receiverInfoPage.select_selectReceiverSuffix();
		dataToBeWritten.put("Receiver_Suffix", receiverInfoPage.get_selectedReceiverSuffix());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Receiver Email
		if (!getValue("Receiver_Email").equals(""))
			receiverContactInfoPage.type_receiverEmailTxt(getValue("Receiver_Email"));

		// Enter Message Field 1
		if (!getValue("Message Field 1").equals(""))
			receiverInfoPage.type_messageField1Txt(getValue("Message Field 1"));

		// Enter Message Field 2
		if (!getValue("Message Field 2").equals(""))
			receiverInfoPage.type_messageField2Txt(getValue("Message Field 2"));

		// Enter Test Question
		if (!getValue("Test Question").equals(""))
			receiverInfoPage.type_testQuestionTxt(getValue("Test Question"));

		// Enter Test Answer
		if (!getValue("Test Answer").equals(""))
			receiverInfoPage.type_testAnswerTxt(getValue("Test Answer"));

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		if (getValue("Sender_Enroll in Plus Program").equals("Y")) {
			String mgPlusNumber = commonControlsPage.getMoneyGramPlusNumber();
			dataToBeWritten.put("MoneyGram Plus Number", mgPlusNumber);
			addDataToOutputExcel(dataToBeWritten);
		}

		// Click on the send money button
		commonControlsPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = commonControlsPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		commonControlsPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Available"),
				"The transaction status is not \"Availble\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}