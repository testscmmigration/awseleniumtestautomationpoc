package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.BillPayPaymentDetailsPage;
import com.saf.aw_nxt_app_pages.BillPaySearchPage;
import com.saf.aw_nxt_app_pages.BillPaySenderIDsPage;
import com.saf.aw_nxt_app_pages.BillPaySenderInfoPage;
import com.saf.aw_nxt_app_pages.BillPayThirdPartyInfoPage;
import com.saf.aw_nxt_app_pages.BillerSearchResultsPage;
import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class USNewCusBPEP_High extends BaseTest {

	String sendAmount, lastOrFamilyName, firstOrGivenName, middleName, address, primaryPhone, senderAlienID,
			senderDriversLicense, senderGovernmentID, senderPassport, senderStateID, senderInternationalID,
			senderSocialSecurityNumber, senderTaxID, DOBYear, DOBMonth, DOBDay, thirdPartyOrganizationName,
			thirdPartyLastOrFamilyName, thirdPartyFirstOrGivenName, thirdPartyMiddleName, thirdPartyAddress,
			thirdPartyDOBYear, thirdPartyDOBMonth, thirdPartyDOBDay, thirdPartyAlienID, thirdPartyInternationalID,
			thirdPartySocialSecurityNumber, thirdPartyTaxID;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USNewCusBPEP_High() {
		super();
	}

	public USNewCusBPEP_High(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USNewCusBPEP_High(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusBPEP_High_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		BillPaySearchPage billPaySearhPage = new BillPaySearchPage(getDriver());
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		BillerSearchResultsPage billerSearchResultsPage = new BillerSearchResultsPage(getDriver());
		BillPayPaymentDetailsPage billPayPaymentDetails = new BillPayPaymentDetailsPage(getDriver());
		BillPaySenderInfoPage billPaySenderInfoPage = new BillPaySenderInfoPage(getDriver());
		BillPaySenderIDsPage billPaySenderIDsPage = new BillPaySenderIDsPage(getDriver());
		BillPayThirdPartyInfoPage billPayThirdPartyInfoPage = new BillPayThirdPartyInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTranPage = new FindTranPage(getDriver());
		TranDetailsPage tranDetailsPage = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Pay a Bill button
		dashboardPage.click_payaBillBtn();

		// Enter Biller name, code, or keyword
		if (!getValue("Biller name, code, or keyword").equals(""))
			billPaySearhPage.type_billerNameCodeOrKeywordTxt(getValue("Biller name, code, or keyword"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Click on the biller search result
		billerSearchResultsPage.click_billerSearchResult();

		// Click on the New customer link
		billPaySearhPage.click_newCustomerLink();

		// Enter Payment amount
		sendAmount = generateRandomNumberWithinRange(bpMaxRangeA, bpMaxRangeB);
		dataToBeWritten.put("Payment amount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		billPayPaymentDetails.type_paymentAmountTxt(sendAmount);

		// Enter Customer account number
		billPayPaymentDetails.type_customerAccountNumberTxt(getValue("Customer account number"));

		// Enter Add message 1
		if (!getValue("Add message 1").equals(""))
			billPayPaymentDetails.type_addMessage1Txt(getValue("Add message 1"));

		// Enter Add message 2
		if (!getValue("Add message 2").equals(""))
			billPayPaymentDetails.type_addMessage2Txt(getValue("Add message 2"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Enter Last/Family Name
		lastOrFamilyName = generateRandomSenderLastName();
		dataToBeWritten.put("Last or Family Name", lastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_lastOrFamilyNameTxt(lastOrFamilyName);

		// Enter First/Given Name
		firstOrGivenName = generateRandomSenderFirstName();
		dataToBeWritten.put("First or Given Name", firstOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_firstOrGivenNameTxt(firstOrGivenName);

		// Enter Middle Name
		middleName = generateRandomSenderMiddleName();
		dataToBeWritten.put("Middle Name", middleName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_middleNameTxt(middleName);

		// Enter Country
		billPaySenderInfoPage.type_countryTxt(getValue("Country"));

		// Enter Address
		address = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Address", address);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_addressTxt(address);

		// Enter City
		billPaySenderInfoPage.type_cityTxt(getValue("City"));

		// Enter State/Province
		billPaySenderInfoPage.type_stateOrProvinceTxt(getValue("State or Province"));

		// Enter Postal Code
		billPaySenderInfoPage.type_postalCodeTxt(getValue("Postal Code"));

		// Enter Primary Phone Country Code
		billPaySenderInfoPage.type_primaryPhoneCountryCodeTxt(getValue("Primary Phone Country Code"));
		billPaySenderInfoPage.selectRequiredCountry(getValue("Primary Phone Country Name"));

		// Enter Primary Phone Number
		primaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Primary Phone Number", primaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_primaryPhoneNumberTxt(primaryPhone);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select Photo ID: ID Type
		billPaySenderIDsPage.select_selectPhotoIDType(getValue("PhotoIDType"));

		// Enter Photo ID: ID Number
		switch (getValue("PhotoIDType")) {
		case "Alien ID":
			senderAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("PhotoIDNumber", senderAlienID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_photoIDNumberTxt(senderAlienID);
			break;
		case "Drivers License":
			senderDriversLicense = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("PhotoIDNumber", senderDriversLicense);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_photoIDNumberTxt(senderDriversLicense);
			break;
		case "Government ID":
			senderGovernmentID = "G" + BaseTest.generateRandomNumber(14);
			dataToBeWritten.put("PhotoIDNumber", senderGovernmentID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_photoIDNumberTxt(senderGovernmentID);
			break;
		case "Passport":
			senderPassport = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(7);
			dataToBeWritten.put("PhotoIDNumber", senderPassport);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_photoIDNumberTxt(senderPassport);
			break;
		case "State ID":
			senderStateID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("PhotoIDNumber", senderStateID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_photoIDNumberTxt(senderStateID);
			break;
		}

		// Enter Photo ID: ID Issue Country
		billPaySenderIDsPage.type_photoIDIssueCountryTxt(getValue("PhotoIDIssueCountry"));

		// Enter Photo ID: ID Issue State/Province
		billPaySenderIDsPage.type_photoIDIssueStateOrProvinceTxt(getValue("PhotoIDIssueStateOrProvince"));

		// Select Second form of ID: Secondary ID Type
		billPaySenderIDsPage.select_selectSecondaryIDType(getValue("SecondaryIDType"));

		// Enter Second form of ID: Secondary ID Number
		switch (getValue("SecondaryIDType")) {
		case "Alien ID":
			senderAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("SecondaryIDNumber", senderAlienID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_secondFormOfIDNumberTxt(senderAlienID);
			break;
		case "International ID":
			senderInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("SecondaryIDNumber", senderInternationalID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_secondFormOfIDNumberTxt(senderInternationalID);
			break;
		case "Social Security Number":
			senderSocialSecurityNumber = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("SecondaryIDNumber", senderSocialSecurityNumber);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_secondFormOfIDNumberTxt(senderSocialSecurityNumber);
			break;
		case "Tax ID":
			senderTaxID = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("SecondaryIDNumber", senderTaxID);
			addDataToOutputExcel(dataToBeWritten);
			billPaySenderIDsPage.type_secondFormOfIDNumberTxt(senderTaxID);
			break;
		}

		// Select Sender Date of Birth: Year
		DOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("DOBYear", DOBYear);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBYear(DOBYear);

		// Select Sender Date of Birth: Month
		DOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("DOBMonth", DOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBMonth(DOBMonth);

		// Select Sender Date of Birth: Day
		DOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("DOBDay", DOBDay);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBDay(DOBDay);

		// Select Occupation
		billPaySenderIDsPage.select_selectOccupation();
		dataToBeWritten.put("Occupation", billPaySenderIDsPage.get_selectedOccupation());
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select Third party
		switch (getValue("Third Party")) {
		case "None":
			// Choose Third Party type as 'None'
			billPayThirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			// Choose Third Party type as 'Organization'
			billPayThirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			// Enter Organization Name
			billPayThirdPartyInfoPage.type_thirdPartySenderOrganizationNameTxt(getValue("ThirdParty_OrganizationName"));

			// Enter Country
			billPayThirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.type_thirdPartySenderAddressTxt(thirdPartyAddress);

			// Enter City
			billPayThirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("ThirdParty_City"));

			// Enter State.Province
			billPayThirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			billPayThirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter third party id type
			billPayThirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			// Choose Third Party type as 'Person'
			billPayThirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			// Enter Last/Family Name
			thirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("ThirdParty_Person_LastOrFamilyName", thirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.type_thirdPartySenderPersonLastOrFamilyNameTxt(thirdPartyLastOrFamilyName);

			// Enter First/Given Name
			thirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("ThirdParty_Person_FirstOrGivenName", thirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.type_thirdPartySenderPersonFirstOrGivenNameTxt(thirdPartyFirstOrGivenName);

			// Enter Middle Name
			thirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("ThirdParty_Person_MiddleName", thirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.type_thirdPartySenderPersonMiddleNameTxt(thirdPartyMiddleName);

			// Enter Country
			billPayThirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.type_thirdPartySenderAddressTxt(thirdPartyAddress);

			// Enter City
			billPayThirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("ThirdParty_City"));

			// Enter State/Province
			billPayThirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			billPayThirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			billPayThirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				billPayThirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyTaxID);
				break;
			}

			// Select Date of Birth: Year
			thirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBYear", thirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.select_selectThirdPartySenderDOBYear(thirdPartyDOBYear);

			// Select Date of Birth: Month
			thirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBMonth", thirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.select_selectThirdPartySenderDOBMonth(thirdPartyDOBMonth);

			// Select Date of Birth: Day
			thirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBDay", thirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			billPayThirdPartyInfoPage.select_selectThirdPartySenderDOBDay(thirdPartyDOBDay);

			// Select Occupation
			billPayThirdPartyInfoPage.select_selectThirdPartySenderOccupation();
			dataToBeWritten.put("ThirdParty_Person_Occupation",
					billPayThirdPartyInfoPage.get_selectedThirdPartySenderOccupation());
			addDataToOutputExcel(dataToBeWritten);

			break;
		}

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on the send money button
		billPaySenderInfoPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = billPaySenderInfoPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		billPaySenderInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTranPage.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetailsPage.getTransactionStatus().contains("Available"),
				"The transaction is not in \"Available\" status.");

		// Click on the Finish button
		tranDetailsPage.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
