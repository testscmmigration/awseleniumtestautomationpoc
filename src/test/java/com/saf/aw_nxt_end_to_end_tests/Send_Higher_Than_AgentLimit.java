package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.SendDestinationAmountPage;
import com.saf.aw_nxt_app_pages.SendFromPage;
import com.saf.aw_nxt_app_pages.StagedTranDetails;
import com.saf.aw_nxt_app_pages.StagedTranListPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class Send_Higher_Than_AgentLimit extends BaseTest {

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public Send_Higher_Than_AgentLimit() {
		super();
	}

	public Send_Higher_Than_AgentLimit(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new Send_Higher_Than_AgentLimit(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void Send_Higher_Than_AgentLimit_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		SendDestinationAmountPage sendDestinationAmount = new SendDestinationAmountPage(getDriver());
		SendFromPage sendFromPage = new SendFromPage(getDriver());
		StagedTranListPage stagedTranListPage = new StagedTranListPage(getDriver());
		StagedTranDetails stagedTranDetails = new StagedTranDetails(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// ***************************************************Normal send flow - New
		// Customer***************************************************

		// Click on the Send Money button
		dashboardPage.click_sendMoneyBtn();

		// Click on the New customer link
		sendFromPage.click_newCustomerLink();

		// Enter send amount
		sendDestinationAmount.type_sendAmountTxt(getValue("SendAmount"));

		// Select currency
		sendDestinationAmount.select_selectCurrency(getValue("Currency"));

		// Select fee type
		sendDestinationAmount.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmount.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmount.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the close button on the toaster message
		sendDestinationAmount.click_closeBtn();

		// ***************************************************Estimate
		// Fee***************************************************
		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Click on 'Yes' button on the 'Confirmation required' popup
		sendDestinationAmount.click_yesBtn();

		// Click on the Estimate Fee button
		dashboardPage.click_estimateFeeBtn();

		// Enter send amount
		sendDestinationAmount.type_sendAmountTxt(getValue("SendAmount"));

		// Select currency
		sendDestinationAmount.select_selectCurrency(getValue("Currency"));

		// Select fee type
		sendDestinationAmount.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmount.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmount.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the close button on the toaster message
		sendDestinationAmount.click_closeBtn();

		// ***************************************************Staged send
		// flow***************************************************
		if (!getValue("Sender_Name").equals("")) {
			// Click on Transact header link
			dashboardPage.click_transactHeaderLnk();

			// Click on 'Yes' button on the 'Confirmation required' popup
			// sendDestinationAmount.click_yesBtn();

			// Click on the Complete Mobile / Kiosk Transaction button
			dashboardPage.click_completeMobileOrKioskTransactionBtn();

			// Click on the required Staged transaction after check
			stagedTranListPage.select_stagedTransaction(getValue("Sender_Name"));

			// Click on the close button on the toaster message
			stagedTranDetails.click_closeBtn();
		}

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
