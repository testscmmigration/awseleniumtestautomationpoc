package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.ReceiveProfileFoundPage;
import com.saf.aw_nxt_app_pages.ReceiverIDsPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverProfileDetailsPage;
import com.saf.aw_nxt_app_pages.ThirdPartyInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USExisCusREC_High extends BaseTest {

	String thirdPartyAddress, thirdPartyAlienID, thirdPartyInternationalID, thirdPartySocialSecurityNumber,
			thirdPartyTaxID, thirdPartyLastOrFamilyName, thirdPartyFirstOrGivenName, thirdPartyMiddleName,
			thirdPartyDOBYear, thirdPartyDOBMonth, thirdPartyDOBDay, thirdPartyOccupation;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USExisCusREC_High() {
		super();
	}

	public USExisCusREC_High(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USExisCusREC_High(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusREC_High_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControls = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		FindTranPage findTranPage = new FindTranPage(getDriver());
		ReceiveProfileFoundPage receiveProfileFoundPage = new ReceiveProfileFoundPage(getDriver());
		ReceiverProfileDetailsPage receiverProfileDetailsPage = new ReceiverProfileDetailsPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());
		ReceiverIDsPage receiverIDsPage = new ReceiverIDsPage(getDriver());
		ThirdPartyInfoPage thirdPartyInfoPage = new ThirdPartyInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button
			loginPage.click_signIn();
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Receive Money button
		dashboardPage.click_receiveMoneyBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Select Sender Date of Birth: Year
		findTranPage.select_selectDOBYear(getValue("Date of Birth Year"));

		// Select Sender Date of Birth: Month
		findTranPage.select_selectDOBMonth(getValue("Date of Birth Month"));

		// Select Sender Date of Birth: Day
		findTranPage.select_selectDOBDay(getValue("Date of Birth Day"));

		// Click on Next button
		findTranPage.click_nextBtn();

		// Select the receiver from the returned search result
		receiveProfileFoundPage.choose_prvReceiverSearchResultList(getValue("ReceiverName"));

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Take screen shot of the sender profile details page
		receiverProfileDetailsPage.takeScreenshot("Receiver Profile: 1");
		receiverProfileDetailsPage.scrollTo_otherLbl();

		// Click on Next button
		commonControls.click_nextBtn();

		// Click on the Next button
		commonControls.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Click on the Next button
		commonControls.click_nextBtn();

		// Select Photo ID: ID Choice
		receiverIDsPage.select_selectPhotoIDChoice(getValue("SenderPrimaryIDChoice"));

		// Enter Photo ID: Last 4 digits of ID
		receiverIDsPage.type_photoIDLast4digitsofIDTxt(getValue("Last4DigitsOfSenderPrimaryIDNumber"));

		// Select Second form of ID: Secondary ID Choice
		receiverIDsPage.select_selectSecondFormOfIDChoice(getValue("SenderSecondaryIDChoice"));

		// Enter Second form of ID: Last 4 digits of ID
		receiverIDsPage.type_secondFormOfIDLast4digitsofIDTxt(getValue("Last4DigitsOfSenderSecondaryIDNumber"));

		// Click on the Next button
		commonControls.click_nextBtn();

		// Select Third party
		switch (getValue("Third Party")) {
		case "None":
			// Choose Third Party type as 'None'
			thirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			// Choose Third Party type as 'Organization'
			thirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			// Enter Organization Name
			thirdPartyInfoPage.type_thirdPartyReceiverOrganizationNameTxt(getValue("ThirdParty_OrganizationName"));

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("ThirdParty_City"));

			// Enter State.Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter third party id type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			// Choose Third Party type as 'Person'
			thirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			// Enter Last/Family Name
			thirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("ThirdParty_Person_LastOrFamilyName", thirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonLastOrFamilyNameTxt(thirdPartyLastOrFamilyName);

			// Enter First/Given Name
			thirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("ThirdParty_Person_FirstOrGivenName", thirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonFirstOrGivenNameTxt(thirdPartyFirstOrGivenName);

			// Enter Middle Name
			thirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("ThirdParty_Person_MiddleName", thirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonMiddleNameTxt(thirdPartyMiddleName);

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("ThirdParty_City"));

			// Enter State/Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}

			// Select Date of Birth: Year
			thirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBYear", thirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBYear(thirdPartyDOBYear);

			// Select Date of Birth: Month
			thirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBMonth", thirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBMonth(thirdPartyDOBMonth);

			// Select Date of Birth: Day
			thirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBDay", thirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBDay(thirdPartyDOBDay);

			// Select Occupation
			thirdPartyInfoPage.select_selectThirdPartyReceiverOccupation();
			dataToBeWritten.put("ThirdParty_Person_Occupation",
					thirdPartyInfoPage.get_selectedThirdPartyReceiverOccupation());
			addDataToOutputExcel(dataToBeWritten);

			break;
		}

		// Click on the Next button
		commonControls.click_nextBtn();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Payout button
		receiverInfoPage.click_payoutBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on Finished button
		receiverInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTranPage.click_nextBtn();

		// Check whether the transaction status is "Received"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Received"),
				"The transaction status is not \"Received\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
