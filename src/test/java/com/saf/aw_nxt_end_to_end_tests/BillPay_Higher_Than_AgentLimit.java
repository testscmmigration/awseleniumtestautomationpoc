package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.BillPayPaymentDetailsPage;
import com.saf.aw_nxt_app_pages.BillPaySearchPage;
import com.saf.aw_nxt_app_pages.BillPaySendFromPage;
import com.saf.aw_nxt_app_pages.BillerSearchResultsPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.StagedTranDetails;
import com.saf.aw_nxt_app_pages.StagedTranListPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class BillPay_Higher_Than_AgentLimit extends BaseTest {

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public BillPay_Higher_Than_AgentLimit() {
		super();
	}

	public BillPay_Higher_Than_AgentLimit(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new BillPay_Higher_Than_AgentLimit(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void BillPay_Higher_Than_AgentLimit_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		BillPaySearchPage billPaySearhPage = new BillPaySearchPage(getDriver());
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		BillerSearchResultsPage billerSearchResultsPage = new BillerSearchResultsPage(getDriver());
		BillPaySendFromPage billPaySendFromPage = new BillPaySendFromPage(getDriver());
		BillPayPaymentDetailsPage billPayPaymentDetails = new BillPayPaymentDetailsPage(getDriver());
		StagedTranListPage stagedTranListPage = new StagedTranListPage(getDriver());
		StagedTranDetails stagedTranDetails = new StagedTranDetails(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// ***************************************************Normal bill pay
		// flow***************************************************

		// Click on the Pay a Bill button
		dashboardPage.click_payaBillBtn();

		// Enter Biller name, code, or keyword
		if (!getValue("Biller name, code, or keyword").equals(""))
			billPaySearhPage.type_billerNameCodeOrKeywordTxt(getValue("Biller name, code, or keyword"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Click on the biller search result
		billerSearchResultsPage.click_billerSearchResult();

		// Enter Phone number to search for the customer
		if (!getValue("Phone number").equals(""))
			billPaySearhPage.type_phoneNumberTxt(getValue("Phone number"));

		// Enter Plus number
		if (!getValue("Plus number").equals(""))
			billPaySearhPage.type_plusNumberTxt(getValue("Plus number"));

		// Enter Account number
		if (!getValue("Account number").equals(""))
			billPaySearhPage.type_accountNumberTxt(getValue("Account number"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select the sender from the returned search result
		billPaySendFromPage.choose_prvSenderSearchResultList(getValue("SenderName"));

		// Enter Payment amount
		billPayPaymentDetails.type_paymentAmountTxt(getValue("Payment amount"));

		// Enter Customer account number
		billPayPaymentDetails.type_customerAccountNumberTxt(getValue("Customer account number"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Click on the close button on the toaster message
		billPayPaymentDetails.click_closeBtn();

		// ***************************************************Staged bill pay
		// flow***************************************************
		if (!getValue("Staged_Sender_Name").equals("")) {
			// Click on Transact header link
			dashboardPage.click_transactHeaderLnk();

			// Click on 'Yes' button on the 'Confirmation required' popup
			billPayPaymentDetails.click_yesBtn();

			// Click on the Complete Mobile / Kiosk Transaction button
			dashboardPage.click_completeMobileOrKioskTransactionBtn();

			// Click on the required Staged transaction after check
			stagedTranListPage.select_stagedTransaction(getValue("Staged_Sender_Name"));

			// Click on the close button on the toaster message
			stagedTranDetails.click_closeBtn();
		}

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
