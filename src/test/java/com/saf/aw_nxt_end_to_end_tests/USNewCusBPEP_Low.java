package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.BillPayPaymentDetailsPage;
import com.saf.aw_nxt_app_pages.BillPaySearchPage;
import com.saf.aw_nxt_app_pages.BillPaySenderIDsPage;
import com.saf.aw_nxt_app_pages.BillPaySenderInfoPage;
import com.saf.aw_nxt_app_pages.BillerSearchResultsPage;
import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class USNewCusBPEP_Low extends BaseTest {

	String sendAmount, lastOrFamilyName, firstOrGivenName, middleName, address, primaryPhone, DOBYear, DOBMonth, DOBDay;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USNewCusBPEP_Low() {
		super();
	}

	public USNewCusBPEP_Low(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USNewCusBPEP_Low(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusBPEP_High_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		BillPaySearchPage billPaySearhPage = new BillPaySearchPage(getDriver());
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		BillerSearchResultsPage billerSearchResultsPage = new BillerSearchResultsPage(getDriver());
		BillPayPaymentDetailsPage billPayPaymentDetails = new BillPayPaymentDetailsPage(getDriver());
		BillPaySenderInfoPage billPaySenderInfoPage = new BillPaySenderInfoPage(getDriver());
		BillPaySenderIDsPage billPaySenderIDsPage = new BillPaySenderIDsPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTranPage = new FindTranPage(getDriver());
		TranDetailsPage tranDetailsPage = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Pay a Bill button
		dashboardPage.click_payaBillBtn();

		// Enter Biller name, code, or keyword
		if (!getValue("Biller name, code, or keyword").equals(""))
			billPaySearhPage.type_billerNameCodeOrKeywordTxt(getValue("Biller name, code, or keyword"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Click on the biller search result
		billerSearchResultsPage.click_billerSearchResult();

		// Click on the New customer link
		billPaySearhPage.click_newCustomerLink();

		// Enter Payment amount
		sendAmount = generateRandomNumberWithinRange(bpMinRangeA, bpMinRangeB);
		dataToBeWritten.put("Payment amount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		billPayPaymentDetails.type_paymentAmountTxt(sendAmount);

		// Enter Customer account number
		billPayPaymentDetails.type_customerAccountNumberTxt(getValue("Customer account number"));

		// Enter Add message 1
		if (!getValue("Add message 1").equals(""))
			billPayPaymentDetails.type_addMessage1Txt(getValue("Add message 1"));

		// Enter Add message 2
		if (!getValue("Add message 2").equals(""))
			billPayPaymentDetails.type_addMessage2Txt(getValue("Add message 2"));

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Enter Last/Family Name
		lastOrFamilyName = generateRandomSenderLastName();
		dataToBeWritten.put("Last or Family Name", lastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_lastOrFamilyNameTxt(lastOrFamilyName);

		// Enter First/Given Name
		firstOrGivenName = generateRandomSenderFirstName();
		dataToBeWritten.put("First or Given Name", firstOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_firstOrGivenNameTxt(firstOrGivenName);

		// Enter Middle Name
		middleName = generateRandomSenderMiddleName();
		dataToBeWritten.put("Middle Name", middleName);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_middleNameTxt(middleName);

		// Enter Country
		billPaySenderInfoPage.type_countryTxt(getValue("Country"));

		// Enter Address
		address = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Address", address);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_addressTxt(address);

		// Enter City
		billPaySenderInfoPage.type_cityTxt(getValue("City"));

		// Enter State/Province
		billPaySenderInfoPage.type_stateOrProvinceTxt(getValue("State or Province"));

		// Enter Postal Code
		billPaySenderInfoPage.type_postalCodeTxt(getValue("Postal Code"));

		// Enter Primary Phone Country Code
		billPaySenderInfoPage.type_primaryPhoneCountryCodeTxt(getValue("Primary Phone Country Code"));
		billPaySenderInfoPage.selectRequiredCountry(getValue("Primary Phone Country Name"));

		// Enter Primary Phone Number
		primaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Primary Phone Number", primaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderInfoPage.type_primaryPhoneNumberTxt(primaryPhone);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select Date of Birth: Year
		DOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("DOBYear", DOBYear);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBYear(DOBYear);

		// Select Date of Birth: Month
		DOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("DOBMonth", DOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBMonth(DOBMonth);

		// Select Date of Birth: Day
		DOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("DOBDay", DOBDay);
		addDataToOutputExcel(dataToBeWritten);
		billPaySenderIDsPage.select_selectDOBDay(DOBDay);

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on the send money button
		billPaySenderInfoPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = billPaySenderInfoPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		billPaySenderInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTranPage.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetailsPage.getTransactionStatus().contains("Available"),
				"The transaction is not in \"Available\" status.");

		// Click on the Finish button
		tranDetailsPage.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
