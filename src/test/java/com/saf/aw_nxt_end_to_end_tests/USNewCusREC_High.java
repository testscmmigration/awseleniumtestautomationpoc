package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.ReceiverContactInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverIDsPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.ThirdPartyInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USNewCusREC_High extends BaseTest {

	String DOBYear, DOBMonth, DOBDay, receiverOccupation, receiverAddress, receiverAddressLine2, receiverAddressLine3,
			receiverPrimaryPhone, receiverAlienID, receiverDriversLicense, receiverGovernmentID, receiverPassport,
			receiverStateID, receiverInternationalID, receiverSocialSecurityNumber, receiverTaxID, thirdPartyAddress,
			thirdPartyAlienID, thirdPartyInternationalID, thirdPartySocialSecurityNumber, thirdPartyTaxID,
			thirdPartyLastOrFamilyName, thirdPartyFirstOrGivenName, thirdPartyMiddleName, thirdPartyDOBYear,
			thirdPartyDOBMonth, thirdPartyDOBDay, thirdPartyOccupation;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USNewCusREC_High() {
		super();
	}

	public USNewCusREC_High(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USNewCusREC_High(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusREC_High_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControls = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());
		ReceiverContactInfoPage receiverContactInfoPage = new ReceiverContactInfoPage(getDriver());
		ReceiverIDsPage receiverIDsPage = new ReceiverIDsPage(getDriver());
		ThirdPartyInfoPage thirdPartyInfoPage = new ThirdPartyInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Receive Money button
		dashboardPage.click_receiveMoneyBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Select Receiver Date of Birth: Year
		DOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("Date of Birth Year", DOBYear);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBYear(DOBYear);

		// Select Receiver Date of Birth: Month
		DOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("Date of Birth Month", DOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBMonth(DOBMonth);

		// Select Receiver Date of Birth: Day
		DOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("Date of Birth Day", DOBDay);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBDay(DOBDay);

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Next button
		commonControls.click_nextBtn();

		// Select Receiver Title
		receiverInfoPage.select_selectReceiverTitle();
		dataToBeWritten.put("Title", receiverInfoPage.get_selectedReceiverTitle());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Birth Country
		receiverInfoPage.type_receiverBirthCountryTxt(getValue("Birth Country"));

		// Select Occupation
		receiverInfoPage.select_selectReceiverOccupation();

		// Enter Country
		receiverInfoPage.type_receiverCountryTxt(getValue("Country"));

		// Enter Receiver Address
		receiverAddress = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Address", receiverAddress);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressTxt(receiverAddress);

		// Enter Receiver Address Line 2
		receiverAddressLine2 = getValue("Address Line 2");
		receiverInfoPage.type_receiverAddressLine2Txt(receiverAddressLine2);

		// Enter Receiver Address Line 3
		receiverAddressLine3 = "Apt# " + BaseTest.generateRandomNumber(3);
		dataToBeWritten.put("Address Line 3", receiverAddressLine3);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressLine3Txt(receiverAddressLine3);

		// Enter City
		receiverInfoPage.type_receiverCityTxt(getValue("City"));

		// Enter State/Province
		receiverInfoPage.type_receiverStateOrProvinceTxt(getValue("State/Province"));

		// Enter Postal Code
		receiverInfoPage.type_receiverPostalCodeTxt(getValue("Postal Code"));

		// Click on Next button
		commonControls.click_nextBtn();

		// Enter Receiver primary phone country code
		receiverContactInfoPage.type_receiverPrimaryPhoneCountryCodeTxt(getValue("Primary Phone Country Code"));
		receiverContactInfoPage.selectRequiredCountry(getValue("Primary Phone Country Name"));

		// Enter Receiver Primary Phone Number
		receiverPrimaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Primary Phone Number", receiverPrimaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		receiverContactInfoPage.type_receiverPrimaryPhoneTxt(receiverPrimaryPhone);

		// Receiver Mobile Phone
		if (!getValue("Mobile Phone").equals("N"))
			receiverContactInfoPage.check_receiverMobilePhoneChk();

		// Enter Receiver Email
		if (!getValue("Email").equals(""))
			receiverContactInfoPage.type_receiverEmailTxt(getValue("Email"));

		// Select Receive Transaction Status
		receiverContactInfoPage.select_selectReceiverReceiveTransactionStatus(getValue("Receive Transaction Status"));

		// Select Receive Offers
		receiverContactInfoPage.select_selectReceiverReceiveOffers(getValue("Receive Offers"));

		// Select Preferred Language
		receiverContactInfoPage.select_selectReceiverPreferredLanguage(getValue("Preferred Language"));

		// Select Enroll in Plus Program?
		if (!getValue("Enroll in Plus Program").equals("N"))
			receiverContactInfoPage.check_receiverEnrollInPlusProgramChk();

		// Click on the Next button
		commonControls.click_nextBtn();

		// Select Photo ID: ID Type
		receiverIDsPage.select_selectPhotoIDType(getValue("Photo ID Type"));

		// Enter Photo ID: ID Number
		switch (getValue("Photo ID Type")) {
		case "Alien ID":
			receiverAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Photo ID Number", receiverAlienID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverAlienID);
			break;
		case "Drivers License":
			receiverDriversLicense = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Photo ID Number", receiverDriversLicense);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverDriversLicense);
			break;
		case "Government ID":
			receiverGovernmentID = "G" + BaseTest.generateRandomNumber(14);
			dataToBeWritten.put("Photo ID Number", receiverGovernmentID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverGovernmentID);
			break;
		case "Passport":
			receiverPassport = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(7);
			dataToBeWritten.put("Photo ID Number", receiverPassport);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverPassport);
			break;
		case "State ID":
			receiverStateID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Photo ID Number", receiverStateID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverStateID);
			break;
		}

		// Enter Photo ID: ID Issue Country
		receiverIDsPage.type_photoIDIssueCountryTxt(getValue("Photo ID Issue Country"));

		// Enter Photo ID: ID Issue State/Province
		receiverIDsPage.type_photoIDIssueStateOrProvinceTxt(getValue("Photo ID Issue StateOrProvince"));

		// Select Second form of ID: Secondary ID Type
		receiverIDsPage.select_selectSecondFormOfIDType(getValue("Second form of ID Type"));

		// Enter Second form of ID: Secondary ID Number
		switch (getValue("Second form of ID Type")) {
		case "Alien ID":
			receiverAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Second form of ID Number", receiverAlienID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverAlienID);
			break;
		case "International ID":
			receiverInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Second form of ID Number", receiverInternationalID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverInternationalID);
			break;
		case "Social Security Number":
			receiverSocialSecurityNumber = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Second form of ID Number", receiverSocialSecurityNumber);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverSocialSecurityNumber);
			break;
		case "Tax ID":
			receiverTaxID = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Second form of ID Number", receiverTaxID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverTaxID);
			break;
		}

		// Click on the Next button
		commonControls.click_nextBtn();

		// Select Third party
		switch (getValue("Third Party")) {
		case "None":
			thirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			thirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			thirdPartyInfoPage.type_thirdPartyReceiverOrganizationNameTxt(getValue("ThirdParty_OrganizationName"));

			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("ThirdParty_City"));
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter third party id type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			thirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			thirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("ThirdParty_Person_LastOrFamilyName", thirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonLastOrFamilyNameTxt(thirdPartyLastOrFamilyName);

			thirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("ThirdParty_Person_FirstOrGivenName", thirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonFirstOrGivenNameTxt(thirdPartyFirstOrGivenName);

			thirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("ThirdParty_Person_MiddleName", thirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonMiddleNameTxt(thirdPartyMiddleName);

			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("ThirdParty_City"));
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}

			thirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBYear", thirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBYear(thirdPartyDOBYear);

			thirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBMonth", thirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBMonth(thirdPartyDOBMonth);

			thirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBDay", thirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBDay(thirdPartyDOBDay);

			thirdPartyInfoPage.select_selectThirdPartyReceiverOccupation();
			break;
		}

		// Click on the Next button
		commonControls.click_nextBtn();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Payout button
		receiverInfoPage.click_payoutBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on Finished button
		receiverInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Received"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Received"),
				"The transaction status is not \"Received\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
