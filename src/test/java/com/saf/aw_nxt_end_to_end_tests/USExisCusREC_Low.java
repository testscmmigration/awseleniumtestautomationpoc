package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.ReceiveProfileFoundPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverProfileDetailsPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USExisCusREC_Low extends BaseTest {
	
	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USExisCusREC_Low() {
		super();
	}

	public USExisCusREC_Low(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USExisCusREC_Low(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusREC_Low_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControls = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		FindTranPage findTranPage = new FindTranPage(getDriver());
		ReceiveProfileFoundPage receiveProfileFoundPage = new ReceiveProfileFoundPage(getDriver());
		ReceiverProfileDetailsPage receiverProfileDetailsPage = new ReceiverProfileDetailsPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());		
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button
			loginPage.click_signIn();
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Receive Money button
		dashboardPage.click_receiveMoneyBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Select Sender Date of Birth: Year		
		findTranPage.select_selectDOBYear(getValue("Date of Birth Year"));

		// Select Sender Date of Birth: Month		
		findTranPage.select_selectDOBMonth(getValue("Date of Birth Month"));

		// Select Sender Date of Birth: Day		
		findTranPage.select_selectDOBDay(getValue("Date of Birth Day"));

		// Click on Next button
		findTranPage.click_nextBtn();

		// Select the receiver from the returned search result
		receiveProfileFoundPage.choose_prvReceiverSearchResultList(getValue("ReceiverName"));

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Take screen shot of the sender profile details page
		receiverProfileDetailsPage.takeScreenshot("Receiver Profile: 1");
		receiverProfileDetailsPage.scrollTo_otherLbl();

		// Click on the next button
		commonControls.click_nextBtn();

		// Click on the Next button
		commonControls.click_nextBtn();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Payout button
		receiverInfoPage.click_payoutBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on Finished button
		receiverInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTranPage.click_nextBtn();

		// Check whether the transaction status is "Received"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Received"),
				"The transaction status is not \"Received\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
