package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.HomePage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class Proprietary_and_confidential extends BaseTest {

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public Proprietary_and_confidential() {
		super();
	}

	public Proprietary_and_confidential(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new Proprietary_and_confidential(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void Proprietary_and_confidential_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		HomePage homePage = new HomePage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button
			loginPage.click_signIn();

			// Click on News header link
			dashboardPage.click_newsHeaderLnk();

			// Click on the Proprietary and confidential link on the agent portal
			dashboardPage.click_proprietaryandconfidentialLnk();
			
			//Click on the close button
			homePage.click_APProprietaryandConfidentialCloseBtn();
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Proprietary and confidential link on the AW.Nxt dash board
		dashboardPage.click_proprietaryandconfidentialLnk();
		
		//Click on the close button
		dashboardPage.click_AWProprietaryandConfidentialCloseBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
