package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.DeliveryOptionsPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.SendDestinationAmountPage;
import com.saf.aw_nxt_app_pages.SendFromPage;
import com.saf.aw_nxt_app_pages.SendToPage;
import com.saf.aw_nxt_app_pages.SenderContactInfoPage;
import com.saf.aw_nxt_app_pages.SenderIDsPage;
import com.saf.aw_nxt_app_pages.SenderProfileDetailsPage;
import com.saf.aw_nxt_app_pages.ThirdPartyInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USExisCus10MinSENExisRecvr_High extends BaseTest {

	String sendAmount, thirdPartyOrganizationName, thirdPartyLastOrFamilyName, thirdPartyFirstOrGivenName,
			thirdPartyMiddleName, thirdPartyAddress, thirdPartyDOBYear, thirdPartyDOBMonth, thirdPartyDOBDay,
			thirdPartyAlienID, thirdPartyInternationalID, thirdPartySocialSecurityNumber, thirdPartyTaxID;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USExisCus10MinSENExisRecvr_High() {
		super();
	}

	public USExisCus10MinSENExisRecvr_High(String testName, String browser,
			LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new USExisCus10MinSENExisRecvr_High(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USExisCus10MinSENExisRecvr_High_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		SendFromPage sendFromPage = new SendFromPage(getDriver());
		SenderProfileDetailsPage senderProfileDetailsPage = new SenderProfileDetailsPage(getDriver());
		SendToPage sendToPage = new SendToPage(getDriver());
		SendDestinationAmountPage sendDestinationAmount = new SendDestinationAmountPage(getDriver());
		DeliveryOptionsPage deliveryOptionsPage = new DeliveryOptionsPage(getDriver());
		SenderIDsPage senderIDsPage = new SenderIDsPage(getDriver());
		SenderContactInfoPage senderContactInfoPage = new SenderContactInfoPage(getDriver());
		ThirdPartyInfoPage thirdPartyInfoPage = new ThirdPartyInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Send Money button
		dashboardPage.click_sendMoneyBtn();

		// Search for existing sender profile
		if (getValue("SrchByLNDOB").equals("Y")) {
			sendFromPage.type_senderLastOrFamilyName2Txt(getValue("SenderLastOrFamilyName"));			
			sendFromPage.select_selectDOBYear(getValue("SenderDOBYear"));
			sendFromPage.select_selectDOBMonth(getValue("SenderDOBMonth"));
			sendFromPage.select_selectDOBDay(getValue("SenderDOBDay"));
		} else if (getValue("SrchByPHLN").equals("Y")) {
			sendFromPage.type_senderPhoneNumberTxt(getValue("SenderPhoneNumber"));
			sendFromPage.type_senderLastOrFamilyNameTxt(getValue("SenderLastOrFamilyName"));
		} else {
			sendFromPage.type_senderPlusNumberTxt(getValue("PlusNumber"));
		}

		// Click on next
		commonControlsPage.click_nextBtn();

		// Select the sender from the returned search result
		sendFromPage.choose_prvSenderSearchResultList("send", getValue("SenderName"));

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Take screen shot of the sender profile details page
		senderProfileDetailsPage.takeScreenshot("Sender Profile: 1");
		senderProfileDetailsPage.scrollTo_otherLbl();

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Select the receiver from the 'Previous Recipients'
		sendToPage.choose_prvReceiverSearchResultList("send", getValue("ReceiverName"));

		// Enter send amount
		sendAmount = generateRandomNumberWithinRange(senMaxRangeA, senMaxRangeB);
		dataToBeWritten.put("SendAmount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		sendDestinationAmount.type_sendAmountTxt(sendAmount);

		// Select currency
		sendDestinationAmount.select_selectCurrency(getValue("Currency"));

		// Select FeeType
		sendDestinationAmount.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmount.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmount.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Enter promo code1
		if (!getValue("PromoCode1").equals(""))
			sendDestinationAmount.type_promoCode1Txt(getValue("PromoCode1"));

		// Enter promo code2
		if (!getValue("PromoCode2").equals(""))
			sendDestinationAmount.type_promoCode2Txt(getValue("PromoCode2"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the Cash Options
		deliveryOptionsPage.click_cashOptionsLink();

		// Select delivery Option
		deliveryOptionsPage.click_min10DlvrOpt();

		// Select Photo ID: ID Choice
		senderIDsPage.select_selectPhotoIDChoice(getValue("SenderPrimaryIDChoice"));

		// Enter Photo ID: Last 4 digits of ID
		senderIDsPage.type_photoIDLast4digitsofIDTxt(getValue("Last4DigitsOfSenderPrimaryIDNumber"));

		// Select Second form of ID: Secondary ID Choice
		senderIDsPage.select_selectSecondFormOfIDChoice(getValue("SenderSecondaryIDChoice"));

		// Enter Second form of ID: Last 4 digits of ID
		senderIDsPage.type_secondFormOfIDLast4digitsofIDTxt(getValue("Last4DigitsOfSenderSecondaryIDNumber"));

		// Click on the Next button on the sender ids page
		commonControlsPage.click_nextBtn();

		// Take screen shot of the sender contact information page
		senderContactInfoPage.takeScreenshot("Sender Contact Information: 1");
		senderContactInfoPage.scrollTo_moneygramPlusNumberLbl();

		// Click on the Next button on the sender contact information page
		commonControlsPage.click_nextBtn();

		// Select Third party
		switch (getValue("Third Party")) {
		case "None":
			// Choose Third Party type as 'None'
			thirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			// Choose Third Party type as 'Organization'
			thirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			// Enter Organization Name
			thirdPartyInfoPage.type_thirdPartySenderOrganizationNameTxt(getValue("ThirdParty_OrganizationName"));

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderAddressTxt(thirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("ThirdParty_City"));

			// Enter State.Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter third party id type
			thirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			// Choose Third Party type as 'Person'
			thirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			// Enter Last/Family Name
			thirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("ThirdParty_Person_LastOrFamilyName", thirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonLastOrFamilyNameTxt(thirdPartyLastOrFamilyName);

			// Enter First/Given Name
			thirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("ThirdParty_Person_FirstOrGivenName", thirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonFirstOrGivenNameTxt(thirdPartyFirstOrGivenName);

			// Enter Middle Name
			thirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("ThirdParty_Person_MiddleName", thirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonMiddleNameTxt(thirdPartyMiddleName);

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("ThirdParty_Country"));

			// Enter Address
			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("ThirdParty_Address");
			dataToBeWritten.put("ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderAddressTxt(thirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("ThirdParty_City"));

			// Enter State/Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			thirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(thirdPartyTaxID);
				break;
			}

			// Select Date of Birth: Year
			thirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBYear", thirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBYear(thirdPartyDOBYear);

			// Select Date of Birth: Month
			thirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBMonth", thirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBMonth(thirdPartyDOBMonth);

			// Select Date of Birth: Day
			thirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("ThirdParty_Person_DOBDay", thirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBDay(thirdPartyDOBDay);

			// Select Occupation
			thirdPartyInfoPage.select_selectThirdPartySenderOccupation();
			dataToBeWritten.put("ThirdParty_Person_Occupation",
					thirdPartyInfoPage.get_selectedThirdPartySenderOccupation());
			addDataToOutputExcel(dataToBeWritten);

			break;
		}

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Next button on the third party information page
		commonControlsPage.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on the send money button
		commonControlsPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = commonControlsPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		commonControlsPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Available"),
				"The transaction is not in \"Availble\" status.");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
