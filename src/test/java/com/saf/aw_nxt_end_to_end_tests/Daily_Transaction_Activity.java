package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.DailyTranReportsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class Daily_Transaction_Activity extends BaseTest {

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public Daily_Transaction_Activity() {
		super();
	}

	public Daily_Transaction_Activity(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new Daily_Transaction_Activity(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void Daily_Transaction_Activity_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		DailyTranReportsPage dailyTranReportsPage = new DailyTranReportsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Mouse over on the Reports header
		dashboardPage.mouseOver_reportsHeaderLnk();

		// Click on the Daily Transaction Activity link
		dashboardPage.click_dailyTransactionActivityLnk();

		// Select 'Select report' dropdown
		dailyTranReportsPage.select_selectReport(getValue("SelectReport"));

		// Enter 'Select activity date'
		dailyTranReportsPage.type_selectActivityDateTxt(getValue("SelectActivityDate"));

		// Select POS(s)
		dailyTranReportsPage.choose_pOSList(getValue("POSs"));

		// Click on 'Run Report' button
		dailyTranReportsPage.click_runReportBtn();

		// String reportsFolder =
		// curProj+"/"+"Reports"+"/"+this.getClass().getSimpleName()+"_"+dailyTranReportsPath+dateTime;
		// System.out.println(reportsFolder);
		// createDirectories(reportsFolder);

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
