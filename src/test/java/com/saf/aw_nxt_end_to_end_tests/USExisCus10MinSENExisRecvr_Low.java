package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.DeliveryOptionsPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.SendDestinationAmountPage;
import com.saf.aw_nxt_app_pages.SendFromPage;
import com.saf.aw_nxt_app_pages.SendToPage;
import com.saf.aw_nxt_app_pages.SenderProfileDetailsPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USExisCus10MinSENExisRecvr_Low extends BaseTest {

	String sendAmount;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USExisCus10MinSENExisRecvr_Low() {
		super();
	}

	public USExisCus10MinSENExisRecvr_Low(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] { new USExisCus10MinSENExisRecvr_Low(this.getClass().getSimpleName(),
				mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USExisCus10MinSENExisRecvr_Low_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		SendFromPage sendFromPage = new SendFromPage(getDriver());
		SenderProfileDetailsPage senderProfileDetailsPage = new SenderProfileDetailsPage(getDriver());
		SendToPage sendToPage = new SendToPage(getDriver());
		SendDestinationAmountPage sendDestinationAmountPage = new SendDestinationAmountPage(getDriver());
		DeliveryOptionsPage deliveryOptionsPage = new DeliveryOptionsPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTranPage = new FindTranPage(getDriver());
		TranDetailsPage tranDetailsPage = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		// Wait for page to load completely
		// launchPage.waitForPageToLoad();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Send Money button
		dashboardPage.click_sendMoneyBtn();

		// Search for existing sender profile
		if (getValue("SrchByLNDOB").equals("Y")) {
			sendFromPage.type_senderLastOrFamilyName2Txt(getValue("SenderLastOrFamilyName"));			
			sendFromPage.select_selectDOBYear(getValue("SenderDOBYear"));
			sendFromPage.select_selectDOBMonth(getValue("SenderDOBMonth"));
			sendFromPage.select_selectDOBDay(getValue("SenderDOBDay"));
		} else if (getValue("SrchByPHLN").equals("Y")) {
			sendFromPage.type_senderPhoneNumberTxt(getValue("SenderPhoneNumber"));
			sendFromPage.type_senderLastOrFamilyNameTxt(getValue("SenderLastOrFamilyName"));
		} else {
			sendFromPage.type_senderPlusNumberTxt(getValue("PlusNumber"));
		}

		// Click on next
		commonControlsPage.click_nextBtn();

		// Select the sender from the returned search result
		sendFromPage.choose_prvSenderSearchResultList("send", getValue("SenderName"));

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Take screen shot of the sender profile details page
		senderProfileDetailsPage.takeScreenshot("Sender Profile: 1");
		senderProfileDetailsPage.scrollTo_otherLbl();

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Select the receiver from the 'Previous Recipients'
		sendToPage.choose_prvReceiverSearchResultList("send", getValue("ReceiverName"));

		// Enter send amount
		sendAmount = generateRandomNumberWithinRange(senMinRangeA, senMinRangeB);
		dataToBeWritten.put("SendAmount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		sendDestinationAmountPage.type_sendAmountTxt(sendAmount);

		// Select currency
		sendDestinationAmountPage.select_selectCurrency(getValue("Currency"));

		// Select fee type
		sendDestinationAmountPage.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmountPage.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmountPage.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Enter promo code1
		if (!getValue("PromoCode1").equals(""))
			sendDestinationAmountPage.type_promoCode1Txt(getValue("PromoCode1"));

		// Enter promo code2
		if (!getValue("PromoCode2").equals(""))
			sendDestinationAmountPage.type_promoCode2Txt(getValue("PromoCode2"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the Cash Options
		deliveryOptionsPage.click_cashOptionsLink();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Select delivery Option
		deliveryOptionsPage.click_min10DlvrOpt();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on the send money button
		commonControlsPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = commonControlsPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		commonControlsPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTranPage.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTranPage.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetailsPage.getTransactionStatus().contains("Available"),
				"The transaction is not in \"Availble\" status.");

		// Click on the Finish button
		tranDetailsPage.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
