package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import jxl.read.biff.BiffException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.ReceiverContactInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

public class USNewCusREC_Low extends BaseTest {

	String DOBYear, DOBMonth, DOBDay, receiverAddress, receiverAddressLine2, receiverAddressLine3, receiverPrimaryPhone;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public USNewCusREC_Low() {
		super();
	}

	public USNewCusREC_Low(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new USNewCusREC_Low(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void USNewCusREC_Low_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControls = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());
		ReceiverContactInfoPage receiverContactInfoPage = new ReceiverContactInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Receive Money button
		dashboardPage.click_receiveMoneyBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Select Sender Date of Birth: Year
		DOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("Date of Birth Year", DOBYear);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBYear(DOBYear);

		// Select Sender Date of Birth: Month
		DOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("Date of Birth Month", DOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBMonth(DOBMonth);

		// Select Sender Date of Birth: Day
		DOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("Date of Birth Day", DOBDay);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBDay(DOBDay);

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Next button
		commonControls.click_nextBtn();

		// Select Receiver Title
		receiverInfoPage.select_selectReceiverTitle();
		dataToBeWritten.put("Title", receiverInfoPage.get_selectedReceiverTitle());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Birth Country
		receiverInfoPage.type_receiverBirthCountryTxt(getValue("Birth Country"));

		// Enter Country
		receiverInfoPage.type_receiverCountryTxt(getValue("Country"));

		// Enter Sender Address
		receiverAddress = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Address", receiverAddress);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressTxt(receiverAddress);

		// Enter Sender Address Line 2
		receiverAddressLine2 = getValue("Address Line 2");
		receiverInfoPage.type_receiverAddressLine2Txt(receiverAddressLine2);

		// Enter Sender Address Line 3
		receiverAddressLine3 = "Apt# " + BaseTest.generateRandomNumber(3);
		dataToBeWritten.put("Address Line 3", receiverAddressLine3);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressLine3Txt(receiverAddressLine3);

		// Enter City
		receiverInfoPage.type_receiverCityTxt(getValue("City"));

		// Enter State/Province
		receiverInfoPage.type_receiverStateOrProvinceTxt(getValue("State Or Province"));

		// Enter Postal Code
		receiverInfoPage.type_receiverPostalCodeTxt(getValue("Postal Code"));

		// Click on Next button
		commonControls.click_nextBtn();

		// Enter Receiver primary phone country code
		receiverContactInfoPage.type_receiverPrimaryPhoneCountryCodeTxt(getValue("Primary Phone Country Code"));
		receiverContactInfoPage.selectRequiredCountry(getValue("Primary Phone Country Name"));

		// Enter Receiver Primary Phone Number
		receiverPrimaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Primary Phone Number", receiverPrimaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		receiverContactInfoPage.type_receiverPrimaryPhoneTxt(receiverPrimaryPhone);

		// Receiver Mobile Phone
		if (!getValue("Mobile Phone").equals("N"))
			receiverContactInfoPage.check_receiverMobilePhoneChk();

		// Enter Receiver Email
		if (!getValue("Email").equals(""))
			receiverContactInfoPage.type_receiverEmailTxt(getValue("Email"));

		// Select Receive Transaction Status
		receiverContactInfoPage.select_selectReceiverReceiveTransactionStatus(getValue("Receive Transaction Status"));

		// Select Receive Offers
		receiverContactInfoPage.select_selectReceiverReceiveOffers(getValue("Receive Offers"));

		// Select Preferred Language
		receiverContactInfoPage.select_selectReceiverPreferredLanguage(getValue("Preferred Language"));

		// Select Enroll in Plus Program?
		if (!getValue("Enroll in Plus Program").equals("N"))
			receiverContactInfoPage.check_receiverEnrollInPlusProgramChk();

		// Click on the Next button
		commonControls.click_nextBtn();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Payout button
		receiverInfoPage.click_payoutBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on Finished button
		receiverInfoPage.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Received"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Received"),
				"The transaction status is not \"Received\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
