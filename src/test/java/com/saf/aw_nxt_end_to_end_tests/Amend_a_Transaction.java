package com.saf.aw_nxt_end_to_end_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.AmendTransactionDetailsPage;
import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.LoginPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class Amend_a_Transaction extends BaseTest {

	String amendedReceiverFirsOrGivenName, amendedReceiverMiddleName, amendedReceiverLastOrFamilyName,
			amendedReceiverSecondLastOrFamilyName;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public Amend_a_Transaction() {
		super();
	}

	public Amend_a_Transaction(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new Amend_a_Transaction(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void Amend_a_Transaction_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControls = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());
		LoginPage loginPage = new LoginPage(getDriver());
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());
		AmendTransactionDetailsPage amendTransactionDetails = new AmendTransactionDetailsPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		if (!BaseTest.usefakeauthUrl.equals("Y")) {
			// Enter User ID
			loginPage.type_userIDTxt(getValue("UserID"));

			// Enter Password		
			loginPage.type_passwordTxt(getValue("Password"));

			// Click on Sign In button		
			loginPage.click_signIn();			
		}

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Select Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("SelectAgentPOS"));

		// Click on the Edit Transfer button
		dashboardPage.click_editTransferBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Amend button
		tranDetails.click_amendBtn();

		// Enter Receiver First/Given Name
		amendedReceiverFirsOrGivenName = generateRandomReceiverFirstName();
		dataToBeWritten.put("FirstorGivenName", amendedReceiverFirsOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_firstOrGivenNameTxt(amendedReceiverFirsOrGivenName);

		// Enter Receiver Middle Name
		amendedReceiverMiddleName = generateRandomReceiverMiddleName();
		dataToBeWritten.put("MiddleName", amendedReceiverMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_middleNameTxt(amendedReceiverMiddleName);

		// Enter Receiver Last/Family Name
		amendedReceiverLastOrFamilyName = generateRandomReceiverLastName();
		dataToBeWritten.put("LastorFamilyName", amendedReceiverLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_lastOrFamilyNameTxt(amendedReceiverLastOrFamilyName);

		// Enter Receiver Second Last/Family Name
		amendedReceiverSecondLastOrFamilyName = generateRandomReceiverSecondLastName();
		dataToBeWritten.put("SecondLastorFamilyName", amendedReceiverSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_secondLastOrFamilyTxt(amendedReceiverSecondLastOrFamilyName);

		// Click on the Next button
		commonControls.click_nextBtn();

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on 'Yes, continue' button
		amendTransactionDetails.click_yesContinueBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on 'Finished' button
		amendTransactionDetails.click_finishedBtn();

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Available"),
				"The transaction status is not \"Availble\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// Click on the Logout button
		if (!BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.logout();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
