package com.saf.aw_nxt_sanity_tests;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.saf.aw_nxt_app_pages.AmendTransactionDetailsPage;
import com.saf.aw_nxt_app_pages.ClosePrintWindow;
import com.saf.aw_nxt_app_pages.CloseReceiptsWindowPage;
import com.saf.aw_nxt_app_pages.CommonControlsPage;
import com.saf.aw_nxt_app_pages.DashboardPage;
import com.saf.aw_nxt_app_pages.DeliveryOptionsPage;
import com.saf.aw_nxt_app_pages.FindTranPage;
import com.saf.aw_nxt_app_pages.LaunchApplicationUrl;
import com.saf.aw_nxt_app_pages.ReceiverContactInfoPage;
import com.saf.aw_nxt_app_pages.ReceiverIDsPage;
import com.saf.aw_nxt_app_pages.ReceiverInfoPage;
import com.saf.aw_nxt_app_pages.SendDestinationAmountPage;
import com.saf.aw_nxt_app_pages.SendFromPage;
import com.saf.aw_nxt_app_pages.SendReversalTransactionDetailsPage;
import com.saf.aw_nxt_app_pages.SenderContactInfoPage;
import com.saf.aw_nxt_app_pages.SenderIDsPage;
import com.saf.aw_nxt_app_pages.SenderInfoPage;
import com.saf.aw_nxt_app_pages.ThirdPartyInfoPage;
import com.saf.aw_nxt_app_pages.TranDetailsPage;
import com.saf.base.BaseTest;
import com.saf.exceptions.DataSheetException;
import com.saf.exceptions.InvalidBrowserException;

import jxl.read.biff.BiffException;

public class AW_Nxt_Sanity_Suite extends BaseTest {

	String sendAmount, senderFirstOrGivenName, senderMiddleName, senderLastOrFamilyName, senderSecondLastOrFamilyName,
			senderAddress, senderAddressLine2, senderAddressLine3, senderAlienID, senderDriversLicense,
			senderGovernmentID, senderPassport, senderStateID, senderInternationalID, senderSocialSecurityNumber,
			senderTaxID, senderPrimaryPhone, senderDOBYear, senderDOBMonth, senderDOBDay, receiverFirsOrGivenName,
			receiverMiddleName, receiverLastOrFamilyName, receiverSecondLastOrFamilyName,
			senderThirdPartyOrganizationName, senderThirdPartyLastOrFamilyName, senderThirdPartyFirstOrGivenName,
			senderThirdPartyMiddleName, senderThirdPartyAddress, senderThirdPartyDOBYear, senderThirdPartyDOBMonth,
			senderThirdPartyDOBDay, senderThirdPartyAlienID, senderThirdPartyInternationalID,
			senderThirdPartySocialSecurityNumber, senderThirdPartyTaxID, amendedReceiverFirsOrGivenName,
			amendedReceiverMiddleName, amendedReceiverLastOrFamilyName, amendedReceiverSecondLastOrFamilyName,
			receiverDOBYear, receiverDOBMonth, receiverDOBDay, receiverOccupation, receiverAddress,
			receiverAddressLine2, receiverAddressLine3, receiverPrimaryPhone, receiverAlienID, receiverDriversLicense,
			receiverGovernmentID, receiverPassport, receiverStateID, receiverInternationalID,
			receiverSocialSecurityNumber, receiverTaxID, thirdPartyAddress, thirdPartyAlienID,
			thirdPartyInternationalID, thirdPartySocialSecurityNumber, thirdPartyTaxID, thirdPartyLastOrFamilyName,
			thirdPartyFirstOrGivenName, thirdPartyMiddleName, thirdPartyDOBYear, thirdPartyDOBMonth, thirdPartyDOBDay,
			thirdPartyOccupation;

	LinkedHashMap<String, String> dataToBeWritten = new LinkedHashMap<String, String>();

	// Call the parent constructor
	public AW_Nxt_Sanity_Suite() {
		super();
	}

	public AW_Nxt_Sanity_Suite(String testName, String browser, LinkedHashMap<String, String> mapDataSheet) {
		super(testName, browser, mapDataSheet);
	}

	@Factory(dataProvider = "dataSheet")
	public Object[] testCreator(LinkedHashMap<String, String> mapDataSheet) {
		return new Object[] {
				new AW_Nxt_Sanity_Suite(this.getClass().getSimpleName(), mapDataSheet.get("Browser"), mapDataSheet) };
	}

	@DataProvider(name = "dataSheet")
	public Object[][] getTestData() throws BiffException, IOException, InvalidBrowserException, DataSheetException,
			ArrayIndexOutOfBoundsException {
		return testDataProvider.getTestDataFromExcel(inputDataSheetPath, this.getClass().getSimpleName());
	}

	@Test
	public void AW_Nxt_Sanity_Suite_TEST() throws Exception {

		dataToBeWritten.put("status", "Fail");
		addDataToOutputExcel(dataToBeWritten);

		// Pages needed
		CommonControlsPage commonControlsPage = new CommonControlsPage(getDriver());
		LaunchApplicationUrl launchPage = new LaunchApplicationUrl(getDriver());		
		DashboardPage dashboardPage = new DashboardPage(getDriver());
		SendFromPage sendFromPage = new SendFromPage(getDriver());
		SendDestinationAmountPage sendDestinationAmountPage = new SendDestinationAmountPage(getDriver());
		DeliveryOptionsPage deliveryOptionsPage = new DeliveryOptionsPage(getDriver());
		SenderInfoPage senderInfoPage = new SenderInfoPage(getDriver());
		SenderIDsPage senderIDsPage = new SenderIDsPage(getDriver());
		SenderContactInfoPage senderContactInfoPage = new SenderContactInfoPage(getDriver());
		ReceiverInfoPage receiverInfoPage = new ReceiverInfoPage(getDriver());
		ReceiverContactInfoPage receiverContactInfoPage = new ReceiverContactInfoPage(getDriver());
		ReceiverIDsPage receiverIDsPage = new ReceiverIDsPage(getDriver());
		ThirdPartyInfoPage thirdPartyInfoPage = new ThirdPartyInfoPage(getDriver());
		ClosePrintWindow closePrintWindow = new ClosePrintWindow(getDriver());
		CloseReceiptsWindowPage closeReceiptsWindowPage = new CloseReceiptsWindowPage(getDriver());
		FindTranPage findTran = new FindTranPage(getDriver());
		TranDetailsPage tranDetails = new TranDetailsPage(getDriver());
		AmendTransactionDetailsPage amendTransactionDetails = new AmendTransactionDetailsPage(getDriver());
		SendReversalTransactionDetailsPage sendReversalTransactionDetails = new SendReversalTransactionDetailsPage(
				getDriver());

		// Load the application URL
		launchPage.launchBasePage();

		// ***************************************************Login***************************************************

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// ***************************************************Select Sender
		// Agent***************************************************

		// Select Sender Agent/POS
		dashboardPage.select_selectAgentPOS(getValue("Sender_AgentPOS"));

		// Check whether the Send Money button is present
		Assert.assertTrue(dashboardPage.sendMoneyBtn_Presence(), "'SendMoney' button is absent on the dashboard page.");
		dashboardPage.takeScreenshot("AW.Nxt dashboard.");

		// ***************************************************Clear
		// Cache***************************************************

		// Click on the Version link
		dashboardPage.click_versionLnk();

		// Click on Clear cache button
		dashboardPage.click_clearCacheBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Click on Close
		dashboardPage.click_closeBtn();

		// ***************************************************Send***************************************************

		// Click on the Send Money button
		dashboardPage.click_sendMoneyBtn();

		// Click on the New customer link
		sendFromPage.click_newCustomerLink();

		// Enter send amount
		sendAmount = generateRandomNumberWithinRange(senMaxRangeA, senMaxRangeB);
		dataToBeWritten.put("SendAmount", sendAmount);
		addDataToOutputExcel(dataToBeWritten);
		sendDestinationAmountPage.type_sendAmountTxt(sendAmount);

		// Select currency
		sendDestinationAmountPage.select_selectCurrency(getValue("Currency"));

		// Select fee type
		sendDestinationAmountPage.select_selectFeeType(getValue("FeeType"));

		// Enter destination country
		sendDestinationAmountPage.type_destinationCountryTxt(getValue("DestinationCountry"));

		// Enter destination state/province
		if (!getValue("DestinationStateOrProvince").equals(""))
			sendDestinationAmountPage.type_destinationStateOrProvinceTxt(getValue("DestinationStateOrProvince"));

		// Enter promo code1
		if (!getValue("PromoCode1").equals(""))
			sendDestinationAmountPage.type_promoCode1Txt(getValue("PromoCode1"));

		// Enter promo code2
		if (!getValue("PromoCode2").equals(""))
			sendDestinationAmountPage.type_promoCode2Txt(getValue("PromoCode2"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Click on the Cash Options
		deliveryOptionsPage.click_cashOptionsLink();

		// Select delivery Option
		deliveryOptionsPage.click_min10DlvrOpt();

		// Select Sender Title
		senderInfoPage.select_selectSenderTitle();
		dataToBeWritten.put("Sender_Title", senderInfoPage.get_selectedSenderTitle());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Sender First/Given Name
		senderFirstOrGivenName = generateRandomSenderFirstName();
		dataToBeWritten.put("Sender_FirstOrGiven Name", senderFirstOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderFirstOrGivenNameTxt(senderFirstOrGivenName);

		// Enter Sender Middle Name
		senderMiddleName = generateRandomSenderMiddleName();
		dataToBeWritten.put("Sender_Middle Name", senderMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderMiddleNameTxt(senderMiddleName);

		// Enter Sender Last/Family Name
		senderLastOrFamilyName = generateRandomSenderLastName();
		dataToBeWritten.put("Sender_LastOrFamily Name", senderLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderLastOrFamilyNameTxt(senderLastOrFamilyName);

		// Enter Sender Second Last/Family Name
		senderSecondLastOrFamilyName = generateRandomSenderSecondLastName();
		dataToBeWritten.put("Sender_Second LastOrFamily Name", senderSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderSecondLastOrFamilyNameTxt(senderSecondLastOrFamilyName);

		// Select Sender Suffix
		senderInfoPage.select_selectSenderNameSuffix();
		dataToBeWritten.put("Sender_Suffix", senderInfoPage.get_selectedSenderNameSuffix());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Sender Country
		senderInfoPage.type_senderCountryTxt(getValue("Sender_Country"));

		// Enter Sender Address
		senderAddress = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Sender_Address", senderAddress);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderAddressTxt(senderAddress);

		// Enter Sender Address Line 2
		senderAddressLine2 = getValue("Sender_AddressLine2");
		senderInfoPage.type_senderAddressLine2Txt(senderAddressLine2);

		// Enter Sender Address Line 3
		senderAddressLine3 = "Apt# " + BaseTest.generateRandomNumber(3);
		dataToBeWritten.put("Sender_AddressLine3", senderAddressLine3);
		addDataToOutputExcel(dataToBeWritten);
		senderInfoPage.type_senderAddressLine3Txt(senderAddressLine3);

		// Enter Sender City
		senderInfoPage.type_senderCityTxt(getValue("Sender_City"));

		// Enter Sender State/Province
		senderInfoPage.type_senderStateOrProvinceTxt(getValue("Sender_StateOrProvince"));

		// Enter Sender Postal Code
		senderInfoPage.type_senderPostalCodeTxt(getValue("Sender_Postal Code"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Select Photo ID: ID Type
		senderIDsPage.select_selectPhotoIDType(getValue("Sender_PrimaryIDType"));

		// Enter Photo ID: ID Number
		switch (getValue("Sender_PrimaryIDType")) {
		case "Alien ID":
			senderAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Sender_PrimaryIDNumber", senderAlienID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_photoIDNumberTxt(senderAlienID);
			break;
		case "Drivers License":
			senderDriversLicense = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Sender_PrimaryIDNumber", senderDriversLicense);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_photoIDNumberTxt(senderDriversLicense);
			break;
		case "Government ID":
			senderGovernmentID = "G" + BaseTest.generateRandomNumber(14);
			dataToBeWritten.put("Sender_PrimaryIDNumber", senderGovernmentID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_photoIDNumberTxt(senderGovernmentID);
			break;
		case "Passport":
			senderPassport = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(7);
			dataToBeWritten.put("Sender_PrimaryIDNumber", senderPassport);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_photoIDNumberTxt(senderPassport);
			break;
		case "State ID":
			senderStateID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Sender_PrimaryIDNumber", senderStateID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_photoIDNumberTxt(senderStateID);
			break;
		}

		// Enter Photo ID: ID Issue Country
		senderIDsPage.type_photoIDIssueCountryTxt(getValue("Sender_PrimaryIDIssueCountry"));

		// Enter Photo ID: ID Issue State/Province
		senderIDsPage.type_photoIDIssueStateOrProvinceTxt(getValue("Sender_PrimaryIDIssueStateOrProvince"));

		// Select Second form of ID: Secondary ID Type
		senderIDsPage.select_selectSecondFormOfIDType(getValue("Sender_SecondaryIDType"));

		// Enter Second form of ID: Secondary ID Number
		switch (getValue("Sender_SecondaryIDType")) {
		case "Alien ID":
			senderAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Sender_SecondaryIDNumber", senderAlienID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_secondFormOfIDNumberTxt(senderAlienID);
			break;
		case "International ID":
			senderInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Sender_SecondaryIDNumber", senderInternationalID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_secondFormOfIDNumberTxt(senderInternationalID);
			break;
		case "Social Security Number":
			senderSocialSecurityNumber = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Sender_SecondaryIDNumber", senderSocialSecurityNumber);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_secondFormOfIDNumberTxt(senderSocialSecurityNumber);
			break;
		case "Tax ID":
			senderTaxID = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Sender_SecondaryIDNumber", senderTaxID);
			addDataToOutputExcel(dataToBeWritten);
			senderIDsPage.type_secondFormOfIDNumberTxt(senderTaxID);
			break;
		}

		// Select Sender Date of Birth: Year
		senderDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("Sender_DOBYear", senderDOBYear);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBYear(senderDOBYear);

		// Select Sender Date of Birth: Month
		senderDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("Sender_DOBMonth", senderDOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBMonth(senderDOBMonth);

		// Select Sender Date of Birth: Day
		senderDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("Sender_DOBDay", senderDOBDay);
		addDataToOutputExcel(dataToBeWritten);
		senderIDsPage.select_selectDOBDay(senderDOBDay);

		// Enter Sender Birth Country
		senderIDsPage.type_birthCountryTxt(getValue("Sender_BirthCountry"));

		// Enter Occupation
		senderIDsPage.select_selectOccupation();
		dataToBeWritten.put("Sender_Occupation", senderIDsPage.get_selectedOccupation());
		addDataToOutputExcel(dataToBeWritten);

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Enter Sender primary phone country code
		senderContactInfoPage.type_primaryPhoneCountryCodeTxt(getValue("Sender_Primary Phone Country Code"));
		senderContactInfoPage.selectRequiredCountry(getValue("Sender_Primary Phone Country Name"));

		// Enter Sender Primary Phone Number
		senderPrimaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Sender_Primary Phone Number", senderPrimaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		senderContactInfoPage.type_senderPrimaryPhoneTxt(senderPrimaryPhone);

		// Select Mobile Phone
		if (!getValue("Seneder_Mobile Phone").equals("N"))
			senderContactInfoPage.check_senderMobilePhoneChk();

		// Type Email
		if (!getValue("Sender_Email").equals(""))
			senderContactInfoPage.type_senderEmailTxt(getValue("Sender_Email"));

		// Select Receive Transaction Status
		senderContactInfoPage
				.select_selectSenderReceiveTransactionStatus(getValue("Sender_Receive Transaction Status"));

		// Select Receive Offers
		senderContactInfoPage.select_selectSenderReceiveOffers(getValue("Sender_Receive Offers"));

		// Select Preferred Language
		senderContactInfoPage.select_selectSenderPreferredLanguage(getValue("Sender_Preferred Language"));

		// Select Enroll in Plus Program?
		if (!getValue("Sender_Enroll in Plus Program").equals("N"))
			senderContactInfoPage.check_senderEnrollInPlusProgramChk();

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Enter Receiver First/Given Name
		receiverFirsOrGivenName = generateRandomReceiverFirstName();
		dataToBeWritten.put("Receiver_First/Given Name", receiverFirsOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverFirsOrGivenNameTxt(receiverFirsOrGivenName);

		// Enter Receiver Middle Name
		receiverMiddleName = generateRandomReceiverMiddleName();
		dataToBeWritten.put("Receiver_Middle Name", receiverMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverMiddleNameTxt(receiverMiddleName);

		// Enter Receiver Last/Family Name
		receiverLastOrFamilyName = generateRandomReceiverLastName();
		dataToBeWritten.put("Receiver_LastOrFamily Name", receiverLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverLastOrFamilyNameTxt(receiverLastOrFamilyName);

		// Enter Receiver Second Last/Family Name
		receiverSecondLastOrFamilyName = generateRandomReceiverSecondLastName();
		dataToBeWritten.put("Receiver_Second LastOrFamily Name", receiverSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverSecondLastOrFamilyNameTxt(receiverSecondLastOrFamilyName);

		// Select Receiver Suffix
		receiverInfoPage.select_selectReceiverSuffix();
		dataToBeWritten.put("Receiver_Suffix", receiverInfoPage.get_selectedReceiverSuffix());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Receiver Email
		if (!getValue("Receiver_Email").equals(""))
			receiverContactInfoPage.type_receiverEmailTxt(getValue("Receiver_Email"));

		// Enter Message Field 1
		if (!getValue("Message Field 1").equals(""))
			receiverInfoPage.type_messageField1Txt(getValue("Message Field 1"));

		// Enter Message Field 2
		if (!getValue("Message Field 2").equals(""))
			receiverInfoPage.type_messageField2Txt(getValue("Message Field 2"));

		// Enter Test Question
		if (!getValue("Test Question").equals(""))
			receiverInfoPage.type_testQuestionTxt(getValue("Test Question"));

		// Enter Test Answer
		if (!getValue("Test Answer").equals(""))
			receiverInfoPage.type_testAnswerTxt(getValue("Test Answer"));

		// Click on the next button
		commonControlsPage.click_nextBtn();

		// Select Third party
		switch (getValue("Sender_Third Party")) {
		case "None":
			// Choose Third Party type as 'None'
			thirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			// Choose Third Party type as 'Organization'
			thirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			// Enter Organization Name
			thirdPartyInfoPage.type_thirdPartySenderOrganizationNameTxt(getValue("Sender_ThirdParty_OrganizationName"));

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("Sender_ThirdParty_Country"));

			// Enter Address
			senderThirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("Sender_ThirdParty_Address");
			dataToBeWritten.put("Sender_ThirdParty_Address", senderThirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderAddressTxt(senderThirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("Sender_ThirdParty_City"));

			// Enter State.Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("Sender_ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("Sender_ThirdParty_PostalCode"));

			// Enter third party id type
			thirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("Sender_ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("Sender_ThirdParty_IDType")) {
			case "Alien ID":
				senderThirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyAlienID);
				break;
			case "International ID":
				senderThirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyInternationalID);
				break;
			case "Social Security Number":
				senderThirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				senderThirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			// Choose Third Party type as 'Person'
			thirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			// Enter Last/Family Name
			senderThirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("Sender_ThirdParty_Person_LastOrFamilyName", senderThirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonLastOrFamilyNameTxt(senderThirdPartyLastOrFamilyName);

			// Enter First/Given Name
			senderThirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("Sender_ThirdParty_Person_FirstOrGivenName", senderThirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonFirstOrGivenNameTxt(senderThirdPartyFirstOrGivenName);

			// Enter Middle Name
			senderThirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("Sender_ThirdParty_Person_MiddleName", senderThirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderPersonMiddleNameTxt(senderThirdPartyMiddleName);

			// Enter Country
			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("Sender_ThirdParty_Country"));

			// Enter Address
			senderThirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("Sender_ThirdParty_Address");
			dataToBeWritten.put("Sender_ThirdParty_Address", senderThirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartySenderAddressTxt(senderThirdPartyAddress);

			// Enter City
			thirdPartyInfoPage.type_thirdPartySenderCityTxt(getValue("Sender_ThirdParty_City"));

			// Enter State/Province
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("Sender_ThirdParty_State"));

			// Enter Postal Code
			thirdPartyInfoPage.type_thirdPartySenderPostalCodeTxt(getValue("Sender_ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			thirdPartyInfoPage.select_selectThirdPartySenderIDType(getValue("Sender_ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("Sender_ThirdParty_IDType")) {
			case "Alien ID":
				senderThirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyAlienID);
				break;
			case "International ID":
				senderThirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyInternationalID);
				break;
			case "Social Security Number":
				senderThirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				senderThirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Sender_ThirdParty_IDNumber", senderThirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartySenderIDNumberTxt(senderThirdPartyTaxID);
				break;
			}

			// Select Date of Birth: Year
			senderThirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("Sender_ThirdParty_Person_DOBYear", senderThirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBYear(senderThirdPartyDOBYear);

			// Select Date of Birth: Month
			senderThirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("Sender_ThirdParty_Person_DOBMonth", senderThirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBMonth(senderThirdPartyDOBMonth);

			// Select Date of Birth: Day
			senderThirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("Sender_ThirdParty_Person_DOBDay", senderThirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartySenderDOBDay(senderThirdPartyDOBDay);

			// Select Occupation
			thirdPartyInfoPage.select_selectThirdPartySenderOccupation();
			dataToBeWritten.put("Sender_ThirdParty_Person_Occupation",
					thirdPartyInfoPage.get_selectedThirdPartySenderOccupation());
			addDataToOutputExcel(dataToBeWritten);

			break;
		}

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the next button on the third party information page
		commonControlsPage.click_nextBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		Set<String> winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Pre-Payment receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Pre-Payment receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		if (getValue("Sender_Enroll in Plus Program").equals("Y")) {
			String sndrMGPlusNumber = commonControlsPage.getMoneyGramPlusNumber();
			dataToBeWritten.put("Sender_MoneyGram Plus Number", sndrMGPlusNumber);
			addDataToOutputExcel(dataToBeWritten);
		}

		// Click on the send money button
		commonControlsPage.click_sendMoneyBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		String refNum = commonControlsPage.getReferenceNumber();

		dataToBeWritten.put("ReferenceNumber", refNum);
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Finished button
		commonControlsPage.click_finishedBtn();

		// ***************************************************View
		// Status***************************************************

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Available"),
				"The transaction status is not \"Availble\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// ***************************************************Amend***************************************************

		// Click on the Edit Transfer button
		dashboardPage.click_editTransferBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Amend button
		tranDetails.click_amendBtn();

		// Enter Receiver First/Given Name
		amendedReceiverFirsOrGivenName = generateRandomReceiverFirstName();
		dataToBeWritten.put("Amended_Receiver_First/Given Name", amendedReceiverFirsOrGivenName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_firstOrGivenNameTxt(amendedReceiverFirsOrGivenName);

		// Enter Receiver Middle Name
		amendedReceiverMiddleName = generateRandomReceiverMiddleName();
		dataToBeWritten.put("Amemded_Receiver_Middle Name", amendedReceiverMiddleName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_middleNameTxt(amendedReceiverMiddleName);

		// Enter Receiver Last/Family Name
		amendedReceiverLastOrFamilyName = generateRandomReceiverLastName();
		dataToBeWritten.put("Amended_Receiver_LastOrFamily Name", amendedReceiverLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_lastOrFamilyNameTxt(amendedReceiverLastOrFamilyName);

		// Enter Receiver Second Last/Family Name
		amendedReceiverSecondLastOrFamilyName = generateRandomReceiverSecondLastName();
		dataToBeWritten.put("Amended_Receiver_Second LastOrFamily Name", amendedReceiverSecondLastOrFamilyName);
		addDataToOutputExcel(dataToBeWritten);
		amendTransactionDetails.type_secondLastOrFamilyTxt(amendedReceiverSecondLastOrFamilyName);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Store the current window handle
		winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on 'Yes, continue' button
		amendTransactionDetails.click_yesContinueBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on 'Finished' button
		amendTransactionDetails.click_finishedBtn();

		// ***************************************************View
		// Status***************************************************

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Available"),
				"The transaction status is not \"Availble\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// ***************************************************Select Receiver
		// Agent***************************************************

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Select Receiver Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("Receiver_AgentPOS"));

		// ***************************************************Clear
		// Cache***************************************************

		// Click on the Version link
		dashboardPage.click_versionLnk();

		// Click on Clear cache button
		dashboardPage.click_clearCacheBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Click on Close
		dashboardPage.click_closeBtn();

		// ***************************************************Receive***************************************************

		// Click on the Receive Money button
		dashboardPage.click_receiveMoneyBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Select Sender Date of Birth: Year
		receiverDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
		dataToBeWritten.put("Receiver_DOBYear", receiverDOBYear);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBYear(receiverDOBYear);

		// Select Sender Date of Birth: Month
		receiverDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
		dataToBeWritten.put("Receiver_DOBMonth", receiverDOBMonth);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBMonth(receiverDOBMonth);

		// Select Sender Date of Birth: Day
		receiverDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
		dataToBeWritten.put("Receiver_DOBDay", receiverDOBDay);
		addDataToOutputExcel(dataToBeWritten);
		findTran.select_selectDOBDay(receiverDOBDay);

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Next button
		commonControlsPage.click_nextBtn();

		// Select Receiver Title
		receiverInfoPage.select_selectReceiverTitle();
		dataToBeWritten.put("Receiver_Title", receiverInfoPage.get_selectedReceiverTitle());
		addDataToOutputExcel(dataToBeWritten);

		// Enter Birth Country
		receiverInfoPage.type_receiverBirthCountryTxt(getValue("Receiver_Birth Country"));

		// Select Occupation
		receiverInfoPage.select_selectReceiverOccupation();

		// Enter Country
		receiverInfoPage.type_receiverCountryTxt(getValue("Receiver_Country"));

		// Enter Receiver Address
		receiverAddress = "Building# " + BaseTest.generateRandomNumber(4);
		dataToBeWritten.put("Receiver_Address", receiverAddress);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressTxt(receiverAddress);

		// Enter Receiver Address Line 2
		receiverAddressLine2 = getValue("Receiver_Address Line 2");
		receiverInfoPage.type_receiverAddressLine2Txt(receiverAddressLine2);

		// Enter Receiver Address Line 3
		receiverAddressLine3 = "Apt# " + BaseTest.generateRandomNumber(3);
		dataToBeWritten.put("Receiver_Address Line 3", receiverAddressLine3);
		addDataToOutputExcel(dataToBeWritten);
		receiverInfoPage.type_receiverAddressLine3Txt(receiverAddressLine3);

		// Enter City
		receiverInfoPage.type_receiverCityTxt(getValue("Receiver_City"));

		// Enter State/Province
		receiverInfoPage.type_receiverStateOrProvinceTxt(getValue("Receiver_State/Province"));

		// Enter Postal Code
		receiverInfoPage.type_receiverPostalCodeTxt(getValue("Receiver_Postal Code"));

		// Click on Next button
		commonControlsPage.click_nextBtn();

		// Enter Receiver primary phone country code
		receiverContactInfoPage
				.type_receiverPrimaryPhoneCountryCodeTxt(getValue("Receiver_Primary Phone Country Code"));
		receiverContactInfoPage.selectRequiredCountry(getValue("Receiver_Primary Phone Country Name"));

		// Enter Receiver Primary Phone Number
		receiverPrimaryPhone = phoneNumberAreaCode + BaseTest.generateRandomNumber(7);
		dataToBeWritten.put("Receiver_Primary Phone Number", receiverPrimaryPhone);
		addDataToOutputExcel(dataToBeWritten);
		receiverContactInfoPage.type_receiverPrimaryPhoneTxt(receiverPrimaryPhone);

		// Receiver Mobile Phone
		if (!getValue("Receiver_Mobile Phone").equals("N"))
			receiverContactInfoPage.check_receiverMobilePhoneChk();

		// Enter Receiver Email
		if (!getValue("Receiver_Email").equals(""))
			receiverContactInfoPage.type_receiverEmailTxt(getValue("Receiver_Email"));

		// Select Receive Transaction Status
		receiverContactInfoPage
				.select_selectReceiverReceiveTransactionStatus(getValue("Receiver_Receive Transaction Status"));

		// Select Receive Offers
		receiverContactInfoPage.select_selectReceiverReceiveOffers(getValue("Receiver_Receive Offers"));

		// Select Preferred Language
		receiverContactInfoPage.select_selectReceiverPreferredLanguage(getValue("Receiver_Preferred Language"));

		// Select Enroll in Plus Program?
//		if (!getValue("Receiver_Enroll in Plus Program").equals("N"))
//			receiverContactInfoPage.check_receiverEnrollInPlusProgramChk();

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select Photo ID: ID Type
		receiverIDsPage.select_selectPhotoIDType(getValue("Receiver_Photo ID Type"));

		// Enter Photo ID: ID Number
		switch (getValue("Receiver_Photo ID Type")) {
		case "Alien ID":
			receiverAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Receiver_Photo ID Number", receiverAlienID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverAlienID);
			break;
		case "Drivers License":
			receiverDriversLicense = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Receiver_Photo ID Number", receiverDriversLicense);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverDriversLicense);
			break;
		case "Government ID":
			receiverGovernmentID = "G" + BaseTest.generateRandomNumber(14);
			dataToBeWritten.put("Receiver_Photo ID Number", receiverGovernmentID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverGovernmentID);
			break;
		case "Passport":
			receiverPassport = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(7);
			dataToBeWritten.put("Receiver_Photo ID Number", receiverPassport);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverPassport);
			break;
		case "State ID":
			receiverStateID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Receiver_Photo ID Number", receiverStateID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_photoIDNumberTxt(receiverStateID);
			break;
		}

		// Enter Photo ID: ID Issue Country
		receiverIDsPage.type_photoIDIssueCountryTxt(getValue("Receiver_Photo ID Issue Country"));

		// Enter Photo ID: ID Issue State/Province
		receiverIDsPage.type_photoIDIssueStateOrProvinceTxt(getValue("Receiver_Photo ID Issue StateOrProvince"));

		// Select Second form of ID: Secondary ID Type
		receiverIDsPage.select_selectSecondFormOfIDType(getValue("Receiver_Second form of ID Type"));

		// Enter Second form of ID: Secondary ID Number
		switch (getValue("Receiver_Second form of ID Type")) {
		case "Alien ID":
			receiverAlienID = "A" + BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Receiver_Second form of ID Number", receiverAlienID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverAlienID);
			break;
		case "International ID":
			receiverInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
			dataToBeWritten.put("Receiver_Second form of ID Number", receiverInternationalID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverInternationalID);
			break;
		case "Social Security Number":
			receiverSocialSecurityNumber = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Receiver_Second form of ID Number", receiverSocialSecurityNumber);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverSocialSecurityNumber);
			break;
		case "Tax ID":
			receiverTaxID = BaseTest.generateRandomNumber(9);
			dataToBeWritten.put("Receiver_Second form of ID Number", receiverTaxID);
			addDataToOutputExcel(dataToBeWritten);
			receiverIDsPage.type_secondaryIDNumberTxt(receiverTaxID);
			break;
		}

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Select Third party
		switch (getValue("Receiver_Third Party")) {
		case "None":
			thirdPartyInfoPage.click_thirdPartyInformationNoneRad();
			break;
		case "Organization":
			thirdPartyInfoPage.click_thirdPartyInformationOrganizationRad();

			thirdPartyInfoPage
					.type_thirdPartyReceiverOrganizationNameTxt(getValue("Receiver_ThirdParty_OrganizationName"));

			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("Receiver_ThirdParty_Country"));

			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("Receiver_ThirdParty_Address");
			dataToBeWritten.put("Receiver_ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("Receiver_ThirdParty_City"));
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("Receiver_ThirdParty_State"));
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("Receiver_ThirdParty_PostalCode"));

			// Enter third party id type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("Receiver_ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("Receiver_ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}
			break;
		case "Person":
			thirdPartyInfoPage.click_thirdPartyInformationPersonRad();

			thirdPartyLastOrFamilyName = generateRandomSenderLastName();
			dataToBeWritten.put("Receiver_ThirdParty_Person_LastOrFamilyName", thirdPartyLastOrFamilyName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonLastOrFamilyNameTxt(thirdPartyLastOrFamilyName);

			thirdPartyFirstOrGivenName = generateRandomSenderFirstName();
			dataToBeWritten.put("Receiver_ThirdParty_Person_FirstOrGivenName", thirdPartyFirstOrGivenName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonFirstOrGivenNameTxt(thirdPartyFirstOrGivenName);

			thirdPartyMiddleName = generateRandomSenderMiddleName();
			dataToBeWritten.put("Receiver_ThirdParty_Person_MiddleName", thirdPartyMiddleName);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverPersonMiddleNameTxt(thirdPartyMiddleName);

			thirdPartyInfoPage.type_thirdPartyCountryTxt(getValue("Receiver_ThirdParty_Country"));

			thirdPartyAddress = BaseTest.generateRandomNumber(4) + " " + getValue("Receiver_ThirdParty_Address");
			dataToBeWritten.put("Receiver_ThirdParty_Address", thirdPartyAddress);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.type_thirdPartyReceiverAddressTxt(thirdPartyAddress);

			thirdPartyInfoPage.type_thirdPartyReceiverCityTxt(getValue("Receiver_ThirdParty_City"));
			thirdPartyInfoPage.type_thirdPartyStateOrProvinceTxt(getValue("Receiver_ThirdParty_State"));
			thirdPartyInfoPage.type_thirdPartyReceiverPostalCodeTxt(getValue("Receiver_ThirdParty_PostalCode"));

			// Enter Third Party ID Type
			thirdPartyInfoPage.select_selectThirdPartyReceiverIDType(getValue("Receiver_ThirdParty_IDType"));

			// Enter Third Party ID Number
			switch (getValue("Receiver_ThirdParty_IDType")) {
			case "Alien ID":
				thirdPartyAlienID = "A" + BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyAlienID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyAlienID);
				break;
			case "International ID":
				thirdPartyInternationalID = BaseTest.generateRandomAlphabet() + BaseTest.generateRandomNumber(12);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyInternationalID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyInternationalID);
				break;
			case "Social Security Number":
				thirdPartySocialSecurityNumber = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartySocialSecurityNumber);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartySocialSecurityNumber);
				break;
			case "Tax ID":
				thirdPartyTaxID = BaseTest.generateRandomNumber(9);
				dataToBeWritten.put("Receiver_ThirdParty_IDNumber", thirdPartyTaxID);
				addDataToOutputExcel(dataToBeWritten);
				thirdPartyInfoPage.type_thirdPartyReceiverIDNumberTxt(thirdPartyTaxID);
				break;
			}

			thirdPartyDOBYear = generateRandomNumberWithinRange(DOBYearMinRange, DOBYearMaxRange);
			dataToBeWritten.put("Receiver_ThirdParty_Person_DOBYear", thirdPartyDOBYear);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBYear(thirdPartyDOBYear);

			thirdPartyDOBMonth = generateRandomNumberWithinRange(DOBMonthMinRange, DOBMonthMaxRange);
			dataToBeWritten.put("Receiver_ThirdParty_Person_DOBMonth", thirdPartyDOBMonth);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBMonth(thirdPartyDOBMonth);

			thirdPartyDOBDay = generateRandomNumberWithinRange(DOBDayMinRange, DOBDayMaxRange);
			dataToBeWritten.put("Receiver_ThirdParty_Person_DOBDay", thirdPartyDOBDay);
			addDataToOutputExcel(dataToBeWritten);
			thirdPartyInfoPage.select_selectThirdPartyReceiverDOBDay(thirdPartyDOBDay);

			thirdPartyInfoPage.select_selectThirdPartyReceiverOccupation();
			break;
		}

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Store the current window handle
		winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on the Payout button
		receiverInfoPage.click_payoutBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the pre-payment receipt window exists. If exists, close it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on Finished button
		receiverInfoPage.click_finishedBtn();

		// ***************************************************View
		// Status***************************************************

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(refNum);

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Received"
		Assert.assertTrue(tranDetails.getTransactionStatus().contains("Received"),
				"The transaction status is not \"Received\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// ***************************************************Receive
		// Reversal***************************************************

		// Click on the Edit Transfer button
		dashboardPage.click_editTransferBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Reversal button
		tranDetails.click_reversalBtn();

		// Select Reversal Reason
		tranDetails.select_selectReversalReason();
		dataToBeWritten.put("Reversal Reason", tranDetails.get_selectedReversalReason());
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Click Yes, continue button
		tranDetails.click_yesContinueBtn();

		// Click Finished button
		tranDetails.click_finishedBtn();

		// ***************************************************View
		// Status***************************************************

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Available"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Available"),
				"The transaction status is not \"Available\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		// ***************************************************Cancel***************************************************

		// Click on Transact header link
		dashboardPage.click_transactHeaderLnk();

		// Select Sender Agent/POS
		if (BaseTest.usefakeauthUrl.equals("Y"))
			dashboardPage.select_selectAgentPOS(getValue("Sender_AgentPOS"));

		// Click on the Edit Transfer button
		dashboardPage.click_editTransferBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Click on Refund button
		tranDetails.click_refundBtn();

		// Select the Reason for refund transaction
		sendReversalTransactionDetails.select_selectReasonForRefundTransaction();
		dataToBeWritten.put("Reason for Refund Transaction",
				sendReversalTransactionDetails.get_selectedReasonForRefundTransaction());
		addDataToOutputExcel(dataToBeWritten);

		// Click on the Next button
		commonControlsPage.click_nextBtn();

		// Store the current window handle
		winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: " + winHandleBefore);

		// Click on 'Yes, continue' button
		sendReversalTransactionDetails.click_yesContinueBtn();

		// Wait for spinner to be gone
		dashboardPage.check_ForSpinner();

		// Close the print window
		wait(10);
		closePrintWindow.closePrintWindow();

		// Check whether the agent and customer receipt window exists. If exists, close
		// it
		winIds = closeReceiptsWindowPage.getWindowHandles();
		System.out.println("No of Windows opened: " + winIds.size());

		if (winIds.size() > 1) {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				if (!winHandle.equals(winHandleBefore)) {
					driver.switchTo().window(winHandle);
					closeReceiptsWindowPage.takeScreenshot("Minimized Agent and Customer receipt window: ");
					driver.manage().window().maximize();
					closeReceiptsWindowPage.takeScreenshot("Maximized Agent and Customer receipt window: ");
					wait(5);
					driver.close();
				}
			}
		}

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Click on 'Finished' button
		sendReversalTransactionDetails.click_finishedBtn();

		// ***************************************************View
		// Status***************************************************

		// Click on the View Status button
		dashboardPage.click_viewStatusBtn();

		// Enter the Reference Number
		findTran.type_referenceNumberTxt(getValue("ReferenceNumber"));

		// Click on Next button
		findTran.click_nextBtn();

		// Check whether the transaction status is "Canceled"
		Assert.assertTrue(tranDetails.getTransactionStatus().equals("Canceled"),
				"The transaction status is not \"Canceled\".");

		// Click on the Finish button
		tranDetails.click_finishBtn();

		dataToBeWritten.put("status", "Pass");
		addDataToOutputExcel(dataToBeWritten);
	}
}
